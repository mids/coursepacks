from sage.all import matrix, lcm, random_matrix, identity_matrix, QQ, ZZ


def myQR(A):
    'test with A = matrix.column([[1, 1, 1, 1], [-1, 4, 4, -1], [4, -2, 2, 0]])'
    # A = matrix([(-1, 0, 0), (-2, -2, 4), (-2, -4, -1)])
    Q, _ = A.change_ring(QQ).T.gram_schmidt()
    Q = matrix.column([row.normalized() for row in Q.rows()])
    R = Q.T * A
    return (Q, R)


def mydenom(A):
    return lcm([_.denominator() for _ in A.list()])


def rand_orth_QQ(n):
    A = random_matrix(QQ, n)
    B = A - A.T
    I = identity_matrix(n)
    return ((B-I).inverse())*(B+I)


def randQ(n, d):
    Q = rand_orth_QQ(n)
    while mydenom(Q) != d:
        Q = rand_orth_QQ(n)
    return Q


def myrandZZ():
    n = ZZ.random_element()
    while n == 0:
        n = ZZ.random_element()
    return n


def mypartition(n, l, nzeros):
    first = [myrandZZ() for _ in range(l - 1)]
    middle = [n - sum(first)]
    last = [0] * nzeros
    return first + middle + last


def myR(n, d):
    columns = [[d] + [0] * (n - 1)]
    for k in range(1, n):
        columns.append(mypartition(d, k + 1, n - k - 1))
    return matrix.column(columns)
