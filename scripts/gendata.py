from collections import namedtuple
from sage.all import RealDistribution, var, list_plot, plot, floor, matrix, vector, lcm, point, mean, ZZ, sqrt


def gendata(n=8, xmin=-5, xmax=10, a0=-5, a1=3, s=4,centered=None, lineZZ=None):
    var('t')
    Data = namedtuple('Data', 'pts A xhat xcoords ycoords f p l')
    while True:
        X = RealDistribution('uniform', [xmin, xmax])
        Y = RealDistribution('gaussian', s)
        f = a1 * t + 10
        # f = lambda x: a1 * x + a0
        xvals = [floor(X.get_random_element()) for _ in range(n)]
        pts = list(set([(x, f(t=x) + floor(Y.get_random_element())) for x in xvals]))
        D = matrix(pts)
        xcoords, ycoords = D.columns()
        if centered:
            if not all([mean(xcoords) in ZZ and mean(ycoords) in ZZ]):
                continue
        A = matrix.column([xcoords, [1] * len(xcoords)])
        xhat = (A.T*A).inverse() * A.T * ycoords
        l = lcm([_.denominator() for _ in xhat])
        if not lineZZ is None:
            if l > lineZZ:
                continue
        xhat1, xhat2 = xhat
        f = xhat1 * t + xhat2
        p = point(pts, pointsize=60, color='orange') + plot(f, min(xcoords)-1, max(xcoords)+1, thickness=3)
        yield Data(pts, A, xhat, xcoords, ycoords, f, p, l)


def gendata_centered(n=8, xmin=-5, xmax=10, a0=-5, a1=3, s=4, centered=True, lineZZ=None):
    var('t')
    DataCentered = namedtuple('DataCentered', 'pts X H u1 u2 p_aspect p')
    while True:
        g = gendata(n, xmin, xmax, a0, a1, s, True, lineZZ)
        data = next(g)
        xmean, ymean = mean(data.xcoords), mean(data.ycoords)
        xcoords = [x-xmean for x in data.xcoords]
        ycoords = [y-ymean for y in data.ycoords]
        pts = zip(xcoords, ycoords)
        X = matrix(pts)
        H = X.transpose() * X / (X.nrows() - 1)
        D, P = H.jordan_form(transformation=True)
        l1, l2 = D.diagonal()
        v1, v2 = P.columns()
        u1, u2 = sqrt(l1) * v1.normalized(), sqrt(l2) * v2.normalized()
        p_aspect = point(pts, pointsize=60, color='orange', aspect_ratio=1) + plot(u1, thickness=3) + plot(u2, thickness=3)
        p = point(pts, pointsize=60, color='orange') + plot(u1, thickness=3) + plot(u2, thickness=3)
        yield data, DataCentered(pts, X, H, u1, u2, p_aspect, p)
