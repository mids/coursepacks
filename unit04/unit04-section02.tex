\documentclass[12pt]{article}

\title{Unit 4.2: The Spectral Theorem and Principal Component Analysis}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\maketitle

\tableofcontents


\newpage
\section{Introduction}

In Unit 3, we used the method of \emph{least squares} to construct the
\emph{line of best fit} to a given data set.
\[
  \begin{tikzpicture}[line cap=round, line join=round, scale=1/2]

    \draw[ultra thick, <->] (-5, 0) -- (6, 0);
    \draw[ultra thick, <->] (0, -6) -- (0, 5);

    \coordinate (p1) at (-4, -5);
    \coordinate (p2) at (0, 1);
    \coordinate (p3) at (2, 2);
    \coordinate (p4) at (3, 4);
    \coordinate (p5) at (1, 0);
    \coordinate (p6) at (-3, -3);
    \coordinate (p7) at (-2, -3);
    \coordinate (p8) at (1, 2);
    \coordinate (p9) at (0, -1);
    \coordinate (p10) at (2, 3);

    \draw[ultra thick, red] (p1) -- (-4, -59/12);
    \draw[ultra thick, red] (p2) -- (0, 0);
    \draw[ultra thick, red] (p3) -- (2, 59/24);
    \draw[ultra thick, red] (p4) -- (3, 59/16);
    \draw[ultra thick, red] (p5) -- (1, 59/48);
    \draw[ultra thick, red] (p6) -- (-3, -59/16);
    \draw[ultra thick, red] (p7) -- (-2, -59/24);
    \draw[ultra thick, red] (p8) -- (1, 59/48);
    \draw[ultra thick, red] (p9) -- (0, 0);
    \draw[ultra thick, red] (p10) -- (2, 59/24);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};
    \node[orange] at (p6) {\textbullet};
    \node[orange] at (p7) {\textbullet};
    \node[orange] at (p8) {\textbullet};
    \node[orange] at (p9) {\textbullet};
    \node[orange] at (p10) {\textbullet};

    % \node at (p1) [below] {$(1, -1)$};
    % \node at (p2) [above] {$(2, 2)$};
    % \node at (p3) [left] {$(0, 6)$};
    % \node at (p4) [left] {$(0, 3)$};
    % \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-6/58*48:5/59*48, smooth, variable=\x, blue]
    plot ({\x},{59*\x/48}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

  \end{tikzpicture}
\]
The \emph{error} in using the line of best fit to approximate the data set is
defined by measuring the \emph{vertical distance} from each point to the
line. The method of least squares produces the line that minimizes this error.

In this section, we will use the theory of eigenvalues and eigenvectors to
develop a more sophisticated method of linear regression called \emph{principal
  component analysis (or PCA)}. When using PCA, we use \emph{orthogonal
  distance} to measure the error in using a line to approximate data.
\[
  \begin{tikzpicture}[line cap=round, line join=round, scale=1/2]

    \draw[ultra thick, <->] (-5, 0) -- (6, 0);
    \draw[ultra thick, <->] (0, -6) -- (0, 5);

    \coordinate (p1) at (-4, -5);
    \coordinate (p2) at (0, 1);
    \coordinate (p3) at (2, 2);
    \coordinate (p4) at (3, 4);
    \coordinate (p5) at (1, 0);
    \coordinate (p6) at (-3, -3);
    \coordinate (p7) at (-2, -3);
    \coordinate (p8) at (1, 2);
    \coordinate (p9) at (0, -1);
    \coordinate (p10) at (2, 3);

    \draw[ultra thick, red] (p1) -- (-4.04079515989628, -4.96681071737251);
    \draw[ultra thick, red] (p2) -- (0.489541918755402, 0.601728608470182);
    \draw[ultra thick, red] (p3) -- (1.77562662057044, 2.18254105445117);
    \draw[ultra thick, red] (p4) -- (3.15298184961106, 3.87554019014693);
    \draw[ultra thick, red] (p5) -- (0.398271391529818, 0.489541918755402);
    \draw[ultra thick, red] (p6) -- (-2.66343993085566, -3.27381158167675);
    \draw[ultra thick, red] (p7) -- (-1008/445, -1239/445);
    \draw[ultra thick, red] (p8) -- (1.37735522904062, 1.69299913569576);
    \draw[ultra thick, red] (p9) -- (-0.489541918755402, -0.601728608470182);
    \draw[ultra thick, red] (p10) -- (1008/445, 1239/445);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};
    \node[orange] at (p6) {\textbullet};
    \node[orange] at (p7) {\textbullet};
    \node[orange] at (p8) {\textbullet};
    \node[orange] at (p9) {\textbullet};
    \node[orange] at (p10) {\textbullet};

    % \node at (p1) [below] {$(1, -1)$};
    % \node at (p2) [above] {$(2, 2)$};
    % \node at (p3) [left] {$(0, 6)$};
    % \node at (p4) [left] {$(0, 3)$};
    % \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-6/58*48:5/59*48, smooth, variable=\x, blue]
    plot ({\x},{59*\x/48}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

  \end{tikzpicture}
\]

PCA produces the line that minimizes this orthogonal error.

\section{The Spectral Theorem}

In the method of principal component analysis, one analyzes the eigenvalues and
eigenvectors of the \emph{covariance matrix} of a given data set. The covariance
matrix is a symmetric matrix which encodes spacial properties between the points
in the data. Recall that a matrix $A$ is \emph{symmetric} if it is square and if
the matrix is equal to its transpose $A^\intercal=A$. The eigenvalues and
eigenvectors of symmetric matrices have interesting algebraic properties, which
are described in the \emph{spectral theorem}. The development of PCA relies on
this theorem.

Before stating the spectral theorem, let us review some terminology. Recall from
Unit 3 Section 2 that a matrix $Q$ is \emph{orthogonal} if
$Q^\intercal=Q^{-1}$. Orthogonal matrices are the square matrices whose columns
are orthonormal. Also recall the notion of \emph{diagonalizable matrices} from
Unit 4 Section 1. A matrix $A$ is \emph{diagonalizable} if it can be factored as
$A=PDP^{-1}$ where $D$ is a diagonal matrix. When a diagonalization $A=PDP^{-1}$
is given, the entries along the diagonal of $D$ are the eigenvalues of $A$
(counting geometric multiplicity) and the columns of $P$ are the corresponding
eigenvectors of $A$.

The spectral theorem states that every symmetric matrix $A$ is diagonalizable
and may be diagonalized as $A=QDQ^\intercal$ where $Q$ is an orthogonal matrix
$Q^{-1}=Q^\intercal$.

\begin{genbox}{The Spectral Theorem (Diagonalization of Symmetric Matrices)}
  Let $A$ be a symmetric matrix, so $A^\intercal=A$.  Then there is a
  diagonalization $A=QDQ^\intercal$ where $Q$ is an orthogonal matrix
  $Q^\intercal=Q^{-1}$.
\end{genbox}

The spectral theorem guarantees that any given symmetric matrix is
diagonalizable and that the eigenvectors chosen during the diagonalization
process may be chosen to be orthonormal.

\begin{sagesilent}
  # Q = [(4/9, -7/9, -4/9), (-1/9, 4/9, -8/9), (-8/9, -4/9, -1/9)]
  # D = diagonal_matrix([81, 729, -81])
  v1 = vector([-12/5, 1])
  v2 = vector([5/12, 1])
  u1 = v1.normalized()
  u2 = v2.normalized()
  Q = matrix.column([u1, u2])
  l1, l2 = 13**2, -13**2
  D = diagonal_matrix([l1, l2])
  A = Q*D*Q.T
  n = A.nrows()
  I = identity_matrix(n)
  M1 = A-l1*I
  M2 = A-l2*I
\end{sagesilent}
\begin{example}
  Consider the matrix $A$ given by
  \[
    A = \sage{A}
  \]
  The eigenvalues of $A$ are $\lambda_1=\sage{l1}$ and $\lambda_2=\sage{l2}$.
  Note that $A$ is symmetric since $A^\intercal=A$. According to the spectral
  theorem, $A$ is guaranteed to have a diagonalizabion $A=QDQ^\intercal$ where
  $Q$ is an orthogonal matrix.

  To find the eigenvectors associated to $\lambda_1=\sage{l1}$, note that
  \[
    \rref(A-\lambda_1\cdot I_{\sage{n}})
    = \rref\sage{M1}
    = \sage{M1.rref()}
  \]
  The pivot basis associated to $\lambda_1$ is then $\Set{\vv*{v}{1}}$ where
  $\vv*{v}{1}=\sage{v1}$.

  To find the eigenvectors associated to $\lambda_2=\sage{l2}$, note that
  \[
    \rref(A-\lambda_2\cdot I_{\sage{n}})
    = \rref\sage{M2}
    = \sage{M2.rref()}
  \]
  The pivot basis associated to $\lambda_2$ is then $\Set{\vv*{v}{2}}$ where
  $\vv*{v}{2}=\sage{v2}$.

  Now, note that $\vv*{v}{1}\cdot\vv*{v}{2}=\sage{v1*v2}$, so $\vv*{v}{1}$ and
  $\vv*{v}{2}$ are \emph{orthogonal}. The lengths of $\vv*{v}{1}$ and
  $\vv*{v}{2}$ are
  \begin{align*}
    \norm{\vv*{v}{1}} &= \sage{v1.norm()} & \norm{\vv*{v}{2}} &= \sage{v2.norm()}
  \end{align*}
  The normalizations of $\vv*{v}{1}$ and $\vv*{v}{2}$ are then
  \begin{align*}
    \vv*{q}{1} &= \oldfrac{1}{\norm{\vv*{v}{1}}}\vv*{v}{1} & \vv*{q}{2} &= \oldfrac{1}{\norm{\vv*{v}{2}}}\vv*{v}{2} \\
               &= \sage{u1}                                &            &= \sage{u2}
  \end{align*}
  The list $\Set{\vv*{q}{1}, \vv*{q}{2}}$ is then orthonormal. If we insert
  these vetors into the columns of a matrix $Q$, we obtain an orthogonal
  matrix. This gives the diagonalization $A=QDQ^\intercal$ where
  \begin{align*}
    Q &= \sage{Q} & D &= \sage{D}
  \end{align*}
\end{example}


\section{The Covariance Matrix}

\subsection{Centering Data}

Many formulas in mathematical statistics involve \emph{centered} data. The
process of centering data involves subtracting the mean from every observation
so that the new data has mean zero. When we center data stored in a matrix $A$,
we produce a new matrix $A_c$. The data inside $A_c$ is called the ``centered
data.''

\begin{example}\label{ex:student}
  Suppose we measure the height ($h$ in inches), weight ($w$ in pounds), and
  final exam score ($e$) of each student in a class of five. Our data is stored
  in a matrix
  \[
    A =
    \begin{blockarray}{r *{3}{r}}
      \begin{block}{r *{3}{>{$\footnotesize}c<{$}}}
        & $h$ & $w$ & $e$ \\
      \end{block}
      \begin{block}{>{$\footnotesize}l<{$}[*{3}{r}]}
        Student 1 & 62 & 113 & 90 \\
        Student 2 & 74 & 183 & 79 \\
        Student 3 & 71 & 176 & 95 \\
        Student 4 & 64 & 127 & 64 \\
        Student 5 & 74 & 181 & 82 \\
      \end{block}%
    \end{blockarray}%
  \]
  The average height, weight, and final exam score is
  \begin{sagesilent}
    A = matrix.column([(62, 74, 71, 64, 74), (113, 183, 176, 127, 181), (90, 79, 95, 64, 82)])
    h, w, e = A.columns()
    h1, h2, h3, h4, h5 = h
    w1, w2, w3, w4, w5 = w
    e1, e2, e3, e4, e5 = e
    M = matrix.column([[mean(h)] * A.nrows(), [mean(w)] * A.nrows(), [mean(e)] * A.nrows()])
    Ac = A-M
    a, b, c = Ac.columns()
    a1, a2, a3, a4, a5 = a
    b1, b2, b3, b4, b5 = b
    c1, c2, c3, c4, c5 = c
  \end{sagesilent}
  \begin{align*}
    \bar{h} &= \oldfrac{\sage{h1}+\sage{h2}+\sage{h3}+\sage{h4}+\sage{h5}}{5} = \sage{mean(h)} \\
    \bar{w} &= \oldfrac{\sage{w1}+\sage{w2}+\sage{w3}+\sage{w4}+\sage{w5}}{5} = \sage{mean(w)} \\
    \bar{e} &= \oldfrac{\sage{e1}+\sage{e2}+\sage{e3}+\sage{e4}+\sage{e5}}{5} = \sage{mean(e)}
  \end{align*}
  To center this data, we subtract the mean from each corresponding variable
  \[
    A_c
    = \sage{A}-\sage{M}
    = \sage{A-M}
  \]
  Note that each column of this new matrix $A_c$ has mean zero
  \begin{align*}
    \oldfrac{\sage{a1}+\sage{a2}+\sage{a3}+\sage{a4}+\sage{a5}}{5} &= \sage{mean(a)} \\
    \oldfrac{\sage{b1}+\sage{b2}+\sage{b3}+\sage{b4}+\sage{b5}}{5} &= \sage{mean(b)} \\
    \oldfrac{\sage{c1}+\sage{c2}+\sage{c3}+\sage{c4}+\sage{c5}}{5} &= \sage{mean(c)}
  \end{align*}
\end{example}

It is useful to note that when we use a line to approximate centered data, the
line is always of the form $f(x)=m\cdot x$. That is, the line always passes
through the origin.

\begin{sagesilent}
  A = matrix([(-3, 6), (2, 11), (-2, 8), (-5, 3), (-5, 4), (-5, 5), (-3, 5)])
  c1, c2 = A.columns()
  n = A.nrows()
  M = matrix.column([[mean(c1)] * n, [mean(c2)] * n])
  Ac = A-M
\end{sagesilent}
\begin{example}\label{ex:pts}
  The figure below depicts the least squares line of best fit to the data points stored in the rows of the matrix $A$.
  \newcommand{\myData}{
    \begin{array}{c}
      \begin{tikzpicture}[line cap=round, line join=round, scale=1/2]

        \draw[ultra thick, <->] (-6, 0) -- (3, 0);
        \draw[ultra thick, <->] (0, -1) -- (0, 12);

        \coordinate (p1) at (-3, 6);
        \coordinate (p2) at (2, 11);
        \coordinate (p3) at (-2, 8);
        \coordinate (p4) at (-5, 3);
        \coordinate (p5) at (-5, 4);
        \coordinate (p6) at (-5, 5);
        \coordinate (p7) at (-3, 5);


        \node[orange] at (p1) {\textbullet};
        \node[orange] at (p2) {\textbullet};
        \node[orange] at (p3) {\textbullet};
        \node[orange] at (p4) {\textbullet};
        \node[orange] at (p5) {\textbullet};
        \node[orange] at (p6) {\textbullet};
        \node[orange] at (p7) {\textbullet};

        % \node at (p1) [below] {$(1, -1)$};
        % \node at (p2) [above] {$(2, 2)$};
        % \node at (p3) [left] {$(0, 6)$};
        % \node at (p4) [left] {$(0, 3)$};
        % \node at (p5) [above] {$(1, 4)$};

        \draw[very thick, <->, domain=-6:3, smooth, variable=\x, blue]
        plot ({\x},{39/38*\x + 345/38}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{align*}
    A &= \sage{A} & \myData
  \end{align*}
  The formula for this line is $f(x)=(\frac{39}{38})x + \frac{345}{38}$. The
  centered data is given by
  \newcommand{\myDataC}{
    \begin{array}{c}
      \begin{tikzpicture}[line cap=round, line join=round, scale=1/2]

        \draw[ultra thick, <->] (-3, 0) -- (6, 0);
        \draw[ultra thick, <->] (0, -4) -- (0, 6);

        \coordinate (p1) at (0, 0);
        \coordinate (p2) at (5, 5);
        \coordinate (p3) at (1, 2);
        \coordinate (p4) at (-2, -3);
        \coordinate (p5) at (-2, -2);
        \coordinate (p6) at (-2, -1);
        \coordinate (p7) at (0, -1);

        \node[orange] at (p1) {\textbullet};
        \node[orange] at (p2) {\textbullet};
        \node[orange] at (p3) {\textbullet};
        \node[orange] at (p4) {\textbullet};
        \node[orange] at (p5) {\textbullet};
        \node[orange] at (p6) {\textbullet};
        \node[orange] at (p7) {\textbullet};

        % \node at (p1) [below] {$(1, -1)$};
        % \node at (p2) [above] {$(2, 2)$};
        % \node at (p3) [left] {$(0, 6)$};
        % \node at (p4) [left] {$(0, 3)$};
        % \node at (p5) [above] {$(1, 4)$};

        \draw[very thick, <->, domain=-3:6, smooth, variable=\x, blue]
        plot ({\x},{39/38*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{align*}
    A_c &= \sage{A}-\sage{M} = \sage{Ac} & \myDataC
  \end{align*}
  Note that centering the data results in translating these points in the plane
  and that the ``shape'' of the data is unaltered. The least squares line of
  best fit for the centered data is given by $f(x)=(\frac{39}{38})\,x$.
\end{example}

\subsection{Definition of the Covariance Matrix}

The object of interest in principal component analysis is the \emph{covariance
  matrix} of the matrix $A$ where data is stored.

\begin{genbox}{The Covariance Matrix}
  Suppose that data is stored in an $n\times p$ matrix $A$, where each column
  corresponds to a variable. The \emph{covariance matrix} of $A$ is defined as
  the new matrix $\Sigma$ (Greek ``sigma'') given by
  \[
    \Sigma = \frac{1}{n-1}A_c^\intercal A_c
  \]
\end{genbox}

So, to compute the covariance matrix of $A$, we start by centering our data to
obtain $A_c$. We then multiply the transpose of $A_c$ by $A_c$ to obtain
$A_c^\intercal A_c$. Finally, we scale $A_c^\intercal A_c$ by $\frac{1}{(n-1)}$,
where $n$ is the number of rows of $A$ to obtain the covariance matrix
$\displaystyle \Sigma=\oldfrac{1}{n-1} A_c^\intercal A_c$.

\begin{example}
  Consider the data from Example \ref{ex:student} above
  \[
    A =
    \begin{blockarray}{r *{3}{r}}
      \begin{block}{r *{3}{>{$\footnotesize}c<{$}}}
        & $h$ & $w$ & $e$ \\
      \end{block}
      \begin{block}{>{$\footnotesize}l<{$}[*{3}{r}]}
        Student 1 & 62 & 113 & 90 \\
        Student 2 & 74 & 183 & 79 \\
        Student 3 & 71 & 176 & 95 \\
        Student 4 & 64 & 127 & 64 \\
        Student 5 & 74 & 181 & 82 \\
      \end{block}%
    \end{blockarray}%
  \]
  We previously found that the centered data is given by
  \begin{sagesilent}
    A = matrix.column([(62, 74, 71, 64, 74), (113, 183, 176, 127, 181), (90, 79, 95, 64, 82)])
    h, w, e = A.columns()
    M = matrix.column([[mean(h)] * A.nrows(), [mean(w)] * A.nrows(), [mean(e)] * A.nrows()])
    Ac = A-M
  \end{sagesilent}
  \[
    A_c=\sage{A}-\sage{M}=\sage{Ac}
  \]
  The covariance matrix is then given by
  \begin{align*}
    \Sigma
    &= \oldfrac{1}{\sage{A.nrows()}-1}A_c^\intercal A_c \\
    &= \oldfrac{1}{\sage{A.nrows()-1}}\sage{Ac.T}\sage{Ac} \\
    &= \oldfrac{1}{\sage{A.nrows()-1}}\sage{Ac.T*Ac}
  \end{align*}
\end{example}

\begin{sagesilent}
  A = matrix([(-3, 6), (2, 11), (-2, 8), (-5, 3), (-5, 4), (-5, 5), (-3, 5)])
  c1, c2 = A.columns()
  n = A.nrows()
  M = matrix.column([[mean(c1)] * n, [mean(c2)] * n])
  Ac = A-M
\end{sagesilent}
\begin{example}
  Consider the data points from Example \ref{ex:pts} stored in the rows of the
  matrix $A$ given by
  \[
    A=\sage{A}
  \]
  We previously found that the centered data is given by
  \[
    A_c=\sage{A}-\sage{M}=\sage{Ac}
  \]
  The covariance matrix is then
  \[
    \Sigma
    = \oldfrac{1}{\sage{A.nrows()}-1}A_c^\intercal A_c
    = \oldfrac{1}{\sage{A.nrows()-1}}\sage{Ac.T}\sage{Ac}
    = \oldfrac{1}{\sage{A.nrows()-1}}\sage{Ac.T*Ac}
  \]
\end{example}

The most important feature of the covariance matrix is a consequence of the
following computation
\[
  \Sigma^\intercal
  = \pair*{\frac{1}{n-1}A_c^\intercal A_c}^\intercal
  = \frac{1}{n-1}\pair*{A_c^\intercal A_c}^\intercal
  = \frac{1}{n-1}A_c^\intercal (A_c^\intercal)^\intercal
  = \frac{1}{n-1}A_c^\intercal A_c
  = \Sigma
\]
That is, transposing the covariance matrix does not change the matrix. This
means that the covariance matrix is symmetric. It is also true that the
eigenvalues of the covariance matrix are positive numbers. Symmetric matrices
with positive eigenvalues are called \emph{positive definite}.

\begin{genbox}{The Covariance Matrix is Positive Definite}
  The covariance matrix $\Sigma$ is a \emph{positive definite} symmetric matrix,
  which means that the eigenvalues of $\Sigma$ are positive numbers.
\end{genbox}

\section{Principal Component Analysis}

\subsection{Orthogonal Error}

\begin{sagesilent}
  A = matrix([(0, 6), (1, 0), (2, 0)])
  n = A.nrows()
  c1, c2 = A.columns()
  M = matrix.column([[mean(c1)] * n, [mean(c2)] * n])
  Ac = A-M
  r1, r2, r3 = Ac.rows()
  p1, p2, p3 = [tuple(row) for row in Ac.rows()]
  var('x')
  f = -3*x
\end{sagesilent}
Consider the problem of using the line $f(x)=\sage{f}$ to approximate the data
set
\[
  \Set{\sage{p1}, \sage{p2}, \sage{p3}}
\]
In Unit 3 Section 1, we defined the \emph{error} of using this line to
approximate the data as the sum of the squares of the vertical distances between
the points and the line.
\[
  \begin{tikzpicture}[line cap=round, line join=round, xscale=2, scale=3/4]

    \draw[ultra thick, <->] (-2, 0) -- (2, 0);
    \draw[ultra thick, <->] (0, -4) -- (0, 5);

    \coordinate (p1) at (-1, 4);
    \coordinate (p2) at (0, -2);
    \coordinate (p3) at (1, -2);

    \draw[ultra thick, red] (p1) -- (-1, 3);
    \draw[ultra thick, red] (p2) -- (0, 0);
    \draw[ultra thick, red] (p3) -- (1, -3);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};

    % \node at (p1) [below] {$(1, -1)$};
    % \node at (p2) [above] {$(2, 2)$};
    % \node at (p3) [left] {$(0, 6)$};
    % \node at (p4) [left] {$(0, 3)$};
    % \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-3/2:4/3, smooth, variable=\x, blue]
    plot ({\x},{-3*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

  \end{tikzpicture}
\]
In this case, the error is
\begin{align*}
  E
  &= (4-f(-1))^2 + (-2-f(0))^2 + (-2-f(1))^2 \\
  &= (4-3)^2+(-2-0)^2+(-2-(-3))^2 \\
  &= \sage{(4-3)**2}+\sage{(-2-0)**2}+\sage{(-2-(-3))**2} \\
  &= \sage{(4-3)**2+(-2-0)**2+(-2-(-3))**2}
\end{align*}
In principal component analysis, we use a different version of error measurement
called \emph{orthogonal error}. Recall from Unit 3 Section 2 the formula for
projecting a vector $\vv{v}$ onto another vector $\vv{w}$, which is given by
\[
  \proj_{\vv{w}}(\vv{v})
  = \oldfrac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}\vv{w}
\]
To find the \emph{orthogonal distance} from a vector $\vv{v}$ to another vector
$\vv{w}$, we compute the value of $\norm{\vv{v}-\proj_{\vv{w}}(\vv{v})}$. This
distance is depicted by the dashed line in the figure below.
\[
  \begin{tikzpicture}[line join=round, line cap=round]%, transform shape, rotate=40]

    \coordinate (O) at (0, 0);
    \coordinate (e1) at (1, 0);
    \coordinate (e2) at (0, 1);

    \coordinate (v) at (3, 2);
    \coordinate (w) at (5, 0);
    \coordinate (p) at (3, 0);

    \draw [ultra thick, dashed, violet] (v) -- (p) node[midway, right, fill=white] {$\left\lVert\vv{v}-\operatorname{proj}_{\vv{w}}(\vv{v})\right\rVert$};
    % \draw [ultra thick, dashed, orange] (v) -- (w) node[midway, right] {\tiny$\left\lVert\vv{v}-\vv{w}\right\rVert$};

    \draw [ultra thick, blue, ->] (O) -- (v) node[above] {$\vv{v}$};
    \draw [ultra thick, blue, ->] (O) -- (w) node[right] {$\vv{w}$};
    \draw [ultra thick, red, ->] (O) -- (p) node[below] {$\operatorname{proj}_{\vv{w}}(\vv{v})$};


    % \coordinate (L) at ($ 8*(e1) $);

    % \coordinate (P1) at ($ 1/2*(L) $);
    % \coordinate (Q1) at ($ (P1)+3*(e2) $);

    % \coordinate (P2) at ($ 3/4*(L) $);
    % \coordinate (Q2) at ($ (P2)-2*(e2) $);

    % \draw[thick] (P1) rectangle ($ (P1)+.25*(e1)+.25*(e2) $);
    % \draw[thick] (P2) rectangle ($ (P2)+.25*(e1)-.25*(e2) $);
    % \draw[thick] (P1) -- (Q1);
    % \draw[thick] (P2) -- (Q2);

    % \node[orange] at (Q1) {\textbullet};
    % \node[orange] at (Q2) {\textbullet};

    % \draw[ultra thick, blue, <->] (O) -- (L);

  \end{tikzpicture}
\]

\begin{genbox}{The Orthogonal Distance from $\vv{v}$ to $\vv{w}$}
  The \emph{orthogonal distance} from $\vv{v}$ to $\vv{w}$ is
  $\norm{\vv{v}-\proj_{\vv{w}}(\vv{v})}$.
\end{genbox}

\begin{sagesilent}
  v = vector([1, -2, -1])
  w = vector([2, -4, 1])
\end{sagesilent}
\begin{example}
  Consider the vectors $\vv{v}$ and $\vv{w}$ given by $\vv{v}=\sage{v}$ and
  $\vv{w}=\sage{w}$. The projection of $\vv{v}$ onto $\vv{w}$ is given by
  \[
    \proj_{\vv{w}}(\vv{v})
    = \oldfrac{\vv{v}\cdot\vv{w}}{\vv{w}\vv{w}}\vv{w}
    = \oldfrac{\sage{v}\cdot\sage{w}}{\sage{w}\sage{w}}\sage{w}
    = \oldfrac{\sage{v*w}}{\sage{w*w}}\sage{w}
  \]
  The \emph{orthogonal distance} from $\vv{v}$ to $\vv{w}$ is then
  \[
    \norm{\vv{v}-\proj_{\vv{w}}(\vv{v})}
    = \norm*{\sage{v}-\oldfrac{\sage{v*w}}{\sage{w*w}}\sage{w}}
    = \norm*{\sage{v-(v*w)/(w*w)*w}}
    = \sage{(v-(v*w)/(w*w)*w).norm()}
  \]
\end{example}

Orthogonal distance is the key tool used to define the \emph{orthogonal error}
in using a line $f(x)=m\cdot x$ to approximate centered data.

\begin{sagesilent}
  var('x m')
  w = vector([1, m])
\end{sagesilent}
\newcommand{\myTmp}{$f(x)=m\cdot x$}
\begin{genbox}{The Orthogonal Error in Approximating Centered Data by \myTmp}
  Consider the problem of using a line $f(x)=m\cdot x$ to approximate a centered
  data set, whose points are listed as vectors
  $\Set{\vv*{p}{1},\vv*{p}{2}, \dotsc,\vv*{p}{n}}$. The \emph{orthogonal error}
  in using $f(x)=m\cdot x$ to approximate the data set is the sum of the squares
  of the orthogonal distances from each of these points to the vector
  $\vv{w}=\sage{w}$. That is, the orthogonal error is
  \newcommand{\orthdist}[2]{\norm{#1-\proj_{#2}(#1)}}
  \[
    E
    = \orthdist{\vv*{p}{1}}{\vv{w}}^2+\orthdist{\vv*{p}{2}}{\vv{w}}^2+\dotsb+\orthdist{\vv*{p}{n}}{\vv{w}}^2
  \]
\end{genbox}

\begin{sagesilent}
  A = matrix([(-1, 4), (0, -2), (1, -2)])
  p1, p2, p3 = A.rows()
  t1, t2, t3 = [tuple(p) for p in (p1, p2, p3)]
  var('x')
  m = -3
  f = m*x
  w = vector([1, m])
  proj = lambda v, w: (w*v)/(w*w)*w
  orthdist = lambda v, w: (v-proj(v, w)).norm()
\end{sagesilent}
\begin{example}
  Consider the problem of using the line $f(x)=\sage{f}$ to approximate the data
  set $\Set{\sage{t1}, \sage{t2}, \sage{t3}}$. To compute the orthogonal error,
  let $\vv{w}=\sage{w}$. The projections of each point onto $\vv{w}$ are given
  by
  \begin{align*}
    \proj_{\vv{w}}(\vv*{p}{1}) &= \sage{proj(p1, w)} & \proj_{\vv{w}}(\vv*{p}{2}) &= \sage{proj(p2, w)} & \proj_{\vv{w}}(\vv*{p}{3}) &= \sage{proj(p3, w)}
  \end{align*}
  The orthogonal distances from each point to $\vv{w}$ are
  \begin{align*}
    \norm{\vv*{p}{1}-\proj_{\vv{w}}(\vv{p}{1})} &= \sage{orthdist(p1, w)} & \norm{\vv*{p}{2}-\proj_{\vv{w}}(\vv*{p}{2})} &= \sage{orthdist(p2, w)} & \norm{\vv*{p}{3}-\proj_{\vv{w}}(\vv*{p}{3})} &= \sage{orthdist(p3, w)}
  \end{align*}
  The orthogonal error is then
  \newcommand{\orthdst}[1]{\norm{\vv*{p}{#1}-\proj_{\vv{w}}(\vv{p}{#1})}}
  \begin{align*}
    E
    &= \orthdst{1}^2 + \orthdst{2}^2 + \orthdst{3}^2 \\
    &= \pair*{\sage{orthdist(p1, w)}}^2+\pair*{\sage{orthdist(p2, w)}}^2+\pair*{\sage{orthdist(p3, w)}}^2 \\
    &= \sage{orthdist(p1, w)**2+orthdist(p2, w)**2+orthdist(p3, w)**2}
  \end{align*}
  We may geometrically visualize each term in the orthogonal error in the
  following diagram
  \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=1]

      \draw[ultra thick, <->] (-2, 0) -- (2, 0);
      \draw[ultra thick, <->] (0, -4) -- (0, 5);

      \coordinate (p1) at (-1, 4);
      \coordinate (p2) at (0, -2);
      \coordinate (p3) at (1, -2);

      \draw[ultra thick, red] (p1) -- (-13/10, 39/10);
      \draw[ultra thick, red] (p2) -- (3/5, -9/5);
      \draw[ultra thick, red] (p3) -- (7/10, -21/10);

      \node[orange] at (p1) {\textbullet};
      \node[orange] at (p2) {\textbullet};
      \node[orange] at (p3) {\textbullet};

      % \node at (p1) [below] {$(1, -1)$};
      % \node at (p2) [above] {$(2, 2)$};
      % \node at (p3) [left] {$(0, 6)$};
      % \node at (p4) [left] {$(0, 3)$};
      % \node at (p5) [above] {$(1, 4)$};

      \draw[very thick, <->, domain=-3/2:4/3, smooth, variable=\x, blue]
      plot ({\x},{-3*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

    \end{tikzpicture}
  \]
\end{example}


\subsection{Orthogonal Regression}

We are now equipped with the tools necessary to develop the technique of
\emph{orthogonal regression}. While linear regression with least squares
produces the line that minimizes the \emph{vertical error}, \emph{orthogonal
  regression} produces the line that minimizes the \emph{orthogonal error}.

\begin{genbox}{Goal of Orthogonal Regression}
  Given a data set, find the line that minimizes the \emph{orthogonal error}.
\end{genbox}

The language of eigenvalues and eigenvectors is required to acheive this
goal. Recall that the \emph{covariance matrix} of $n$ data points stored in the
rows of a matrix $A$ is defined as
$\displaystyle\Sigma=\frac{1}{n-1}A_c^\intercal A_c$. The covariance matrix
$\Sigma$ is a \emph{positive definite} matrix, meaning that $\Sigma$ is
\emph{symmetric} and that each of the eigenvalues of $\Sigma$ is a positive
number. Also recall that an \emph{eigenpair} of $\Sigma$ is a pair
$(\lambda, \vv{v})$ consisting of an eigenvalue $\lambda$ of $\Sigma$ and an
eigenvector $\vv{v}$ of $\Sigma$ associated to the eigenvalue $\lambda$. It is
customary to list the eigenvalues of $\Sigma$ in \emph{decreasing order}
\[
  \lambda_1 \geq \lambda _2\geq\dotsb\geq \lambda_k
\]
This means that $\lambda_1$ is the largest eigenvalue of $\Sigma$.  An eigen
pair $(\lambda_i, \vv{v})$ of $\Sigma$ is called an \emph{$i$th principal
  component} of $\Sigma$. Since $\lambda_1$ is the largest eigenvalue of
$\Sigma$, an eigenpair $(\lambda_1, \vv*{v}{1})$ is called a \emph{first
  principal component}.

\begin{genbox}{The First Principal Component}
  Suppose that $\lambda_1$ is the largest eigenvalue of the covariance matrix
  $\Sigma$. A \emph{first principal component} of $\Sigma$ is an eigenpair
  $(\lambda_1, \vv*{v}{1})$ of $\Sigma$.
\end{genbox}

In a first principal component $(\lambda_1, \vv*{v}{1})$, the vector $\vv{v}$
points in the direction for which variance in the data is maximal. According to
the spectral theorem, a vector $\vv*{v}{2}$ in a second principal component
$(\lambda_2, \vv*{v}{2})$ is orthogonal to $\vv*{v}{1}$.

It turns out that the first principal component possesses all the information
needed to construct the line that minimizes the orthogonal error.

\begin{sagesilent}
  var('x y')
  v = vector([x, y])
\end{sagesilent}
\begin{genbox}{The Technique of Orthogonal Regression}
  Let $A$ be a matrix whose rows consist of data points. Let
  $(\lambda_1, \vv*{v}{1})$ be a first principal component of the covariance
  matrix $\Sigma$. Write the vector $\vv*{v}{1}$ in coordinates
  $\vv*{v}{1}=\sage{v}$ and define $m=\frac{y}{x}$. Then the line
  $f(x)=m\cdot x$ minimizes the orthogonal error.
\end{genbox}

The following example demonstrates how to construct the line that minimizes the
orthogonal error.

\begin{sagesilent}
  A = matrix([(5, 16), (2, 8), (-1, 9), (5, 13), (7, 16), (8, 18), (0, 9), (1, 6), (-5, 2), (-2, 13)])
  c1, c2 = A.columns()
  n = A.nrows()
  M = matrix.column([[mean(c1)] * n, [mean(c2)] * n])
  Ac = A-M
  S = Ac.T*Ac
  H = S / (n-1)
  l2, l1 = H.eigenvalues()
  l1 = round(l1, 3)
  l2 = round(l2, 3)
  t2, t1 = H.right_eigenvectors()
  _, l, _ = t1
  x, y = l[0]
  x = round(x, 3)
  y = round(y, 3)
  v = vector([x, y])
  m = y/x
\end{sagesilent}
\begin{example}\label{ex:orth}
  The figure below depicts the data points stored in the rows of the matrix $A$.
    \newcommand{\myData}{
    \begin{array}{c}
      \begin{tikzpicture}[line cap=round, line join=round, scale=1/3]

        \draw[ultra thick, <->] (-6, 0) -- (9, 0);
        \draw[ultra thick, <->] (0, -1) -- (0, 19);

        \coordinate (p1) at (5, 16);
        \coordinate (p2) at (2, 8);
        \coordinate (p3) at (-1, 9);
        \coordinate (p4) at (5, 13);
        \coordinate (p5) at (7, 16);
        \coordinate (p6) at (8, 18);
        \coordinate (p7) at (0, 9);
        \coordinate (p8) at (1, 6);
        \coordinate (p9) at (-5, 2);
        \coordinate (p10) at (-2, 13);

        \node[orange] at (p1) {\textbullet};
        \node[orange] at (p2) {\textbullet};
        \node[orange] at (p3) {\textbullet};
        \node[orange] at (p4) {\textbullet};
        \node[orange] at (p5) {\textbullet};
        \node[orange] at (p6) {\textbullet};
        \node[orange] at (p7) {\textbullet};
        \node[orange] at (p8) {\textbullet};
        \node[orange] at (p9) {\textbullet};
        \node[orange] at (p10) {\textbullet};

        % \draw[very thick, <->, domain=-6:3, smooth, variable=\x, blue]
        % plot ({\x},{39/38*\x + 345/38}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{align*}
    A &= \sage{A} & \myData
  \end{align*}
  The covariance matrix is
  $\displaystyle\Sigma=\oldfrac{1}{\sage{n-1}}\sage{S}$. The eigenvalues of
  $\Sigma$ are $\lambda_1\approx\sage{l1}$ and $\lambda_2\approx\sage{l2}$. A
  first principal component is given by $(\lambda_1,\vv*{v}{1})$ where
  $\vv*{v}{1}\approx \sage{v}$. We use the coordinates of $\vv*{v}{1}$ to define
  $m=\nicefrac{\sage{y}}{\sage{x}}=\sage{m}$. The line that minimizes the
  orthogonal distance to the centered data is then $f(x)=\sage{m}\,x$. This line
  and the centered data is depicted below.
  \[
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3]

        \draw[ultra thick, <->] (-7, 0) -- (7, 0);
        \draw[ultra thick, <->] (0, -10) -- (0, 8);

        \coordinate (p1) at ( 3,  5);
        \coordinate (p2) at ( 0, -3);
        \coordinate (p3) at (-3, -2);
        \coordinate (p4) at ( 3,  2);
        \coordinate (p5) at ( 5,  5);
        \coordinate (p6) at ( 6,  7);
        \coordinate (p7) at (-2, -2);
        \coordinate (p8) at (-1, -5);
        \coordinate (p9) at (-7, -9);
        \coordinate (p10) at (-4,  2);

        \node[orange] at (p1) {\textbullet};
        \node[orange] at (p2) {\textbullet};
        \node[orange] at (p3) {\textbullet};
        \node[orange] at (p4) {\textbullet};
        \node[orange] at (p5) {\textbullet};
        \node[orange] at (p6) {\textbullet};
        \node[orange] at (p7) {\textbullet};
        \node[orange] at (p8) {\textbullet};
        \node[orange] at (p9) {\textbullet};
        \node[orange] at (p10) {\textbullet};

        \draw[very thick, <->, domain=-7:7, smooth, variable=\x, blue]
        plot ({\x},{1.253477058174919*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

      \end{tikzpicture}
  \]
\end{example}

The method of orthogonal regression offers a more sophisticated alternative to
the method of least squares. The figure below depicts the centered data set from
Example \ref{ex:orth} along with the line minimizing the orthogonal error (blue)
and the line minimizing the vertical error (green).
\[
    \begin{tikzpicture}[line cap=round, line join=round, scale=1/3]

        \draw[ultra thick, <->] (-7, 0) -- (7, 0);
        \draw[ultra thick, <->] (0, -10) -- (0, 8);

        \coordinate (p1) at ( 3,  5);
        \coordinate (p2) at ( 0, -3);
        \coordinate (p3) at (-3, -2);
        \coordinate (p4) at ( 3,  2);
        \coordinate (p5) at ( 5,  5);
        \coordinate (p6) at ( 6,  7);
        \coordinate (p7) at (-2, -2);
        \coordinate (p8) at (-1, -5);
        \coordinate (p9) at (-7, -9);
        \coordinate (p10) at (-4,  2);

        \node[orange] at (p1) {\textbullet};
        \node[orange] at (p2) {\textbullet};
        \node[orange] at (p3) {\textbullet};
        \node[orange] at (p4) {\textbullet};
        \node[orange] at (p5) {\textbullet};
        \node[orange] at (p6) {\textbullet};
        \node[orange] at (p7) {\textbullet};
        \node[orange] at (p8) {\textbullet};
        \node[orange] at (p9) {\textbullet};
        \node[orange] at (p10) {\textbullet};

        \draw[very thick, <->, domain=-7:7, smooth, variable=\x, blue]
        plot ({\x},{1.253477058174919*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

        \draw[very thick, <->, domain=-7:7, smooth, variable=\x, blue]
        plot ({\x},{1.253477058174919*\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

        \draw[very thick, <->, domain=-7:7, smooth, variable=\x, green]
        plot ({\x},{\x}); % node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

      \end{tikzpicture}
\]
While both of these lines approximate the data, the line produced by orthogonal
regression is considered the ``best'' possible approximation.

\end{document}
