\documentclass[8pt]{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
% \usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing, positioning, quotes, angles}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%



\title{Scalar and Vector Projections}
\subtitle{MIDS Linear Algebra Review}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}

\section{Motivation: Least Squares Analysis}

\begin{frame}

  \frametitle{\secname}

  \begin{block}{Problem}
    Given data points $(x_1,y_1),(x_2, y_2),\dotsc,(x_n,y_n)$. %
    \onslide<4->{How can we find the ``line of best fit'' to this data?}
    \[
      \begin{tikzpicture}[line join=round, line cap=round, scale=1/4]
        \foreach \Point in {
          (8, 5),
          (8.5, 6),
          (9, 6),
          (13, 7),
          (14, 7),
          (16, 8),
          (17.5,8),
          (19,9),
          (21.5,9),
          (25,10),
          (28, 10.5),
          (30, 11.5),
          (31, 12.5),
          (33, 11.5),
          (33.5, 12),
          (34, 12)
        }{
          \onslide<3->{
            \node [red] at \Point {\textbullet};
          }
        }

        \onslide<2->{
          \draw[ultra thick, <->] (-3, 0) -- (40, 0) node [right] {$x$};
          \draw[ultra thick, <->] (0, -3) -- (0, 15) node [above] {$y$};
        }

        \onslide<5->{
          \draw[very thick, <->, blue] (-3, 2.84207205944203) -- (40, 13.843754381045843);
        }
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}

  \begin{block}{MLB Average Ticket Price from 2006 to 2018}
    \onslide<2->{The formula for the ``line of best fit'' is $f(x)=.793\,x-1568.701$.}
    \[
      \begin{tikzpicture}[line join=round, line cap=round, yscale=1/8, xscale=3/4]
        \foreach \Point in {
          (1, 22.21),
          (2, 22.77),
          (3, 25.43),
          (4, 26.64),
          (5, 26.74),
          (6, 26.91),
          (7, 26.98),
          (8, 27.48),
          (9, 27.93),
          (10, 29.94),
          (11, 31),
          (12, 32.44)
        }{
          \node [red] at \Point {\textbullet};
          % \node at \Point [below] {$x$};
        }

        \foreach \x in {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}{
          \pgfmathtruncatemacro{\myYear}{\x+2005}
          \pgfmathsetmacro{\xHigh}{10+3/8pt}
          \pgfmathsetmacro{\xLow}{10-3/8pt}
          \draw [ultra thick] (\x, \xHigh)--(\x,\xLow) node[below] {\tiny\myYear};
          % \node at \Point [below] {$x$};
        }

        \foreach \y in {15, 20, 25, 30, 35}{

          \pgfmathtruncatemacro{\myY}{\y}
          \draw [ultra thick] (9/4pt, \y) -- (-9/4pt, \y) node[left] {\tiny\myY};
          % \node at \Point [below] {$x$};
        }

        \draw[ultra thick, <->] (-4/6, 10) -- (13, 10) node [right] {Year};
        \draw[ultra thick, <->] (0, 5) -- (0, 40) node [above] {Price};

        \onslide<3->{
          \draw[very thick, <->, blue] (-4/6, 21.5198601398601) -- (13, 32.3628787878788);
        }

      \end{tikzpicture}
    \]

    \onslide<4->{
    The ``line of best fit'' can be used to predict the price of MLB tickets in 2018 as
    \[
      \textnormal{2018 Price}\approx f(2018) \approx 32.362
    \]
    }
  \end{block}


\end{frame}


\section{The Scalar Projection of $\vec{v}$ Along $\vec{w}$}

\begin{frame}
  \frametitle{\secname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{definition}
      \onslide<1->{Let $\vv{v},\vv{w}\in\mathbb{R}^n$.}%
      \onslide<4->{The \emph{scalar projection} or \emph{component} of $\vv{v}$
        along $\vv{w}$ is the depicted scalar.}
    \end{definition}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[line join=round, line cap=round]

        \pgfmathsetmacro{\va}{3}
        \pgfmathsetmacro{\vb}{2}
        \pgfmathsetmacro{\wa}{4}
        \pgfmathsetmacro{\wb}{-1}

        \pgfmathsetmacro{\wnorm}{sqrt(\wa*\wa+\wb*\wb)}
        \pgfmathsetmacro{\vdotw}{\va*\wa+\vb*\wb}
        \pgfmathsetmacro{\compwv}{\vdotw/\wnorm}

        \pgfmathsetmacro{\compvwa}{\vdotw/\wnorm*\wa/\wnorm}
        \pgfmathsetmacro{\compvwb}{\vdotw/\wnorm*\wb/\wnorm}

        \pgfmathsetmacro{\compnorm}{sqrt(\compvwa*\compvwa+\compvwa*\compvwa)}
        \pgfmathsetmacro{\compdiffnorm}{sqrt((\compvwa-\va)*(\compvwa-\va)+(\compvwb-\vb)*(\compvwb-\vb))}

        \pgfmathsetmacro{\boxlen}{1/5}

        \coordinate (O) at (0, 0);
        \coordinate (v) at (\va, \vb);
        \coordinate (w) at (\wa, \wb);
        \coordinate (what) at ($ (O)!1/\wnorm!(w) $);
        \coordinate (comp) at (\compvwa, \compvwb);

        \coordinate (R) at ($ (comp)-{\boxlen/\compnorm}*(comp)$);
        %\node at (R) {R};

        \coordinate (Q) at ($ (comp)+{\boxlen/\compdiffnorm}*(v)-{\boxlen/\compdiffnorm}*(comp)$);
        %\node at (Q) {Q};

        \coordinate (S) at ($ (R)-(comp)+(Q) $);
        %\node at (S) {S};

        \onslide<8->{
          \draw[black]
          (w) -- (O) -- (v)
          pic["$\theta$", thick, draw=black, <->, angle eccentricity=1.2, angle radius=1cm] {angle=w--O--v};
        }


        \onslide<5->{
          \draw[thick] (R) -- (S) -- (Q);
          \draw[thick, dashed] (v) -- (comp);
          \draw[very thick, decoration={brace, mirror, raise=5pt}, decorate]
          (O) --  node[below=10pt] {$\comp_{\vv{w}}(\vv{v})$} (comp);
        }

        \onslide<2->{\draw[ultra thick, blue, ->] (O) -- (v) node[above] {$\vv{v}$};}
        \onslide<3->{\draw[ultra thick, violet, ->] (O) -- (w) node[right] {$\vv{w}$};}

        % \draw[ultra thick, orange, ->] (O) -- (comp);


      \end{tikzpicture}
    \]
  \end{columns}

  \begin{block}{\onslide<6->{Formula}}
    \onslide<7->{To compute $\comp_{\vv{w}}(\vv{v})$, note that
      $\displaystyle
      \cos\theta=}\onslide<9->{\frac{\comp_{\vv{w}}(\vv{v})}{\norm{\vv{v}}}$.}
    \onslide<10->{Hence}
    \[
      \onslide<10->{\comp_{\vv{w}}(\vv{v}) =}
      \onslide<11->{\norm{\vv{v}}\cos\theta =}
      \onslide<12->{\norm{\vv{v}}\cdot\frac{\vv{v}\cdot\vv{w}}{\norm{\vv{v}}\cdot\norm{\vv{w}}} =}
      \onslide<13->{\frac{\vv{v}\cdot\vv{w}}{\norm{\vv{w}}} =}
      \onslide<14->{\vv{v}\cdot\widehat{w}}
    \]
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\langle0, 2, -3\rangle$ and $\vv{w}=\langle3, 5, -6\rangle$. \pause Then
    \[
      \comp_{\vv{w}}(\vv{v})
      = \pause\frac{\vv{v}\cdot\vv{w}}{\norm{\vv{w}}}
      = \pause\frac{(0)(3)+(2)(5)+(-3)(-6)}{\sqrt{3^2+5^2+(-6)^2}}
      = b\pause\frac{28}{\sqrt{70}}
    \]
  \end{example}
\end{frame}

\end{document}
