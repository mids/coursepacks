\documentclass[12pt]{article}

\title{Unit 1.1: Scalars and Vectors}
\author{MIDS Linear Algebra Review}
\date{}


\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\maketitle


\tableofcontents


\newpage
\section{The Real Number Line $\mathbb{R}$}
\label{sec:numline}

The basic mathematical tool used for measurement is called a \emph{real
  number}. These are the numbers living on the \emph{real number line}, which
has a simple visual representation.
\begin{genbox}{The Real Number Line $\mathbb{R}$}
  \[
    \begin{tikzpicture}[line cap=round, line join=round, baseline=-.5ex]
      \draw[ultra thick, <->] (-6, 0) -- (6, 0);

      \draw[ultra thick] (0, 3pt) -- (0, -3pt) node [below] {$0$};
      \draw[ultra thick] (1, 3pt) -- (1, -3pt) node [below] {$1$};
      \draw[ultra thick] (-4, 3pt) -- (-4, -3pt) node [below] {$-17$};
      \draw[ultra thick] (3.1415, 3pt) -- (3.1415, -3pt) node [below] {$\pi$};
    \end{tikzpicture}
  \]
\end{genbox}
The symbol $\mathbb{R}$ is used to refer to ``all real numbers'' and these
numbers are often referred to as \emph{scalars}.  To efficiently communicate
whether or not a quantity is a scalar, mathematicians use \emph{set membership
  notation}.
\begin{genbox}{Set Membership Notation}%\setlength{\abovedisplayskip}{0}
  \noindent\begin{minipage}{.5\linewidth}
    \centering
    {\color{blue}$c\in\mathbb{R}$} means ``{\color{blue}$c$ \emph{is a real number}}''
  \end{minipage}%
  \begin{minipage}{.5\linewidth}
    \centering
    {\color{red}$c\notin\mathbb{R}$} means ``{\color{red}$c$ \emph{is not a real number}}''
  \end{minipage}
\end{genbox}
For example, the labelings on our depiction of the real number line above
indicate that
\begin{align*}
  -17 &\in \mathbb{R} & 0&\in\mathbb{R} & 1 &\in\mathbb{R} & \pi &\in\mathbb{R}
\end{align*}
On the other hand, the symbols $\int$ and $\Sigma$ refer to mathematical
\emph{operations}. These operations are not scalars and we therefore write
$\int\notin\mathbb{R}$ and $\Sigma\notin\mathbb{R}$. More information about set theory can be found here: \url{https://en.wikipedia.org/wiki/Set_theory}.

Our mathematical interest in scalars is directly linked to data
analysis. Indeed, the entries in a data set often consist of physical
measuements.
\begin{genbox}{Examples of Scalar Measurements}
  \begin{description}
  \item[{\color{blue}Length}] ``The Burj Khalifa is {\color{blue}$\SI{829.8}{\metre}$} tall.''
  \item[{\color{orange}Temperature}] ``The freezing point of nitrogen is
    {\color{orange}$\SI{-210}{\degreeCelsius}$}.''
  \item[{\color{purple}Time}] ``The running time of Stanley Kubrick's \emph{2001: A Space
      Odyssey} is {\color{purple}{$\SI{8520}{\second}$}}.''
  \item[{\color{olive}Area}] ``The land area of Durham, NC is
    {\color{olive}$\SI{278.1}{\km\squared}$}.''
  \end{description}
\end{genbox}



\section{Euclidean Space $\mathbb{R}^n$}
\label{sec:eucsp}

\begin{sagesilent}
  scores = vector([58, 64, 72, 79, 83, 88, 95])
\end{sagesilent}
A single scalar measurement is rarely sufficient for data analysis. Consider an
example involving exam scores.

\begin{example}\label{ex:exam}
  Suppose that the scores on a midterm in a course with seven students are
  organized in the list
  \[
    \textnormal{scores}=\sage{scores}
  \]
  A student sees she scored 79 on the exam, but she does not know if that is a
  good score. To better understand how she performed on this exam, the student
  may be interested in the \emph{mean} of this data
  \[
    \textnormal{mean}
    = \frac{58+64+72+79+83+88+95}{7}=\oldfrac{\sage{sum(scores)}}{7}
    = \sage{sum(scores) / len(scores)}
  \]
  So, a student with a score below $77$ performed \emph{below average} and a
  student with a score above $77$ performed \emph{above average}.  To evaluate
  how a student performed on an exam relative to the average, one must have
  access to the entire \emph{list of scalars}.
\end{example}
The mathematical tool used for organizing lists of scalars is called a
\emph{vector}. We will use two notational conventions for defining vectors.
\newcommand{\myHorizV}{\langle x_1, x_2, \dotsc, x_n\rangle}
\newcommand{\myVertV}{ \left[
    \begin{array}{c}
      x_1 \\ x_2\\ \vdots \\ x_n
    \end{array}
  \right]
}
\begin{genbox}{Vector Notation}
  \noindent
  \begin{minipage}{.5\linewidth}
    \centering
    % \textbf{Vertical notation:} $$
    \captionof*{figure}{Vertical Notation}
    \[
      \vv{v}=\myVertV
    \]
  \end{minipage}%
  \begin{minipage}{.5\linewidth}
    \centering
    \captionof*{figure}{Horizontal Notation}
    \[
      \vv{v}=\myHorizV
    \]
  \end{minipage}
\end{genbox}
Vertical notation is useful when vertical writing space is plentiful and for
when one wishes to think of a vector as a ``$n\times
1$ matrix,'' which we will discuss later in this unit. Horizontal notation is
useful when vertical writing space is scarce. In both cases, we typically
indicate that a variable is a vector by placing an arrow above a lowercase
letter.

The symbol $\mathbb{R}^n$ is used to refer to ``all vectors with
$n$ entries'' and is called \emph{Euclidean
  $n$-space}. More information about Euclidean spaces can be found here:
\url{https://en.wikipedia.org/wiki/Euclidean_space}.

% To efficiently communicate whether or not a symbol is a vector, mathematicians
% use the following notational conventions
% \begin{description}
% \item[{\color{blue}$x\in\mathbb{R}^n$}] means ``$x$ \emph{is a vector with $n$ entries}''
% \item[{\color{blue}$x\notin\mathbb{R}^n$}] means ``$x$ \emph{is not a vector with $n$ entries}''
% \end{description}


\begin{sagesilent}
  a = vector([2, -8, -357])
  b = matrix.column([2, -67/3])
  c = random_matrix(ZZ, 7, 1)
  v = matrix.column(a)
  w = vector(b)
  x = vector(c)
\end{sagesilent}
\begin{example}\label{ex:vecmem}
  Consider the vectors
  \begin{align*}
    \vv{a} &= \sage{a} & \vv{b} &= \sage{b} & \vv{c} &= \sage{c} & \vv{v} &= \sage{v}
  \end{align*}
  These vectors satisfy
  \begin{align*}
    \vv{a} &\in\mathbb{R}^{\sage{len(a)}} & \vv{b} &\in\mathbb{R}^{\sage{b.nrows()}} & \vv{c} &\in\mathbb{R}^{\sage{c.nrows()}} & \vv{v} &\in\mathbb{R}^{\sage{v.nrows()}}
  \end{align*}
  Consequently,
  \begin{align*}
    \vv{a} &\notin\mathbb{R}^{2} & \vv{b} &\notin\mathbb{R}^{3} & \vv{c} &\notin\mathbb{R}^{16} & \vv{v} &\notin\mathbb{R}^{9}
  \end{align*}
\end{example}

A generic element of $\mathbb{R}^n$ is of the form $\vv{v}=\myHorizV$. The
scalars $x_1,x_2,\dotsc,x_n$ are called the \emph{coordinates} of $\vv{v}$. Two
vectors are equal if they have the same coordinates, so $\vv{a}=\vv{v}$ in
Example \ref{ex:vecmem} above.

In applied situations, many of the techniques you will be learning about will
represent each variable you are examining as a dimension. So if you are
examining a data set with six variables of interest, such techniques would
describe the data in six-dimensional space.

\begin{example}
  The table below organizes the opening prices of ten stocks on the New York
  Stock Exchange on 28-August 2017.
  \begin{center}
    \begin{tabular}{lr}
      Description & Open\\
      \hline
      CHESAPEAKE ENERGY CORP & 3.79\\
      BANK AMER CORP & 23.77\\
      FORD MTR CO DEL & 10.82\\
      VALE S A & 10.68\\
      KROGER CO & 21.7399\\
      ALIBABA GROUP HLDG LTD & 171.74\\
      SNAP INC & 14.78\\
      GENERAL ELECTRIC CO & 24.49\\
      WHITING PETE CORP NEW & 4.41\\
      GAMESTOP CORP NEW & 19.40\\
    \end{tabular}
  \end{center}
  These opening prices can be thought of as vector in $\mathbb{R}^{10}$
  \[
    \langle3.79, 23.77, 10.82, 10.68, 21.7399, 171.74, 14.78, 24.49,
    4.41, 19.40\rangle
  \]
  The NYSE carries roughly $2400$ stocks. Thus, on any given day, the list of
  all opening prices of these stocks on the NYSE is an element of
  $\mathbb{R}^{2400}$!
\end{example}


\section{Geometric Interpretations of Vectors}
\label{sec:geom-vec}

In the example above, we saw that examining the data set describing the New York
Stock Exchange involved working with a $2400$-dimensional space.  To better
understand how to work with vectors describing high-dimensional data sets, it is
useful to derive intuition from the geometry of $\mathbb{R}^2$ and
$\mathbb{R}^3$.

One may ``visualize'' $\mathbb{R}^2$ as the familiar \emph{$xy$-plane}
\[
  \begin{tikzpicture}[line cap=round, line join=round]
    \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
    \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};

    % \draw[thick, blue, dashed] (1, 0) -- (1, 2);
    % \draw[thick, blue, dashed] (0, 2) -- (1, 2);
    % \draw[ultra thick] (1, 3pt) -- (1, -3pt) node [below] {$1$};
    % \draw[ultra thick] (3pt, 2) -- (-3pt, 2) node [left] {$2$};
    % \node[label={right:$P=(1,2)$}] at (1, 2) {\textbullet};

    % \draw[thick, blue, dashed] (-3, 0) -- (-3, -2);
    % \draw[thick, blue, dashed] (0, -2) -- (-3, -2);
    % \draw[ultra thick] (-3, 3pt) -- (-3, -3pt) node [below, fill=white] {$-3$};
    % \draw[ultra thick] (3pt, -2) -- (-3pt, -2) node [left, fill=white] {$-2$};
    % \node[label={below:$Q=(-3,-2)$}] at (-3, -2) {\textbullet};

    % \node[label={above left: $O$}] at (0, 0) {\textbullet};
  \end{tikzpicture}
\]
Geometrically, we interpret a vector $\vv{v}=\langle x, y\rangle$ in
$\mathbb{R}^2$ as an arrow connecting the \emph{origin} $(0, 0)$ to the
location in $\mathbb{R}^2$ with coordinates $(x, y)$.

\begin{sagesilent}
  v2 = vector([1, 2])
  w2 = vector([-3, -2])
\end{sagesilent}
\begin{example}\label{ex:dim2}
  The figure below depicts the vectors $\vv{v}=\sage{v2}$ and $\vv{w}=\sage{w2}$
  \[
    \begin{tikzpicture}[line cap=round, line join=round]

      \draw[very thick, blue, ->] (0, 0) -- (1, 2) node [right] {$\vv{v}=\sage{v2}$};
      \draw[very thick, purple, ->] (0, 0) -- (-3, -2) node [below] {$\vv{w}=\sage{w2}$};

      \draw[ultra thick, <->] (-4, 0) -- (4, 0) node [right] {$x$};
      \draw[ultra thick, <->] (0, -3) -- (0, 3) node [above] {$y$};


      % \draw[thick, blue, dashed] (1, 0) -- (1, 2);
      % \draw[thick, blue, dashed] (0, 2) -- (1, 2);
      \draw[ultra thick] (1, 3pt) -- (1, -3pt) node [below] {$1$};
      \draw[ultra thick] (3pt, 2) -- (-3pt, 2) node [left] {$2$};
      % \node[label={right:$\vv{v}=\sage{v2}$}] at (1, 2) {\textbullet};

      % \draw[thick, blue, dashed] (-3, 0) -- (-3, -2);
      % \draw[thick, blue, dashed] (0, -2) -- (-3, -2);
      \draw[ultra thick] (-3, 3pt) -- (-3, -3pt) node [below] {$-3$};
      \draw[ultra thick] (3pt, -2) -- (-3pt, -2) node [left] {$-2$};
      % \node[label={below:$Q=(-3,-2)$}] at (-3, -2) {\textbullet};

      % \node[label={above left: $O$}] at (0, 0) {\textbullet};
    \end{tikzpicture}
  \]
\end{example}

To ``visualize'' $\mathbb{R}^3$, one starts with the $xy$-plane and adds a third
``$z$-axis'' to create \emph{$xyz$-space}
\[
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[tdplot_main_coords, scale=3/2]
    \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
    \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
    \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

    % \draw[] (1.5, 0, .1pt) -- (1.5, 0, -.1pt) node [below] {\tiny{$3$}};
    % \draw[] (0, 1, .1pt) -- (0, 1, -.1pt) node [below] {\tiny{$3$}};
    % \draw[] (0, .1pt, -3/2) -- (0, -.1pt, -3/2) node [left] {\tiny{$-4$}};
    % \draw[thick, blue, dashed] (3/2, 0, 0) -- (3/2, 1, 0);
    % \draw[thick, blue, dashed] (0, 1, 0) -- (3/2, 1, 0);
    % \draw[thick, blue, dashed] (3/2, 1, 0) -- (3/2, 1, -3/2);
    % \node[label={right:\tiny{$P=(3,3,-4)$}}] at (3/2, 1, -3/2) {\textbullet};

    % \draw[] (-2, 0, .1pt) -- (-2, 0, -.1pt) node [below] {\tiny{$-4$}};
    % \draw[] (0, 4/3, .1pt) -- (0, 4/3, -.1pt) node [below] {\tiny{$4$}};
    % \draw[] (0, .1pt, 5/3) -- (0, -.1pt, 5/3) node [left] {\tiny{$5$}};
    % \draw[thick, blue, dashed] (-2, 0, 0) -- (-2, 4/3, 0);
    % \draw[thick, blue, dashed] (0, 4/3, 0) -- (-2, 4/3, 0);
    % \draw[thick, blue, dashed] (-2, 4/3, 0) -- (-2, 4/3, 5/3);
    % \node[label={right:\tiny{$P=(-4,4,5)$}}] at (-2, 4/3, 5/3) {\textbullet};
  \end{tikzpicture}
\]
Geometrically, we interpret a vector $\vv{v}=\langle x, y, z\rangle$ in
$\mathbb{R}^3$ as an arrow connecting the \emph{origin} $(0, 0, 0)$ to the
location in $\mathbb{R}^3$ with coordinates $(x, y, z)$.

\begin{sagesilent}
  v3 = vector([3, 3, -4])
  w3 = vector([-4, 4, 5])
\end{sagesilent}
\begin{example}\label{ex:dim3}
  The figure below depicts the vectors $\vv{v}=\sage{v3}$ and $\vv{w}=\sage{w3}$
  \[
    \tdplotsetmaincoords{70}{110}
    \begin{tikzpicture}[tdplot_main_coords, scale=3/2]

      \draw[very thick, ->, blue] (0, 0, 0) -- (3/2, 1, -3/2) node [right] {$\vv{v}=\sage{v3}$};
      \draw[very thick, ->, purple] (0, 0, 0) -- (-2, 4/3, 5/3) node [right] {$\vv{w}=\sage{w3}$};

      \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
      \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
      \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

      \draw[] (1.5, 0, .1pt) -- (1.5, 0, -.1pt) node [below] {\tiny{$3$}};
      \draw[] (0, 1, .1pt) -- (0, 1, -.1pt) node [below] {\tiny{$3$}};
      \draw[] (0, .1pt, -3/2) -- (0, -.1pt, -3/2) node [left] {\tiny{$-4$}};
      \draw[thick, blue, dashed] (3/2, 0, 0) -- (3/2, 1, 0);
      \draw[thick, blue, dashed] (0, 1, 0) -- (3/2, 1, 0);
      \draw[thick, blue, dashed] (3/2, 1, 0) -- (3/2, 1, -3/2);
      % \node[blue, label={right:\tiny{$\vv{v}=\sage{v3}$}, color:blue}] at (3/2, 1, -3/2) {};

      \draw[] (-2, 0, .1pt) -- (-2, 0, -.1pt) node [below] {\tiny{$-4$}};
      \draw[] (0, 4/3, .1pt) -- (0, 4/3, -.1pt) node [below] {\tiny{$4$}};
      \draw[] (0, .1pt, 5/3) -- (0, -.1pt, 5/3) node [left] {\tiny{$5$}};
      \draw[thick, purple, dashed] (-2, 0, 0) -- (-2, 4/3, 0);
      \draw[thick, purple, dashed] (0, 4/3, 0) -- (-2, 4/3, 0);
      \draw[thick, purple, dashed] (-2, 4/3, 0) -- (-2, 4/3, 5/3);
      % \node[label={right:\tiny{$P=(-4,4,5)$}}] at (-2, 4/3, 5/3) {\textbullet};
    \end{tikzpicture}
  \]
\end{example}

Our geometric interpretation of vectors as arrows conveniently allows us to use
the Euclidean distance formula to define the \emph{length} of a vector.


\begin{genbox}{The Vector Length Formula}
  The \emph{length} of $\vv{v}=\langle v_1, v_2, \dotsc, v_n\rangle$, denoted by
  ``double bar notation'' $\norm{\vv{v}}$, is
  \[
    \norm{\vv{v}}=\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}
  \]
  Note that $\norm{\vv{v}}$ is a \emph{scalar}.
\end{genbox}

The terms \emph{length} and \emph{magnitude} are used interchangeably.


\begin{example}
  The vectors $\vv{v}=\sage{v2}$ and $\vv{w}=\sage{w2}$ from Example
  \ref{ex:dim2} above have lengths
  \begin{align*}
    \norm{\vv{v}} &= \norm{\sage{v2}}                         & \norm{\vv{w}} &= \norm{\sage{w2}} \\
                  &= \sqrt{(\sage{v2[0]})^2+(\sage{v2[1]})^2} &               &= \sqrt{(\sage{w2[0]})^2+(\sage{w2[1]})^2} \\
                  &= \sqrt{\sage{v2[0]**2}+\sage{v2[1]**2}}   &               &= \sqrt{\sage{w2[0]**2}+\sage{w2[1]**2}} \\
                  &= \sage{v2.norm()}                         &               &= \sage{w2.norm()}
  \end{align*}
\end{example}

\begin{example}
  The vectors $\vv{v}=\sage{v3}$ and $\vv{w}=\sage{w3}$ from Example
  \ref{ex:dim3} above have magnitudes
  \begin{align*}
    \norm{\vv{v}} &= \norm{\sage{v3}}                                          & \norm{\vv{w}} &= \norm{\sage{w3}} \\
                  &= \sqrt{(\sage{v3[0]})^2+(\sage{v3[1]})^2+(\sage{v3[2]})^2} &               &= \sqrt{(\sage{w3[0]})^2+(\sage{w3[1]})^2+(\sage{w3[2]})^2} \\
                  &= \sqrt{\sage{v3[0]**2}+\sage{v3[1]**2}+\sage{v3[2]**2}}    &               &= \sqrt{\sage{w3[0]**2}+\sage{w3[1]**2}+\sage{w3[2]**2}} \\
                  &= \sage{v3.norm()}                                          &               &= \sage{w3.norm()}
  \end{align*}
\end{example}
Notice that the length of a vector is defined for any value of $n$. This is our
first example of using the geometry of low-dimensions to \emph{define} geometry
in high dimensions.

\begin{sagesilent}
  a = vector([2, -4, -7, 0, 3])
\end{sagesilent}
\begin{example}
  Consider the vector $\vv{a}\in\mathbb{R}^{\sage{len(a)}}$ given by
  $\vv{a}=\sage{a}$. The magnitude of $\vv{a}$ is
  \newcommand{\myTerm}[1]{(\sage{a[#1]})^2}
  \newcommand{\myTermEval}[1]{\sage{a[#1]**2}}
  \begin{align*}
    \norm{\vv{a}}
    &= \sqrt{\myTerm{0}+\myTerm{1}+\myTerm{2}+\myTerm{3}+\myTerm{4}} \\
    &= \sqrt{\myTermEval{0}+\myTermEval{1}+\myTermEval{2}+\myTermEval{3}+\myTermEval{4}} \\
    &= \sage{a.norm()}
  \end{align*}
\end{example}



% \section{Exercises}
% \label{sec:ex}

% \begin{sagesilent}
%   set_random_seed(404344)
%   c1 = ZZ.random_element(x=-1000, y=1000)
%   c2 = ZZ.random_element(x=-1000, y=1000)
%   v1 = random_vector(ZZ, 2)
%   v2 = random_vector(ZZ, 3)
%   v3 = random_matrix(ZZ, 4, 1)
%   v4 = random_matrix(ZZ, 5, 1)
% \end{sagesilent}
% \begin{exercise}
%   Let
%   \begin{align*}
%     c &= \sage{c1} & d &= \sage{c2} & \vv{v} &= \sage{v1} & \vv{w} &= \sage{v2} & \vv{x} &= \sage{v3} & \vv{y} &= \sage{v4}
%   \end{align*}
%   \begin{probparts}
%   \item Which (if any) of $c$, $d$, $\vv{v}$, $\vv{w}$, $\vv{x}$, and $\vv{y}$
%     is a \emph{scalar}?
%   \item Which (if any) of $c$, $d$, $\vv{v}$, $\vv{w}$, $\vv{x}$, and $\vv{y}$
%     is a \emph{vector}?
%   \item Which (if any) of $c$, $d$, $\vv{v}$, $\vv{w}$, $\vv{x}$, and $\vv{y}$
%     is a vector in $\mathbb{R}^2$?
%   \item Which (if any) of $c$, $d$, $\vv{v}$, $\vv{w}$, $\vv{x}$, and $\vv{y}$
%     is a vector in $\mathbb{R}^3?$
%   \item Which (if any) of $c$, $d$, $\vv{v}$, $\vv{w}$, $\vv{x}$, and $\vv{y}$
%     is a vector in $\mathbb{R}^4?$
%   \end{probparts}
% \end{exercise}


% \begin{sagesilent}
%   set_random_seed(1891310)
%   a = random_matrix(ZZ, 2, 1)
%   b = random_vector(ZZ, 3)
%   c = random_matrix(ZZ, 4, 1)
%   d = random_matrix(ZZ, 5, 1)
% \end{sagesilent}
% \begin{exercise}\label{ex:norms}
%   Consider the vectors
%   \begin{align*}
%     \vv{a} &= \sage{a} & \vv{b} &= \sage{b} & \vv{c} &= \sage{c} & \vv{d} &= \sage{d}
%   \end{align*}
%   Find the lengths of $\vv{a}$, $\vv{b}$, $\vv{c}$, and $\vv{d}$.
% \end{exercise}

% \begin{exercise}
%   In this exercise, we will use the computer algebra system \texttt{sage} to
%   check our answers to Exercise \ref{ex:norms} above.
%   \begin{enumerate}
%   \item Navigate to \url{https://sagecell.sagemath.org/}.
%   \item Type the following into the code box
%     \begin{sageverbatim}
%       a = vector([-1, -1])
%       a.norm()
%     \end{sageverbatim}
%   \item Click the ``evaluate'' button.
%   \end{enumerate}
%   The output of this code is \texttt{sqrt(2)}, indicating that
%   $\norm{\vv{a}}=\sqrt{2}$. Check your other answers to Exercise
%   \ref{ex:norms} using \texttt{sage}.
% \end{exercise}



\end{document}
