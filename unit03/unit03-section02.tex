\documentclass[12pt]{article}

\title{Unit 3.2: Gram-Schmidt Orthogonalization and $QR$-Factorization}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\maketitle

% \section*{Introduction}
% \label{sec:intro}

% Consider the two matrices
% \begin{sagesilent}
%   A = matrix([(2, -6, 4), (1, 3, 2), (2, 0, -5)])
%   Q = matrix([[2, -2, 1], [1, 2, 2], [2, 1, -2]]) / 3
% \end{sagesilent}
% \begin{align*}
%   A &= \sage{A} & Q &= \sage{Q}
% \end{align*}
% Computing the inverses of $A$ and $Q$ (either by hand or using the
% \texttt{A.inverse()} and \texttt{Q.inverse()} commands in \texttt{sage}) gives
% \begin{align*}
%   A^{-1} &= \sage{A.inverse()} & Q^{-1} &= \sage{Q.inverse()}
% \end{align*}

% Motivation is to replace $A^\intercal A\vv{x}=A^\intercal\vv{b}$ with
% $R\vv{x}=Q^\intercal\vv{b}$.

\tableofcontents


\newpage
\section{Introduction}

At this stage, we have encountered the operations of \emph{matrix inversion} and
\emph{matrix transposition}. These two operations satisfy the \emph{involution
  rule}
\begin{align*}
  (A^\intercal)^\intercal &= A & (A^{-1})^{-1} &= A
\end{align*}
and the \emph{reverse multiplication rule}
\begin{align*}
  (AB)^\intercal &= B^\intercal A^\intercal & (AB)^{-1} &= B^{-1} A^{-1}
\end{align*}
We have found uses for inversion and transposition in our study of systems of
linear equations of the form $A\vv{x}=\vv{b}$. If $A$ is invertible, then
$A\vv{x}=\vv{b}$ is solved by $\vv{x}=A^{-1}\vv{b}$. If $A\vv{x}=\vv{b}$ is
inconsistent, then the method of least squares allows us to \emph{approximate} a
solution by solving $A^\intercal A\vv{x}=A^\intercal\vv{b}$.

Although the operations of inversion and transposition share several properties,
the effort exerted in performing the operations themselves is quite
different. As we saw in Unit 3 Section 3, to invert a matrix $A$, we augment
with the appropriately-sized identity matrix $I$ and perform Gauss-Jordan
elimination. The larger the matrix, the more steps it takes to find $A^{-1}$.
On the other hand, the process of transposing a matrix requires very little
effot. Computing $A^\intercal$ only requires one to insert the columns of $A$
into the rows of $A^\intercal$.

So, from a computational perspective, it is more desirable to work with
$A^\intercal$ than $A^{-1}$. In this section, we will study situations where it
is appropriate to use transposition in place of inversion. In particular, we
will find that a square matrix $A$ satisfies $A^\intercal=A^{-1}$ if its columns
form an \emph{orthonormal list}.


\section{Orthonormal Lists}
\label{sec:orthonorm}

Recall our visual representation of $\mathbb{R}^3$ as $xyz$-space
\[
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[tdplot_main_coords, scale=3/2]
    \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
    \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
    \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

    \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [below right] {$\vv*{e}{1}$};
    \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
    \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
  \end{tikzpicture}
\]
The three coordinate axes are defined by the \emph{standard basis vectors}
\begin{sagesilent}
  e1, e2, e3 = identity_matrix(3).columns()
\end{sagesilent}
\begin{align*}
  \vv*{e}{1} &= \sage{e1} & \vv*{e}{2} &= \sage{e2} & \vv*{e}{3} &= \sage{e3}
\end{align*}
Note that each of $\vv*{e}{1}$, $\vv*{e}{2}$, and $\vv*{e}{3}$ is a
\emph{unit vector}, meaning that each has length one
\begin{align*}
  \norm{\vv*{e}{1}} &= 1 & \norm{\vv*{e}{2}} &= 1 & \norm{\vv*{e}{3}} &= 1
\end{align*}
Additionally, each pair of vectors in $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$
is \emph{orthogonal}
\begin{align*}
  \vv*{e}{1}\cdot\vv*{e}{2} &= 0 & \vv*{e}{1}\cdot\vv*{e}{3} &= 0 & \vv*{e}{2}\cdot\vv*{e}{3} &= 0
\end{align*}
As we will see shortly, these two properties make the collection
$\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ particularly convenient to work
with.

Now, suppose that we choose to \emph{rotate} the axes for $xyz$-space
\newcommand{\myOld}{
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[tdplot_main_coords, scale=3/2]
    \draw[thick,<->] (-3,0,0) -- (3,0,0) node[anchor=north east]{$x$};
    \draw[thick,<->] (0,-2,0) -- (0,2,0) node[anchor=north west]{$y$};
    \draw[thick,<->] (0,0,-2) -- (0,0,2) node[anchor=south]{$z$};

    \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [below right] {$\vv*{e}{1}$};
    \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{e}{2}$};
    \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{e}{3}$};
  \end{tikzpicture}
}
\newcommand{\myNew}{
  \tdplotsetmaincoords{70}{110}
  \begin{tikzpicture}[tdplot_main_coords, scale=3/2, rotate=30]
    \draw[thick,<->] (-3,0,0) -- (3,0,0);
    \draw[thick,<->] (0,-2,0) -- (0,2,0);
    \draw[thick,<->] (0,0,-2) -- (0,0,2);

    \draw[ultra thick, ->, blue] (0, 0, 0) -- (2, 0, 0) node [below right] {$\vv*{q}{1}$};
    \draw[ultra thick, ->, red] (0, 0, 0) -- (0, 4/3, 0) node [below] {$\vv*{q}{2}$};
    \draw[ultra thick, ->, violet] (0, 0, 0) -- (0, 0, 4/3) node [right] {$\vv*{q}{3}$};
  \end{tikzpicture}
}
\[
  \begin{tikzcd}[column sep=large]
    \myOld\arrow[]{r}{\textnormal{rotate}} \pgfmatrixnextcell \myNew
  \end{tikzcd}
\]
After rotating, the vectors in $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$ become
$\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$. The new vectors
$\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$ have coordinates different from the
coordinates of $\Set{\vv*{e}{1},\vv*{e}{2},\vv*{e}{3}}$. However, since we have
not ``stretched'' the axes, the lengths have been preserved
\begin{align*}
  \norm{\vv*{q}{1}} &= 1 & \norm{\vv*{q}{2}} &= 1 & \norm{\vv*{q}{3}} &= 1
\end{align*}
Additionally, our rotation has preserved the angles between the vectors, so
each pair of vectors in $\Set{\vv*{q}{1},\vv*{q}{2},\vv*{q}{3}}$ is
\emph{orthogonal}
\begin{align*}
  \vv*{q}{1}\cdot\vv*{q}{2} &= 0 & \vv*{q}{1}\cdot\vv*{q}{3} &= 0 & \vv*{q}{2}\cdot\vv*{q}{3} &= 0
\end{align*}
So, by rotating, we have constructed a new list of unit vectors that are
orthogonal to each other. Lists of vectors with these properties are called
\emph{orthonormal}.

\begin{genbox}{Orthonormal List of Vectors}
  A list of vectors $\Set{\vv*{q}{1}, \vv*{q}{2}, \dotsc, \vv*{q}{k}}$ is
  called \emph{orthonormal} if
  \[
    \vv*{q}{i}\cdot\vv*{q}{j}
    =
    \begin{cases}
      0 & \textnormal{when }i\neq j \quad \textnormal{(\emph{orthogonal} vectors)} \\
      1 & \textnormal{when }i=j \quad \textnormal{(\emph{unit} vectors)}
    \end{cases}
  \]
  That is, the list is orthonormal if each vector is a unit vector and each
  pair of vectors is orthogonal.
\end{genbox}

To check if a list is orthonormal, one must verify that each vector has
length one and that each pair of vectors has dot product equal to zero.

\begin{sagesilent}
  from orthogonal import my_orthogonal_QQ
  Q = my_orthogonal_QQ(1, 1, 3, 0, orthogonal=True)
  q1, q2, q3 = Q.columns()
\end{sagesilent}
\begin{example}\label{ex:myorthoex}
  Consider the vectors
  \begin{align*}
    \vv*{q}{1} &= \oldfrac{1}{\sage{q1.norm()}}\sage{q1} & \vv*{q}{2} &= \oldfrac{1}{\sage{q2.norm()}}\sage{q2} & \vv*{q}{3} &= \oldfrac{1}{\sage{q3.norm()}}\sage{q3}
  \end{align*}
  The lengths of these vectors are
  \begin{align*}
    \norm{\vv*{q}{1}} &= \norm*{\oldfrac{1}{\sage{q1.norm()}}\sage{q1}}      & \norm{\vv*{q}{2}} &= \norm*{\oldfrac{1}{\sage{q2.norm()}}\sage{q2}}      & \norm{\vv*{q}{3}} &= \norm*{\oldfrac{1}{\sage{q3.norm()}}\sage{q3}} \\
                      &= \oldfrac{1}{\sage{q1.norm()}}\cdot\norm*{\sage{q1}} &                   &= \oldfrac{1}{\sage{q2.norm()}}\cdot\norm*{\sage{q2}} &                   &= \oldfrac{1}{\sage{q3.norm()}}\cdot\norm*{\sage{q3}} \\
                      &= \oldfrac{1}{\sage{q1.norm()}}\cdot\sage{q1.norm()}  &                   &= \oldfrac{1}{\sage{q2.norm()}}\cdot\sage{q2.norm()}  &                   &= \oldfrac{1}{\sage{q3.norm()}}\cdot\sage{q3.norm()} \\
                      &= 1                                                &                   &= 1                                                &                   &= 1
  \end{align*}
  The dot product of each pair of these vectors is
  \begin{align*}
    \vv*{q}{1}\cdot\vv*{q}{2} &= \oldfrac{1}{\sage{q1.norm()}}\sage{q1}\cdot\oldfrac{1}{\sage{q2.norm()}}\sage{q2}=\sage{q1.dot_product(q2)} \\
    \vv*{q}{1}\cdot\vv*{q}{3} &= \oldfrac{1}{\sage{q1.norm()}}\sage{q1}\cdot\oldfrac{1}{\sage{q3.norm()}}\sage{q3}=\sage{q1.dot_product(q3)} \\
    \vv*{q}{2}\cdot\vv*{q}{3} &= \oldfrac{1}{\sage{q2.norm()}}\sage{q2}\cdot\oldfrac{1}{\sage{q3.norm()}}\sage{q3}=\sage{q2.dot_product(q3)}
  \end{align*}
  Each of the vectors in the list $\Set{\vv*{q}{1}, \vv*{q}{2},\vv*{q}{3}}$
  is a unit vector and each pair of vectors from the list is orthogonal. The
  list is therefore orthonormal.
\end{example}


\section{Matrices with Orthonormal Columns}
\label{sec:orthcols}

The letter $Q$ is typically used to denote a matrix with orthonormal
columns. These matrices possess several nice properties which make them
desirable to work with. For example, suppose that we insert the vectors in an
orthonormal list $\Set{\vv*{q}{1}, \vv*{q}{2},\vv*{q}{3}}$ into the columns of a
matrix
\newcommand{\myQ}{
  \left[
    \begin{array}{ccc}
      \vv*{q}{1} & \vv*{q}{2} & \vv*{q}{3}
    \end{array}
  \right]
}
\newcommand{\myQT}{
  \left[
    \begin{array}{ccc}
      \vv*{q}{1} \\ \vv*{q}{2} \\ \vv*{q}{3}
    \end{array}
  \right]
}
\newcommand{\myQTQ}{
  \left[
    \begin{array}{ccc}
      \vv*{q}{1}\cdot\vv*{q}{1} & \vv*{q}{1}\cdot\vv*{q}{2} & \vv*{q}{1}\cdot\vv*{q}{3}  \\
      \vv*{q}{2}\cdot\vv*{q}{1} & \vv*{q}{2}\cdot\vv*{q}{2} & \vv*{q}{2}\cdot\vv*{q}{3}  \\
      \vv*{q}{3}\cdot\vv*{q}{1} & \vv*{q}{3}\cdot\vv*{q}{2} & \vv*{q}{3}\cdot\vv*{q}{3}
    \end{array}
  \right]
}
\[
  Q=\myQ
\]
Then the matrix product $Q^\intercal Q$ takes the form
\[
  Q^\intercal Q
  = \myQT\myQ
  = \myQTQ
  = \sage{identity_matrix(3)}
  = I
\]
The $(i, j)$ entry of $Q^\intercal Q$ is the dot product of the $i$th row of
$Q^\intercal$, which is the vector $\vv*{q}{i}$, with the $j$th column of $Q$,
which is the vector $\vv*{q}{j}$.  When $i\neq j$, this dot product is zero
since every pair of vectors in an orthonormal list is orthogonal. When $i=j$,
the dot product is one since $\vv*{q}{i}\cdot\vv*{q}{i}=\norm{\vv*{q}{i}}^2=1$.

The relation $Q^\intercal Q=I$ is the defining property for matrices with
orthonormal columns.

\begin{genbox}{Matrices with Orthonormal Columns}
  \begin{center}
    Every matrix $Q$ with orthonormal columns satisfies $Q^\intercal Q=I$.
  \end{center}
\end{genbox}

It is important to note that these matrices need not be square.

\begin{sagesilent}
  from orthogonal import my_orthogonal_QQ
  Q = my_orthogonal_QQ(1, 2, -3, 0)
  q1, q2, q3 = Q.columns()
  Q = matrix.column([q1, q2])
  a1, a2, a3 = 7*q1
  b1, b2, b3 = 7*q2
\end{sagesilent}
\begin{example}\label{ex:rectortho}
  Consider the matrix
  \[
    Q = \sage{Q}
  \]
  The product $Q^\intercal Q$ is given by
  \newcommand{\myExQTQA}{
    \left[
      \begin{array}{rr}
        (\sage{a1})^2+(\sage{a2})^2+(\sage{a3})^2 & (\sage{a1})(\sage{b1})+(\sage{a2})(\sage{b2})+(\sage{a3})(\sage{b3}) \\
        (\sage{a1})(\sage{b1})+(\sage{a2})(\sage{b2})+(\sage{a3})(\sage{b3}) & (\sage{b1})^2+(\sage{b2})^2+(\sage{b3})^2
      \end{array}
    \right]
  }
  \begin{align*}
    Q^\intercal Q
    &= \sage{Q.transpose()}\sage{Q} \\
    &= \oldfrac{1}{7}\sage{7*Q.transpose()}\oldfrac{1}{7}\sage{7*Q} \\
    &= \oldfrac{1}{49}\sage{7*Q.transpose()}\sage{7*Q} \\
    &= \oldfrac{1}{49}\myExQTQA \\
    &= \oldfrac{1}{49}\sage{49*Q.transpose()*Q} \\
    &= \sage{Q.transpose()*Q} \\
    &= I
  \end{align*}
  Since $Q^\intercal Q=I$, $Q$ has orthonormal columns.
\end{example}

When $Q$ is a square matrix, the relation $Q^\intercal Q=I$ means that
$Q^\intercal=Q^{-1}$. Such matrices are called \emph{orthogonal matrices}.

\begin{genbox}{Orthogonal Matrices}
  A square matrix $Q$ with orthonormal columns is called
  \emph{orthogonal}. These are the matrices that satisfy $Q^\intercal=Q^{-1}$.
\end{genbox}

\begin{sagesilent}
  from orthogonal import my_orthogonal_QQ
  Q = my_orthogonal_QQ(1, 1, 3, 0, orthogonal=True)
  q1, q2, q3 = Q.columns()
\end{sagesilent}
In Example \ref{ex:myorthoex}, we demonstrated that the vectors
\begin{align*}
  \vv*{q}{1} &= \oldfrac{1}{\sage{q1.norm()}}\sage{q1} & \vv*{q}{2} &= \oldfrac{1}{\sage{q2.norm()}}\sage{q2} & \vv*{q}{3} &= \oldfrac{1}{\sage{q3.norm()}}\sage{q3}
\end{align*}
form an orthonormal list. If we insert these vectors into the columns of a matrix
\[
  Q=\sage{Q/11}
\]
then $Q$ is an \emph{orthogonal matrix} $Q^\intercal=Q^{-1}$.



\subsection{Least-Squares Approximation of $Q\vec{x}=\vec{b}$}
\label{sec:lsortho}

The main utility of the equation $Q^\intercal Q=I$ its ability to simplify least
squares approximation problems. Recall that the \emph{least squares
  approximation} of a system $A\vv{x}=\vv{b}$ is the solution $\widehat{x}$ to
$A^\intercal A\widehat{x}=A^\intercal\vv{b}$. For a general matrix $A$, solving
$A^\intercal A\widehat{x}=A^\intercal\vv{b}$ requires one to row-reduce the
augmented matrix $[A^\intercal A\mid A^\intercal\vv{b}]$.

Row-reducing $[A^\intercal A\mid A^\intercal\vv{b}]$ comes at a computational
cost, especially when $A$ is large. However, if $A$ is a matrix with orthonormal
columns $A=Q$, then the least-squares approximate solution to $Q\vv{x}=\vv{b}$
is the solution to $Q^\intercal Q\widehat{x}=Q^\intercal\vv{b}$. Since
$Q^\intercal Q=I$, we immediately obtain the least squares solution
$\widehat{x}=Q^\intercal\vv{b}$.

Finding the least squares approximation $\widehat{x}=Q^\intercal\vv{b}$ to
$Q\vv{x}=\vv{b}$ only requires that one transpose $Q$ and then multiply by
$\vv{b}$. From a computational perspective this requires very little effort.

\newcommand{\myDummyTitle}{$Q\vv{x}=\vv{b}$}
\begin{genbox}{Least-Squares Approximation of \myDummyTitle}
  The least-squares approximation $\widehat{x}$ of $Q\vv{x}=\vv{b}$ is
  $\widehat{x}=Q^\intercal\vv{b}$. Consequently, if
  \[
    Q\widehat{x}=QQ^\intercal\vv{b}\neq\vv{b}
  \]
  then the system $Q\vv{x}=\vv{b}$ is inconsistent.
\end{genbox}


\begin{sagesilent}
  from orthogonal import my_orthogonal_QQ
  Q = my_orthogonal_QQ(1, 2, -3, 0)
  q1, q2, q3 = Q.columns()
  Q = matrix.column([q1, q2])
  a1, a2, a3 = 7*q1
  b1, b2, b3 = 7*q2
  b = matrix.column([3, -2, 4])
\end{sagesilent}
\begin{example}
  Consider the matrix $Q$ and the vector $\vv{b}$ given by
  \begin{align*}
    Q &= \sage{Q} & \vv{b} &= \sage{b}
  \end{align*}
  In Example \ref{ex:rectortho} we demonstrated that $Q^\intercal Q=I$, so $Q$
  has orthonormal columns. The least-squares approximate solution to
  $Q\vv{x}=\vv{b}$ is then given by
  \[
    \widehat{x}
    = Q^\intercal\vv{b}
    = \sage{Q.transpose()}\sage{b}
    = \sage{Q.transpose()*b}
  \]
  Since
  \[
    Q\widehat{x}
    = \sage{Q}\sage{Q.transpose()*b}
    = \sage{Q*Q.transpose()*b}
    \neq \vv{b}
  \]
  we see that $Q\vv{x}=\vv{b}$ is \emph{inconsistent}.
\end{example}



\section{$QR$-Decompositions}
\label{sec:gramschmidt}

The advantage of working with matrices $Q$ with orthonormal columns as opposed
to general matrices $A$ is that the least-squares approximation to
$Q\vv{x}=\vv{b}$ is easier to compute than the least-squares approximation to
$A\vv{x}=\vv{b}$. It is therefore reasonable to ask whether or not it is
possible to replace the ``difficult'' problem of finding the least-squares
approximation to $A\vv{x}=\vv{b}$ with the ``simplified'' problem of finding the
least-squares approximation to $Q\vv{x}=\vv{b}$.

\begin{genbox}{Goal}
  Solve least-squares problems of the form $Q\vv{x}=\vv{b}$ instead of
  least-squares problems of the form $A\vv{x}=\vv{b}$.
\end{genbox}

One step toward achieving this goal is by obtaining a \emph{$QR$-decomposition
  of $A$}.

\begin{genbox}{$QR$-Decomposition of $A$}
  A \emph{$QR$-decomposition} of a matrix $A$ is an equation of the form $A=QR$
  where $Q$ has orthonormal columns ($Q^\intercal Q=I$) and $R$ is an
  \emph{invertible upper-triangular} matrix.
\end{genbox}

\subsection{$QR$-Decompositions and Least-Squares Approximations}
\label{sec:qrls}

When a $QR$-decomposition of a matrix $A$ is known, the task of finding the
least-squares approximation of $A\vv{x}=\vv{b}$ is computationally
simplified.

\newcommand{\myDummyQR}{$A=QR$}
\begin{genbox}{Least-Squares Solution Given \myDummyQR}
  Given $A=QR$, the least squares approximate solution to $A\vv{x}=\vv{b}$ is
  the solution $\widehat{x}$ to the upper-triangular system
  $R\widehat{x}=Q^\intercal\vv{b}$.
\end{genbox}

A computer can quickly solve the upper-triangular system
$R\widehat{x}=Q^\intercal\vv{b}$ by using \emph{back-substitution}, as shown
below.


\begin{sagesilent}
  from orthogonal import my_orthogonal_QQ
  q1, q2, q3 = my_orthogonal_QQ(1, 1, -2, 0).columns()
  Q = matrix.column([q1, q2])
  R = matrix(QQ, [[3, 1], [0, 1]])
  A = Q*R
  b = matrix.column(QQ, [1, -3, 4])
  M = R.augment(Q.transpose()*b, subdivide=True)
  hatx1, hatx2 = (R.inverse()*Q.transpose()*b).list()
  hatx = vector([hatx1, hatx2])
\end{sagesilent}
\begin{example}\label{ex:qrls}
  Consider the matrix $A$ and the vector $\vv{b}$ given by
  \begin{align*}
    A &= \sage{A} & \vv{b} &= \sage{b}
  \end{align*}
  A $QR$-decomposition of $A$ is given by
  \[
    \underbrace{\sage{A}}_{A} = \underbrace{\sage{Q}}_{Q}\underbrace{\sage{R}}_{R}
  \]
  The least-squares approximate solution to $A\vv{x}=\vv{b}$ is the solution
  $\widehat{x}$ to $R\widehat{x}=Q^\intercal\vv{b}$. This system can be written
  in augmented form
  \[
    [R\mid Q^\intercal\vv{b}]
    =
    \begin{blockarray}{*{3}{r}}
      \begin{block}{ccc}
        \widehat{x}_1 & \widehat{x}_2 & \\
      \end{block}
      \begin{block}{[rr|r]}
        3 & 1 & \frac{13}{3} \\
        0 & 1 & -\frac{4}{3} \\
      \end{block}%
    \end{blockarray}%
  \]
  The second equation in this system is
  $\widehat{x}_2=\sage{hatx2}$. Substituting $\widehat{x}_2=\sage{hatx2}$ into
  the first equation gives
  \[
    3\,\widehat{x}_1+\pair*{-\frac{4}{3}}=\frac{13}{3}
  \]
  This equation can be solved for $\widehat{x}_1$ to obtain
  $\widehat{x}_1=\sage{hatx1}$. The least-squares approximate solution to $A\vv{x}=\vv{b}$
  is thus $\widehat{x}=\sage{hatx}$.
\end{example}

\subsection{Using the Gram-Schmidt Algorithm to Compute $A=QR$}
\label{sec:algo}

In Example \ref{ex:qrls} above, we used a given decomposition $A=QR$ to find the
least-squares approximation $\widehat{x}$ of $A\vv{x}=\vv{b}$. However, the
question of how to \emph{find} the decomposition $A=QR$ still remains. One
method for finding $A=QR$ is the \emph{Gram-Schmidt algorithm}. The Gram-Schmidt
algorithm takes a matrix $A$ with linearly independent columns as an input and
produces a factorization $A=QR$ as output.

\begin{genbox}{Input and Output for Gram-Schmidt}
  The Gram-Schmidt algorithm has
  \begin{description}
  \item[Input] A matrix $A$ with linearly independent columns ($\rank(A)$ equals
    the number of columns of $A$).
  \item[Output] A $QR$-decomposition $A=QR$.
  \end{description}
\end{genbox}

The Gram-Schmidt algorithm relies on the method of \emph{projecting one vector
  onto another}.

\begin{genbox}{Formula for Projecting a Vector $\vec{v}$ onto Another Vector $\vec{w}$}
  \[
    \proj_{\vv{w}}(\vv{v})=\frac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}\vv{w}
  \]
\end{genbox}

An explanation of the formula for $\proj_{\vv{w}}(\vv{v})$ is given in the video
supplement to this section: \url{https://warpwire.duke.edu/w/HxoCAA/}.

We can now describe the steps in the Gram-Schmidt algorithm.

\begin{genbox}{The Gram-Schmidt Algorithm}
  \begin{description}
  \item[Input] A matrix $A$ with linearly independent columns ($\rank(A)$ equals
    the number of columns of $A$).
  \end{description}
  \begin{step}
    Write
    $A = \begin{bmatrix}\vv*{a}{1} & \vv*{a}{2} & \dotsb &
      \vv*{a}{k}\end{bmatrix}$ to label the columns of $A$ as
    $\Set*{\vv*{a}{1}, \vv*{a}{2}, \dotsc, \vv*{a}{k}}$.
  \end{step}
  \begin{step}
    Use the formulas
    \newcommand{\myNormGS}[1]{\vv*{q}{#1} &= \oldfrac{\vv*{w}{#1}}{\norm{\vv*{w}{#1}}}}
    \newcommand{\myProjGS}[2]{\proj_{\vv*{w}{#1}}(\vv*{a}{#2})}
    \begin{align*}
      \vv*{w}{1} &= \vv*{a}{1}                                                          & \myNormGS{1} \\
      \vv*{w}{2} &= \vv*{a}{2}-\myProjGS{1}{2}                                          & \myNormGS{2} \\
      \vv*{w}{3} &= \vv*{a}{3}-\myProjGS{1}{3}-\myProjGS{2}{3}                          & \myNormGS{3} \\
      \vv*{w}{4} &= \vv*{a}{4}-\myProjGS{1}{4}-\myProjGS{2}{4}-\myProjGS{3}{4}          & \myNormGS{4} \\
                 &\vdots                                                                && \vdots      \\
      \vv*{w}{k} &= \vv*{a}{k}-\myProjGS{1}{k}-\myProjGS{2}{k}-\dotsb-\myProjGS{k-1}{k} & \myNormGS{k}
    \end{align*}
    to define vectors $\Set{\vv*{w}{1},\vv*{w}{2},\dotsc,\vv*{w}{k}}$ and
    $\Set{\vv*{q}{1},\vv*{q}{2},\dotsc,\vv*{q}{k}}$.
  \end{step}
  \begin{step}
    Define $Q$ by the formula
    \newcommand{\myQGS}{\begin{bmatrix}\vv*{q}{1} & \vv*{q}{2} & \dotsb & \vv*{q}{k}\end{bmatrix}}
    \newcommand{\myDotGS}[2]{\vv*{q}{#1}\cdot\vv*{a}{#2}}
    \newcommand{\myRGS}{
      \left[
        \begin{array}{ccccc}
          \myDotGS{1}{1} & \myDotGS{1}{2} & \myDotGS{1}{3} & \dotsb & \myDotGS{1}{k} \\
          0              & \myDotGS{2}{2} & \myDotGS{2}{3} & \dotsb & \myDotGS{2}{k} \\
          0              & 0              & \myDotGS{3}{3} & \dotsb & \myDotGS{3}{k} \\
          \vdots         & \vdots         & \vdots         & \ddots & \vdots         \\
          0              & 0              & 0              & \dotsb & \myDotGS{k}{k}
        \end{array}
      \right]
    }
    \[
      Q = \myQGS
    \]
    % \begin{align*}
    %   Q &= \myQGS & R &= \myRGS
    % \end{align*}
    The matrix $Q$ has orthonormal columns ($Q^\intercal Q=I$).
  \end{step}
  \begin{step}
    Define $R$ by the formula $R=Q^\intercal A$.
  \end{step}
  \begin{description}
  \item[Output] A $QR$-decomposition $A=QR$.
  \end{description}
\end{genbox}



\begin{sagesilent}
  a1 = vector([1, 1, 0])
  a2 = vector([2, 2, 3])
  A = matrix.column([a1, a2])
  Q, R = A.transpose().gram_schmidt()
  w1, w2 = Q.rows()
\end{sagesilent}
\newpage
\subsection{A $3\times 2$ Example}

In this example, we will use the Gram-Schmidt algorithm to find a
$QR$-decomposition of the matrix $A = \sage{A}$.

\setcounter{step}{0}
\begin{step}
  Label the columns of this matrix as $\vv*{a}{1}=\sage{a1}$ and $\vv*{a}{2}=\sage{a2}$.
\end{step}
\newcommand{\myNormGS}[1]{\vv*{q}{#1} &= \oldfrac{\vv*{w}{#1}}{\norm{\vv*{w}{#1}}}}
\newcommand{\myProjGS}[2]{\proj_{\vv*{w}{#1}}(\vv*{a}{#2})}
\newcommand{\sageDot}[2]{\sage{#1.dot_product(#2)}}
\newcommand{\sageFracSep}[2]{\oldfrac{\sage{#1}\cdot\sage{#2}}{\sage{#1}\cdot\sage{#1}}}
\newcommand{\sageFracDot}[2]{\oldfrac{\sageDot{#1}{#2}}{\sageDot{#1}{#1}}}
\newcommand{\sageFrac}[2]{\sage{#1.dot_product(#2)/#1.dot_product(#1)}}
\newcommand{\myProjS}[2]{\oldfrac{#1\cdot#2}{#1\cdot#1}#1}
\begin{step}
  The formula for $\vv*{w}{1}$ is
  \[
    \vv*{w}{1} = \vv*{a}{1} = \sage{a1}
  \]
  The formula for $\vv*{w}{2}$ is
  \begin{align*}
    \vv*{w}{2} &= \vv*{a}{2}-\myProjGS{1}{2} \\
               &= \vv*{a}{2}-\myProjS{\vv*{w}{1}}{\vv*{a}{2}} \\
               &= \vv*{a}{2}-\sageFracSep{w1}{a2}\vv*{w}{1} \\
               &= \vv*{a}{2}-\sageFracDot{w1}{a2}\,\vv*{w}{1} \\
               &= \vv*{a}{2}-\sageFrac{w1}{a2}\,\vv*{w}{1} \\
               &= \sage{a2}-\sageFrac{w1}{a2}\,\sage{w1} \\
               &= \sage{w2}
  \end{align*}
  The formulas for $\vv*{q}{1}$ and $\vv*{q}{2}$ are then
  \newcommand{\mySageqi}[1]{\oldfrac{1}{\sage{#1.norm()}}\sage{#1}}
  \begin{align*}
    \myNormGS{1} = \mySageqi{w1} &
                                   \myNormGS{2} = \mySageqi{w2}=\sage{w2/w2.norm()}
  \end{align*}
\end{step}
\begin{step}
  The formula for $Q$ is
  \newcommand{\myQGSA}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{2}} & 0 \\
        \frac{1}{\sqrt{2}} & 0 \\
        0                  & 1
      \end{array}
    \right]
  }
  \[
    Q
    = \begin{bmatrix}\vv*{q}{1} & \vv*{q}{2} \end{bmatrix}
    = \myQGSA
  \]
\end{step}
\begin{step}
  The formula for $R$ is
  \newcommand{\myQGSAT}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} & 0 \\
        0                  & 0                  & 1
      \end{array}
    \right]
  }
  \newcommand{\myRGSA}{
    \left[
      \begin{array}{rr}
        \sqrt{2} & 2\,\sqrt{2} \\
        0        & 3
      \end{array}
    \right]
  }
  \[
    R
    = Q^\intercal A
    = \myQGSAT\sage{A}
    = \myRGSA
  \]
\end{step}
This gives the $QR$-decomposition
\newcommand{\myQGSA}{
  \left[
    \begin{array}{rr}
      \frac{1}{\sqrt{2}} & 0 \\
      \frac{1}{\sqrt{2}} & 0 \\
      0                  & 1
    \end{array}
  \right]
}
\newcommand{\myRGSA}{
  \left[
    \begin{array}{rr}
      \sqrt{2} & 2\,\sqrt{2} \\
      0        & 3
    \end{array}
  \right]
}
\[
  \underbrace{\sage{A}}_{A}=\underbrace{\myQGSA}_{Q}\underbrace{\myRGSA}_{R}
\]










% \section{Exercises}
% \label{sec:ex}




\end{document}
