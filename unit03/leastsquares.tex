\documentclass[]{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{blkarray, bigstrut}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing, positioning, quotes, angles}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}
\usepackage{hyperref}


\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }



\title{Using \texttt{sage} to Compute the ``Line of Best Fit''}
\subtitle{MIDS Linear Algebra Review}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

% \begin{frame}
%   \frametitle{Overview}
%   \tableofcontents
% \end{frame}


\section{Problem}


\newcommand{\myA}{
  \begin{blockarray}{*{8}{r}}
    \begin{block}{cccccccc}
      x_1 & x_2 & x_3 & x_4 & x_5 & x_6 & x_7 & \\
    \end{block}
    \begin{block}{[rrrrrrr|r]}
      -3 & 9 & 4 & -19 & 17 & -3 & 14 & -6 \\
      2 & -6 & -3 & 14 & -12 & 3 & -11 & 6 \\
      -5 & 15 & 14 & -61 & 43 & -26 & 59 & -52 \\
      -2 & 6 & -1 & 2 & 4 & 12 & -12 & 24 \\
      5 & -15 & -5 & 25 & -25 & 0 & -15 & 0 \\
    \end{block}%
  \end{blockarray}%
}

\newcommand{\myAugA}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      0 & 1 & 6 \\
      1 & 1 & 0 \\
      2 & 1 & 0 \\
    \end{block}%
  \end{blockarray}%
}


\begin{frame}

  \frametitle{\secname}

  Find the ``line of best fit'' to the data points.
  \begin{columns}[onlytextwidth, T, c]
    \column{.4\textwidth}
    \[
      \begin{tikzpicture}[line cap=round, line join=round, yscale=1/3]
        \draw[thick, <->] (-1/2, 0) -- (3, 0) node [right] {$t$};
        \draw[thick, <->] (0, -5) -- (0, 8) node [above] {$y$};

        \node[blue] at (0, 6) {\textbullet};
        \node[blue] at (0, 6) [right] {$(0, 6)$};

        \node[blue] at (1, 0) {\textbullet};
        \node[blue] at (1, 0) [below] {$(1, 0)$};

        \node[blue] at (2, 0) {\textbullet};
        \node[blue] at (2, 0) [above] {$(2, 0)$};


        \draw[thick, violet, <->] (-1/2, 13/2) -- (3, -4) node [below] {$f(t)=a_1\cdot t+a_0$};
      \end{tikzpicture}
    \]
    \column{.6\textwidth}
    \newcommand{\myLeft}{
      \begin{array}{rcl}
        f(0) &=& 6 \\
        f(1) &=& 0 \\
        f(2) &=& 0
      \end{array}
    }
    \newcommand{\myRight}{
      \begin{array}{rcrcr}
        0\cdot a_1 &+& a_0 &=& 6 \\
        1\cdot a_1 &+& a_0 &=& 0 \\
        2\cdot a_1 &+& a_0 &=& 0
      \end{array}
    }
    \pause
    \begin{align*}
      \myLeft && \myAugA
    \end{align*}
  \end{columns}

\end{frame}



\section{Solution}

\begin{sagesilent}
  A = matrix.column([[0, 1, 2],[1, 1, 1]])
  b = vector([6, 0, 0])
\end{sagesilent}

\newcommand{\myRref}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1 \\
    \end{block}%
  \end{blockarray}%
}

\begin{frame}

  \frametitle{\secname}

  To solve the system, use {\color{blue}\url{sagecell.sagemath.org/}}:

  \input{mySageA.tex}

  \pause

  This code shows that
  \[
    \rref\myAugA=\myRref
  \]

  \pause

  The system is inconsistent!

\end{frame}


\newcommand{\myLeast}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      5 & 3 & 0 \\
      3 & 3 & 6 \\
    \end{block}%
  \end{blockarray}%
}

\newcommand{\myLeastRref}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 0 & -3 \\
      0 & 1 &  5 \\
    \end{block}%
  \end{blockarray}%
}

\section{Least Squares Method}

\begin{frame}

  \frametitle{\secname}

  Replace inconsistent $[A\mid\vv{b}]$ with consistent
  $[A^\intercal A\mid A^\intercal\vv{b}]$.

  \input{mysageB.tex}

  \pause

  This code shows that
  \[
    \rref\myLeast=\myLeastRref
  \]

  \pause

  The ``line of best fit'' is $f(t)=-3\,t+5$.


\end{frame}




\end{document}
