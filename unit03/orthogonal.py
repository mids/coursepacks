from sage.all import matrix


def my_orthogonal_QQ(a, b, c, d, orthogonal=None):
    n = a**2 + b**2 + c**2 + d**2
    a11 = a**2 + b**2 - c**2 - d**2
    a22 = a**2 - b**2 + c**2 - d**2
    a33 = a**2 - b**2 - c**2 + d**2
    a12 = 2*(b*c - a*d)
    a13 = 2*(a*c + b*d)
    a23 = 2*(c*d - a*b)
    a21 = 2*(a*d + b*c)
    a31 = 2*(b*d - a*c)
    a32 = 2*(a*b + c*d)
    A = matrix([[a11, a12, a13], [a21, a22, a23], [a31, a32, a33]])
    if orthogonal:
        return A
    return A / n
