\documentclass[12pt]{article}

\title{Unit 3.1: Linear Independence and Least-Squares Approximations}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\maketitle



\tableofcontents


\newpage
\section{Motivation: Fitting Lines to Data}
\label{sec:fitline}

In the previous unit, we demonstrated how to use Gauss-Jordan elimination to
solve a system of linear equations given by $A\vv{x}=\vv{b}$. We found that
every system $A\vv{x}=\vv{b}$ is either \emph{consistent} (solvable) or
\emph{inconsistent} (unsolvable).

At first, it may seem that inconsistent systems may not be useful in
applications. However, it often happens in practice that $A\vv{x}=\vv{b}$ has no
solutions. This is usually an indication that the system $A\vv{x}=\vv{b}$ is
\emph{overdetermined}, meaning that there are more equations than there are
variables. The scalars inside $\vv{b}$ are often imperfect measurements, which
can lead to an impossible equation in the Gauss-Jordan algorithm. In this
section, we will discuss what to do when $A\vv{x}=\vv{b}$ is inconsistent.

The figure below depicts five data points.
\[
  \begin{tikzpicture}[line cap=round, line join=round, yscale=3/4, xscale=3]

    \draw[ultra thick, <->] (-2/3, 0) -- (3, 0) node [right] {$t$};
    \draw[ultra thick, <->] (0, -8/3) -- (0, 8) node [above] {$y$};

    \coordinate (p1) at (1, -1);
    \coordinate (p2) at (2, 2);
    \coordinate (p3) at (0, 6);
    \coordinate (p4) at (0, 3);
    \coordinate (p5) at (1, 4);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};

    \node at (p1) [below] {$(1, -1)$};
    \node at (p2) [above] {$(2, 2)$};
    \node at (p3) [left] {$(0, 6)$};
    \node at (p4) [left] {$(0, 3)$};
    \node at (p5) [above] {$(1, 4)$};

  \end{tikzpicture}
\]
Consider the problem of finding a line passing through each of these data
points. A human can simply look at the data points and see that they do not lie
on a line. A computer, however, needs to translate the question of whether or
not these points lie on a line into an algebraic statement. Such a line would be
of the form $f(t)=a_1\cdot t+a_0$. Plugging in each data point's $t$-coordinate
into $f(t)$ should yield the data point's $y$-coordinate. Algebraically, this is
written as
\begin{align*}
  f(0) &= 6 & f(0) &= 3 & f(1) &= 4 & f(1) &= -1 & f(2) &= 2
\end{align*}
Evaluating each of these expressions gives a linear system of five equations and
two variables
\[
  \begin{array}{rcrcrcrcl}
    0\cdot a_1 &+& a_0 &=&  6 &\leftarrow & f(0)&=& 6 \\
    0\cdot a_1 &+& a_0 &=&  3 &\leftarrow & f(0)&=& 3 \\
    1\cdot a_1 &+& a_0 &=&  4 &\leftarrow & f(1)&=& 4 \\
    1\cdot a_1 &+& a_0 &=& -1 &\leftarrow & f(1)&=&-1 \\
    2\cdot a_1 &+& a_0 &=&  2 &\leftarrow & f(2)&=& 2
  \end{array}
\]
We can use Gauss-Jordan elimination to solve this system. The augmented matrix
for this system is
\[
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      0 & 1 & 6 \\
      0 & 1 & 3 \\
      1 & 1 & 4 \\
      1 & 1 & -1 \\
      2 & 1 & 2 \\
    \end{block}%
  \end{blockarray}%
\]
After elimination, we find that
\[
  \rref
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      0 & 1 & 6 \\
      0 & 1 & 3 \\
      1 & 1 & 4 \\
      1 & 1 & -1 \\
      2 & 1 & 2 \\
    \end{block}%
  \end{blockarray}%
  =
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      a_1 & a_0 & \\
    \end{block}
    \begin{block}{[rr|r]}
      \cfbox{blue}{1} & 0 & 0 \\
      0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & \cfbox{red}{1} \\
      0 & 0 & 0 \\
      0 & 0 & 0 \\
    \end{block}%
  \end{blockarray}%
\]
The system is \emph{inconsistent}, since there is a pivot is in the augmented
column. This means that the five data points do not all lie on a line.

Now that we have determined that the data points do not lie on a line, we might
then ask how to find the line that ``best fits'' these five data points.  To
address this question, we begin by measuring the \emph{error} in approximating
that data by a given line. For example, consider the line
$f(t)=(-\frac{3}{2})\cdot t+4$ plotted below.
\[
  \begin{tikzpicture}[line cap=round, line join=round, yscale=3/4, xscale=3]

    \draw[ultra thick, <->] (-2/3, 0) -- (3, 0) node [right] {$t$};
    \draw[ultra thick, <->] (0, -8/3) -- (0, 8) node [above] {$y$};

    \coordinate (p1) at (1, -1);
    \coordinate (p2) at (2, 2);
    \coordinate (p3) at (0, 6);
    \coordinate (p4) at (0, 3);
    \coordinate (p5) at (1, 4);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};

    \node at (p1) [below] {$(1, -1)$};
    \node at (p2) [above] {$(2, 2)$};
    \node at (p3) [left] {$(0, 6)$};
    \node at (p4) [left] {$(0, 3)$};
    \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-2/3:3, smooth, variable=\x, blue]
    plot ({\x},{-3*\x/2+4}) node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

  \end{tikzpicture}
\]
\begin{sagesilent}
  A = matrix.column([[0, 0, 1, 1, 2], [1, 1, 1, 1, 1]])
  b = matrix.column([6, 3, 4, -1, 2])
  var('a1, a0')
  vx = matrix.column([a1, a0])
  xhat = vector([-3/2, 4])
  xhatcol = matrix.column(xhat)
\end{sagesilent}
The original system was $A\vv{x}=\vv{b}$ where
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
By choosing $f(t)=(-\frac{3}{2})\cdot t+4$, we are \emph{approximating} a
solution to the unsolvable system $A\vv{x}=\vv{b}$ by setting
$\widehat{x}=\sage{xhat}$. The \emph{error} of this approximation is defined as
the square of the distance from the vector $\vv{b}$ to the vector
$A\widehat{x}$. Recall from Unit 1 Section 2 that this quantity is given by
\[
  E
  = \norm{\vv{b}-A\widehat{x}}^2
\]
The coordinates of the difference $\vv{b}-A\widehat{x}$ are
\newcommand{\myF}[1]{(-\frac{3}{2})\cdot#1+4\cdot 1}
\newcommand{\myEcoord}[2]{#1-f(#2)}
\newcommand{\myAxhatEx}{
  \left[
    \begin{array}{r}
      \myF{0} \\
      \myF{0} \\
      \myF{1} \\
      \myF{1} \\
      \myF{2}
    \end{array}
  \right]
}
\newcommand{\myDiff}{
  \left[
    \begin{array}{r}
      \myEcoord{6}{0} \\
      \myEcoord{3}{0} \\
      \myEcoord{4}{1} \\
      \myEcoord{-1}{1} \\
      \myEcoord{2}{2}
    \end{array}
  \right]
}
\[
  \vv{b}-A\widehat{x}
  = \sage{b}-\sage{A}\sage{xhatcol}
  = \sage{b}-\myAxhatEx
  = \myDiff
\]
The error then takes the form
\newcommand{\myETerm}[3]{
  \underbrace{{(\myEcoord{#1}{#2})^2}}_{=e_{#3}^2}
}
\begin{align*}
  E
  &= \norm{\vv{b}-A\widehat{x}}^2 \\
  &= \myETerm{6}{0}{1}
    + \myETerm{3}{0}{2}
    + \myETerm{4}{1}{3}
    + \myETerm{-1}{1}{4}
    + \myETerm{2}{2}{5}
\end{align*}
Each term in the error can be represented on the figure.
\[
  \begin{tikzpicture}[line cap=round, line join=round, yscale=3/4, xscale=3]

    \draw[ultra thick, <->] (-2/3, 0) -- (3, 0) node [right] {$t$};
    \draw[ultra thick, <->] (0, -8/3) -- (0, 8) node [above] {$y$};

    \coordinate (p1) at (1, -1);
    \coordinate (p2) at (2, 2);
    \coordinate (p3) at (0, 6);
    \coordinate (p4) at (0, 3);
    \coordinate (p5) at (1, 4);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};

    \node at (p1) [below] {$(1, -1)$};
    \node at (p2) [above] {$(2, 2)$};
    \node at (p3) [left] {$(0, 6)$};
    \node at (p4) [left] {$(0, 3)$};
    \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-2/3:3, smooth, variable=\x, blue]
    plot ({\x},{-3*\x/2+4}) node [below] {$f(t)=(-\frac{3}{2})\cdot t+4$};

    \draw[ultra thick, red] (p1) -- (1, 5/2) node [midway, left] {$\abs{e_4}$};
    \draw[ultra thick, orange] (p2) -- (2, 1) node [midway, right] {$\abs{e_5}$};
    \draw[ultra thick, magenta] (p3) -- (0, 4) node [midway, right] {$\abs{e_1}$};
    \draw[ultra thick, cyan] (p4) -- (0, 4) node [midway, right] {$\abs{e_2}$};
    \draw[ultra thick, teal] (p5) -- (1, 5/2) node [midway, right] {$\abs{e_3}$};

  \end{tikzpicture}
\]
That is, each term in the error $E$ is the square of the vertical distance from
a data point to the line. The problem of ``finding the line of best fit to the
data'' may then be posed as follows.

\begin{genbox}{The Line of Best Fit}
  The \emph{line of best fit} to the data is determined by finding the vector
  $\widehat{x}$ that yields the smallest error $E=\norm{\vv{b}-A\widehat{x}}^2$.
\end{genbox}

A technique for finding $\widehat{x}$ is called \emph{the method of least
  squares}.

\begin{genbox}{Idea Behind the Method of Least Squares}
  When $A\vv{x}=\vv{b}$ is inconsistent, the \emph{method of least squares}
  determines the vector $\widehat{x}$ that minimizes the error
  $E=\norm{\vv{b}-A\widehat{x}}^2$.
\end{genbox}

In the rest of this section, we will discuss the method of least squares and its
applications.

\section{Linear Independence}
\label{sec:linind}

To use the method of least squares to approximate a solution to
$A\vv{x}=\vv{b}$, the coefficient matrix $A$ must have columns that are
\emph{linearly independent}.

\begin{genbox}{Definition of Linear Independence and Linear Dependence}
  Let $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ be the columns of a matrix
  \[
    A = \begin{bmatrix}\vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k} \end{bmatrix}
  \]
  The list of vectors $\Set{\vv*{v}{1}, \vv*{v}{2}, \dotsc, \vv*{v}{k}}$ is
  \begin{description}
  \item[linearly independent] if $\rank(A)=\textnormal{the number of columns of }A$
  \item[linearly dependent] if $\rank(A)<\textnormal{the number of columns of }A$
  \end{description}
  A matrix $A$ whose columns are linearly independent ($\rank(A)$ equals the
  number of columns of $A$) is said to have \emph{full column rank}.
\end{genbox}

Checking if a list of vectors is linearly independent requires one to compute
the rank of a matrix, which can be done by using the Gauss-Jordan algorithm to
find the reduced row echelon form $\rref(A)$ and counting the number of pivots.


\begin{sagesilent}
  A = matrix([(-3, 10, 23), (2, -7, -16), (-2, 4, 10), (-3, 16, 35)])
  v1, v2, v3 = A.columns()
\end{sagesilent}
\begin{example}
  Consider the vectors
  \begin{align*}
    \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3}
  \end{align*}
  Determine if the list $\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$ is linearly
  independent or linearly dependent.
  \begin{solution}
    Start by inserting the vectors $\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$
    into the columns of a matrix
    \[
      A = \sage{A}
    \]
    The reduced row echelon form of $A$ is
    \[
      \rref(A)=\sage{A.rref()}
    \]
    There are two pivots, so $\rank(A)=\sage{A.rank()}$. However, $A$ has three
    columns and $3\neq 2$. This means that the list
    $\Set{\vv*{v}{1}, \vv*{v}{2}, \vv*{v}{3}}$ is \emph{linearly dependent}.
  \end{solution}
\end{example}

\begin{sagesilent}
  A = matrix([(-3, -4, -21, 44), (-5, -19, -80, 123), (4, 13, 54, -85), (-2, -4, -19, 35)])
  a1, a2, a3, a4 = A.columns()
\end{sagesilent}
\begin{example}
  Consider the vectors
  \begin{align*}
    \vv*{a}{1} &= \sage{a1} & \vv*{a}{2} &= \sage{a2} \\
    \vv*{a}{3} &= \sage{a3} & \vv*{a}{4} &= \sage{a4}
  \end{align*}
  Determine if the list $\Set{\vv*{a}{1}, \vv*{a}{2}, \vv*{a}{3}, \vv*{a}{4}}$ is linearly
  independent or linearly dependent.
  \begin{solution}
    Insert the vectors $\Set{\vv*{a}{1}, \vv*{a}{2}, \vv*{a}{3}, \vv*{a}{4}}$
    into the columns of a matrix
    \[
      A = \sage{A}
    \]
    The reduced row echelon form of $A$ is
    \[
      \rref(A)=\sage{A.rref()}
    \]
    There are four pivots, so $\rank(A)=\sage{A.rank()}$. The number of columns
    of $A$ is also four. This means that the list
    $\Set{\vv*{a}{1}, \vv*{a}{2}, \vv*{a}{3}, \vv*{a}{4}}$ is \emph{linearly
      independent}.
  \end{solution}
\end{example}

\begin{sagesilent}
  B = matrix([(-2, 7), (3, -5), (1, -3)])
\end{sagesilent}
\begin{example}\label{ex:Bcolrank}
  Determine if the matrix
  \[
    B = \sage{B}
  \]
  has full column rank.
  \begin{solution}
    The reduced row echelon form of $B$ is
    \[
      \rref(B)=\sage{B.rref()}
    \]
    There are two pivots, so $\rank(B)=\sage{B.rank()}$. Since $B$ has two
    columns, this means that $B$ has full column rank.
  \end{solution}
\end{example}

Note that every variable in a system $A\vv{x}=\vv{b}$ where $A$ has full column
rank must be a \emph{dependent variable}. This means that systems of the form
$A\vv{x}=\vv{b}$ where $A$ has full column rank are either consistent with a
unique solution or inconsistent with no solutions.

The principal reason why full column rank matrices are required by the method of
least squares is that multiplication on the left by the transpose always yields
a nonsingular coefficient matrix.

\begin{genbox}{Nonsingularity of $A^\intercal A$ when $A$ has Full Column Rank}
  If $A$ has full column rank, then $A^\intercal A$ is \emph{nonsingular}.
\end{genbox}

This means that the system $A^\intercal A\vv{x}=A^\intercal \vv{b}$ has a unique
solution $\widehat{x}$, even if $A\vv{x}=\vv{b}$ has no solution.

\begin{sagesilent}
  b = matrix.column([2, -1, 1])
  M = B.augment(b, subdivide=True)
  N = (B.transpose() * B).augment(B.transpose() * b, subdivide=True)
  hatx = (B.transpose() * B).inverse() * B.transpose() * vector(b)
\end{sagesilent}
\begin{example}
  Consider the marix $B$ and the vector $\vv{b}$ given by
  \begin{align*}
    B &= \sage{B} & \vv{b} &= \sage{b}
  \end{align*}
  The matrix $B$ was shown to have full column rank in Example
  \ref{ex:Bcolrank}. However, performing Gauss-Jordan elimination on the
  augmented matrix $[B\mid\vv{b}]$ yields
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  There is a pivot in the augmented column, so the system $B\vv{x}=\vv{b}$ is
  inconsistent.

  Multiplying $B\vv{x}=\vv{b}$ on the left by $B^\intercal$ gives a new system
  $B^\intercal B\widehat{x}=B^\intercal\vv{b}$. The matrix $B^\intercal B$ and
  the vector $B^\intercal\vv{b}$ are given by
  \begin{align*}
    B^\intercal B &= \sage{B.transpose()*B} & B^\intercal\vv{b} &= \sage{B.transpose() *b}
  \end{align*}
  Performing Gauss-Jordan elimination on the augmented matrix
  $[B^\intercal B\mid B^\intercal\vv{b}]$ yields
  \[
    \rref\sage{N} = \sage{N.rref()}
  \]
  The system $B^\intercal B\widehat{x}=B^\intercal\vv{b}$ has unique solution
  $\widehat{x}=\sage{hatx}$.
\end{example}



\section{The Method of Least Squares}
\label{sec:ls}

In the previous section, we emphasized the fact that $A^\intercal A$ is a
nonsingular matrix when $A$ has \emph{full column rank} ($\rank(A)$ equals the
number of columns of $A$). This is the key to the \emph{method of
  least-squares}.

\begin{genbox}{The Method of Least Squares}
  When $A\vv{x}=\vv{b}$ has no solution, multiply by $A^\intercal$ and solve
  $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ for $\widehat{x}$.

  The vector $\widehat{x}$ is called the \emph{least squares approximate
    solution} to $A\vv{x}=\vv{b}$.

  This approximate solution makes the \emph{error} $E=\norm{A\widehat{x}-\vv{b}}^2$
  as small as possible.
\end{genbox}


\subsection{A $3\times 2$ Example}
\label{sec:tinyex}


\begin{sagesilent}
  M = matrix([(-2, -5, -12), (3, 7, 6), (3, 7, 2)])
  M.subdivide([], [2])
  a1, a2, b = M.columns()
  A = matrix.column([a1, a2])
  b = matrix.column(b)
  ATA = A.transpose() * A
  ATb = A.transpose() *b
  N = ATA.augment(ATb, subdivide=True)
  xhat = ATA.inverse()*A.transpose()*vector(b)
  e = A*xhat-vector(b)
  E = e.dot_product(e)
  e1, e2, e3 = e
\end{sagesilent}
\begin{example}
  Consider the matrix $A$ and the vector $\vv{b}$ given by
  \begin{align*}
    A &= \sage{A} & \vv{b} &= \sage{b}
  \end{align*}
  If $A\vv{x}=\vv{b}$ is solvable, then find all solutions. If $A\vv{x}=\vv{b}$
  is not solvable, then find the least squares approximate solution
  $\widehat{x}$ and determine the error $E$.
\end{example}

\begin{solution}
  To determine if $A\vv{x}=\vv{b}$ is solvable, perform Gauss-Jordan
  elimination on the augmented matrix $[A\mid\vv{b}]$. This yields
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  There is a pivot in the augmented column, so $A\vv{x}=\vv{b}$ is \emph{not
    solvable}.

  The least squares approximate solution to $A\vv{x}=\vv{b}$ is the solution
  $\widehat{x}$ to $A^\intercal A\widehat{x}=A^\intercal\vv{b}$. The product
  $A^\intercal A$ is given by
  \[
    A^\intercal A = \sage{A.transpose()}\sage{A} = \sage{ATA}
  \]
  and the product $A^\intercal\vv{b}$ is given by
  \[
    A^\intercal\vv{b} = \sage{A.transpose()}\sage{b} = \sage{ATb}
  \]
  To solve $A^\intercal A\widehat{x}=A^\intercal \vv{b}$ for $\widehat{x}$, we
  perform Gauss-Jordan elimination on the augmented matrix
  $[A^\intercal A\mid A^\intercal \vv{b}]$. This yields
  \[
    \rref\sage{N} = \sage{N.rref()}
  \]
  The least squares approximate solution to $A\vv{x}=\vv{b}$ is thus
  $\widehat{x}=\sage{xhat}$.

  To find the \emph{error} $E=\norm{A\widehat{x}-\vv{b}}^2$ for this
  approximation, note that
  \[
    A\widehat{x}
    = \sage{A}\sage{matrix.column(xhat)}
    = \sage{matrix.column(A*xhat)}
  \]
  The error is therefore given by
  \begin{align*}
    E
    &= \norm{A\widehat{x}-\vv{b}}^2 \\
    &= \norm{\sage{A*xhat}-\sage{vector(b)}}^2 \\
    &= \norm{\sage{e}}^2 \\
    &= (\sage{e1})^2+(\sage{e2})^2+(\sage{e3})^2 \\
    &= \sage{E}
  \end{align*}

\end{solution}


\section{Application: Linear Regression}

\subsection{Fitting a Line to Data}

Consider again the problem of fitting a line to a given collection of data
points.

\[
  \begin{tikzpicture}[line cap=round, line join=round, yscale=3/4, xscale=3]

    \draw[ultra thick, <->] (-2/3, 0) -- (3, 0) node [right] {$t$};
    \draw[ultra thick, <->] (0, -8/3) -- (0, 8) node [above] {$y$};

    \coordinate (p1) at (1, -1);
    \coordinate (p2) at (2, 2);
    \coordinate (p3) at (0, 6);
    \coordinate (p4) at (0, 3);
    \coordinate (p5) at (1, 4);

    \node[orange] at (p1) {\textbullet};
    \node[orange] at (p2) {\textbullet};
    \node[orange] at (p3) {\textbullet};
    \node[orange] at (p4) {\textbullet};
    \node[orange] at (p5) {\textbullet};

    \node at (p1) [below] {$(1, -1)$};
    \node at (p2) [above] {$(2, 2)$};
    \node at (p3) [left] {$(0, 6)$};
    \node at (p4) [left] {$(0, 3)$};
    \node at (p5) [above] {$(1, 4)$};

    \draw[very thick, <->, domain=-2/3:3, smooth, variable=\x, blue]
    plot ({\x},{-3*\x/2+4}) node [below] {$f(t)=a_1\cdot t+a_0$};

  \end{tikzpicture}
\]

\begin{sagesilent}
  var('a1 a0')
  vx = matrix.column([a1, a0])
  A = matrix.column([(0, 0, 1, 1, 2), (1, 1, 1, 1, 1)])
  b = matrix.column([6, 3, 4, -1, 2])
  AT = A.transpose()
  ATA = AT*A
  ATb = AT*b
  N = ATA.augment(ATb, subdivide=True)
  xhat = ATA.inverse()*AT*vector(b)
\end{sagesilent}
In attempt to find a line $f(t)=a_1\cdot t+a_0$ passing through each of these
data points, we used the equations
\begin{align*}
  f(0) &= 6 & f(0) &= 3 & f(1) &= 4 & f(1) &= -1 & f(2) &= 2
\end{align*}
to set-up the system of equations $A\vv{x}=\vv{b}$ where
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
We found, however, that $A\vv{x}=\vv{b}$ was \emph{unsolvable}.

We can now use the method of least squares to find the ``line of best fit.''
Since $A\vv{x}=\vv{b}$ is unsolvable, we multiply by $A^\intercal$ and solve
$A^\intercal A\widehat{x}=A^\intercal\vv{b}$ for the least squares approximate
solution $\widehat{x}$. The product $A^\intercal A$ is given by
\[
  A^\intercal A
  = \sage{AT}\sage{A}
  = \sage{ATA}
\]
and the product $A^\intercal\vv{b}$ is given by
\[
  A^\intercal\vv{b}
  = \sage{AT}\sage{b}
  = \sage{ATb}
\]
To solve $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ for $\widehat{x}$, we
perform Gauss-Jordan elimination on the augmented matrix
$[A^\intercal A\mid A^\intercal\vv{b}]$. This yields
\[
  \rref\sage{N}
  = \sage{N.rref()}
\]
The least squares approximation is thus $\widehat{x}=\sage{xhat}$, which gives
the line of best fit $f(t)=(-\frac{3}{2})\cdot t+4$.

\newpage
\subsection{Population Modeling}

\begin{sagesilent}
  var('a1 a0')
  A = matrix.column([[0, 5, 10, 15], [1, 1, 1, 1]])
  b = matrix.column([227, 237, 249, 262])
  vx = matrix.column([a1, a0])
  AT = A.transpose()
  ATA = AT*A
  ATb = AT*b
  M = ATA.augment(ATb, subdivide=True)
  xhat = ATA.inverse()*AT*vector(b)
  xhat1, xhat2 = xhat
  var('t')
  P = xhat1*t+xhat2
\end{sagesilent}
Table \ref{tab:uspop} gives the esimated population of the United States (in
millions) rounded to three digits in the years 1980, 1985, 1990, and 1995.
\begin{table}[h]
  \centering
  \begin{tabular}{c|cccc}
    year & 1980 & 1985 & 1990 & 1995 \\ \hline
    population & 227 & 237 & 249 & 262
  \end{tabular}
  \caption{US Population 1980-1995}
  \label{tab:uspop}
\end{table}
Assuming a linear relationship between time $t$ and population $P$, use this
data to predict the US population in the year 2000.
\begin{solution}
  Use the variable $t$ to denote ``years after 1980.'' By assuming that
  population $P$ depends linearly on time $t$, we are assuming that
  $P(t)=a_1\cdot t+a_0$. We are therefore interested in finding the least
  squares approximate solution to the system
  \[
    \begin{blockarray}{*{3}{r}}
      \begin{block}{ccc}
        a_1 & a_0 & \\
      \end{block}
      \begin{block}{[rr|r]}
        0 & 1 & 227 \\
        5 & 1 & 237 \\
        10 & 1 & 249 \\
        15 & 1 & 262 \\
      \end{block}%
    \end{blockarray}%
  \]
  This is the system $A\vv{x}=\vv{b}$ where
  \begin{align*}
    A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
  \end{align*}
  The least squares approximate solution $\widehat{x}$ is the solution to
  $A^\intercal\widehat{x}=A^\intercal\vv{b}$. The product $A^\intercal A$ is
  given by
  \[
    A^\intercal A
    = \sage{AT}\sage{A}
    = \sage{AT*A}
  \]
  The product $A^\intercal\vv{b}$ is given by
  \[
    A^\intercal\vv{b}
    = \sage{AT}\sage{b}
    = \sage{AT*b}
  \]
  To solve $A^\intercal A\widehat{x}=A^\intercal\vv{b}$ for $\widehat{x}$, we
  perform Gauss-Jordan elimination on the augmented matrix
  $[A^\intercal A\mid A^\intercal\vv{b}]$. This yields
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  The least squares approximate solution is thus $\widehat{x}=\sage{xhat}$. Our
  population model is then given by $P(t)=\sage{P}$. Our prediction for the
  population of the US in the year 2000 is then $P(20) =
  \sage{P(t=20)}\textnormal{ million people}$.
\end{solution}


% \section{Curve Fitting}
% \label{sec:curvefit}




\end{document}
