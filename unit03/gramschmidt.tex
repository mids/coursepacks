\documentclass[]{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{calc, decorations.pathreplacing, positioning, quotes, angles}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\proj}{proj}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%



\title{Vector Projections}
\subtitle{MIDS Linear Algebra Review}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}

\begin{frame}
  \titlepage
\end{frame}

% \begin{frame}
%   \frametitle{Overview}
%   \tableofcontents
%   %   \begin{columns}[onlytextwidth, t]
%   %     \column{.5\textwidth}
%   %     \tableofcontents[sections={1-4}]
%   %     \column{.5\textwidth}
%   %     \tableofcontents[sections={5-}]
%   %   \end{columns}
% \end{frame}

\section{The Projection of $\vec{v}$ Along $\vec{w}$}


\begin{frame}
  \frametitle{\secname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{definition}
      The \emph{projection} of $\vv{v}$ along $\vv{w}$ is the depicted vector.
    \end{definition}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[line join=round, line cap=round]

        \pgfmathsetmacro{\va}{3}
        \pgfmathsetmacro{\vb}{2}
        \pgfmathsetmacro{\wa}{4}
        \pgfmathsetmacro{\wb}{-1}

        \pgfmathsetmacro{\wnorm}{sqrt(\wa*\wa+\wb*\wb)}
        \pgfmathsetmacro{\vdotw}{\va*\wa+\vb*\wb}
        \pgfmathsetmacro{\compwv}{\vdotw/\wnorm}

        \pgfmathsetmacro{\compvwa}{\vdotw/\wnorm*\wa/\wnorm}
        \pgfmathsetmacro{\compvwb}{\vdotw/\wnorm*\wb/\wnorm}

        \pgfmathsetmacro{\compnorm}{sqrt(\compvwa*\compvwa+\compvwa*\compvwa)}
        \pgfmathsetmacro{\compdiffnorm}{sqrt((\compvwa-\va)*(\compvwa-\va)+(\compvwb-\vb)*(\compvwb-\vb))}

        \pgfmathsetmacro{\boxlen}{1/5}

        \coordinate (O) at (0, 0);
        \coordinate (v) at (\va, \vb);
        \coordinate (w) at (\wa, \wb);
        \coordinate (what) at ($ (O)!1/\wnorm!(w) $);
        \coordinate (comp) at (\compvwa, \compvwb);

        \coordinate (R) at ($ (comp)-{\boxlen/\compnorm}*(comp)$);
        % \node at (R) {R};

        \coordinate (Q) at ($ (comp)+{\boxlen/\compdiffnorm}*(v)-{\boxlen/\compdiffnorm}*(comp)$);
        % \node at (Q) {Q};

        \coordinate (S) at ($ (R)-(comp)+(Q) $);
        % \node at (S) {S};

        % \onslide<8->{
        % \draw[black]
        % (w) -- (O) -- (v)
        % pic["$\theta$", thick, draw=black, <->, angle eccentricity=1.2, angle radius=1cm] {angle=w--O--v};
        % }


        \draw[ultra thick, blue, ->] (O) -- (v) node[above] {$\vv{v}$};
        \draw[ultra thick, violet, ->] (O) -- (w) node[right] {$\vv{w}$};

        \onslide<3->{
          \draw[ultra thick, red, ->] (O) -- (comp) node[midway, below=10pt] {$\proj_{\vv{w}}(\vv{v})=c\cdot\vv{w}$};
          % \draw[very thick, decoration={brace, mirror, raise=5pt}, decorate]
          % (O) --  node[below=10pt] {$\proj_{\vv{w}}(\vv{v})=c\cdot\vv{w}$} (comp);
        }

        \onslide<2->{
          \draw[thick] (R) -- (S) -- (Q);
          \draw[thick, dashed] (v) -- (comp);
        }

        % \draw[ultra thick, orange, ->] (O) -- (comp);


      \end{tikzpicture}
    \]
  \end{columns}

  \begin{block}{\onslide<4->{Formula}}
    \onslide<4->{Must have $(\vv{v}-c\cdot\vv{w})\perp\vv{w}$.}
    \onslide<5->{This means that}
    \newcommand{\myLeft}{
        \begin{array}{rcl}
          \onslide<5->{\vv{w}\cdot(\vv{v}-c\cdot\vv{w})} &\onslide<5->{=}& \onslide<5->{0} \\
          \onslide<6->{\vv{w}\cdot\vv{v}-c\cdot\vv{w}\cdot\vv{w}} &\onslide<6->{=}& \onslide<6->{0}
        \end{array}
    }
    \newcommand{\myRight}{\onslide<7->{c=\frac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}}}
    \begin{align*}
      \myLeft && \myRight
    \end{align*}
    \onslide<8->{This gives
      $\displaystyle\proj_{\vv{w}}(\vv{v})=\frac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}\,\vv{w}$}
  \end{block}

\end{frame}


\begin{sagesilent}
  v = vector([2, -1, -3])
  w = vector([5, -1, 2])
  v1, v2, v3 = v
  w1, w2, w3 = w
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $\vv{v}=\sage{v}$ and $\vv{w}=\sage{w}$. \onslide<2->{Then}
    \begin{align*}
      \onslide<2->{\proj_{\vv{w}}(\vv{v})
      &=} \onslide<3->{\frac{\vv{w}\cdot\vv{v}}{\vv{w}\cdot\vv{w}}\,\vv{w} \\
      &=} \onslide<4->{ \frac{\sage{w}\cdot\sage{v}}{\sage{w}\cdot\sage{w}}\,\vv{w} \\
      &=} \onslide<5->{ \frac{(\sage{w1})(\sage{v1})+(\sage{w2})(\sage{v2})+(\sage{w3})(\sage{v3})}{(\sage{w1})(\sage{w1})+(\sage{w2})(\sage{w2})+(\sage{w3})(\sage{w3})}\,\vv{w} \\
      &=} \onslide<6->{ \frac{\sage{w.dot_product(v)}}{\sage{w.dot_product(w)}}\sage{w} \\
      &=} \onslide<7->{ \sage{w.dot_product(v) / w.dot_product(w)}\sage{w}}
    \end{align*}


  \end{example}
\end{frame}

\end{document}
