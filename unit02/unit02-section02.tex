\documentclass[12pt]{article}

\title{Unit 2.2: Gauss-Jordan Elimination}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='\\langle', right='\\rangle')
\end{sagesilent}


\maketitle


\tableofcontents


\newpage

\section{Motivation}
\label{sec:mot}

Recall once again the system of linear equations found in Unit 1 Section 3
\newcommand{\mySysEq}{
  \begin{array}{rcrcr}
    x &+&     y &=& 800 \\
    5\,x &+& 20\,y &=& 8500
  \end{array}
}
\[
  \mySysEq
\]
In this system, the variable $x$ represents the total number of ``budget phone
cases'' sold by a company in 2017 and the variable $y$ represents the number of
``deluxe phone cases'' sold by the same company in 2017.

In Section 1 of this unit, we discussed two methods for using matrices to
represent this system. The ``matrix form'' of this system is the matrix equation
$A\vv{x}=\vv{b}$ where
\begin{sagesilent}
  A = matrix([[1, 1], [5, 20]])
  var('x y')
  vx = matrix.column([x, y])
  b = matrix.column([800, 8500])
\end{sagesilent}
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
The ``augmented form'' of this system is the augmented matrix given by
\newcommand{\mySysAug}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      x & y &  \\
    \end{block}
    \begin{block}{[rr|r]}
      \cfbox{blue}{1} &  1 & 800 \\
      5 & 20 & 8500 \\
    \end{block}%
  \end{blockarray}%
}
\[
  \mySysAug
\]
We have ``boxed'' the $(1, 1)$ entry of this augmented matrix because this entry
is a pivot.  In Section 1 of this unit, we demonstrated how to analyze a system
in \emph{reduced row echelon form}. The problem with our phone case system is
that it is \emph{not} in reduced row echelon form.

However, we can manipulate our system by using equation arithmetic. The equation
and augmented forms of the system are
\begin{align*}
  \mySysEq && \mySysAug
\end{align*}
We can ``eliminate'' the variable $x$ from the second equation by subtracting
five times the first equation from the second equation. This gives
\newcommand{\mySysEqA}{
  \begin{array}{rcrcr}
    x &+&     y &=& 800 \\
      & & 15\,y &=& 4500
  \end{array}
}
\newcommand{\mySysAugA}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      x & y &  \\
    \end{block}
    \begin{block}{[rr|r]}
      \cfbox{blue}{1} &  1 & 800 \\
      0 & 15 & 4500 \\
    \end{block}%
  \end{blockarray}%
}
\begin{align*}
  \mySysEqA && \mySysAugA
\end{align*}
To solve for $y$, we can multiply the second equation by $\frac{1}{15}$. This
gives
\newcommand{\mySysEqB}{
  \begin{array}{rcrcr}
    x &+& y &=& 800 \\
      & & y &=& 300
  \end{array}
}
\newcommand{\mySysAugB}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      x & y &  \\
    \end{block}
    \begin{block}{[rr|r]}
      \cfbox{blue}{1} & 1 & 800 \\
      0 & \cfbox{blue}{1} & 300 \\
    \end{block}%
  \end{blockarray}%
}
\begin{align*}
  \mySysEqB && \mySysAugB
\end{align*}
We now have pivots in positions $(1, 1)$ and $(2, 2)$.  Finally, we can
eliminate the variable $y$ from the first equation by subtracting the second
equation from the first. This gives \newcommand{\mySysEqC}{
  \begin{array}{rcrcr}
    x & &   &=& 500 \\
      & & y &=& 300
  \end{array}
}
\newcommand{\mySysAugC}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      x & y &  \\
    \end{block}
    \begin{block}{[rr|r]}
      \cfbox{blue}{1} & 0 & 500 \\
      0 & \cfbox{blue}{1} & 300 \\
    \end{block}%
  \end{blockarray}%
}
\begin{align*}
  \mySysEqC && \mySysAugC
\end{align*}
At this stage, the system is in reduced row echelon form and we see that the
solution is given by $x=500$ and $y=300$. That is, the phone case company sold
$500$ ``budget'' phone cases and $300$ ``deluxe'' phone cases in 2017.

So, we started with a system of linear equations not in reduced row echelon form
and used equation arithmetic until the system was in reduced row echelon
form. It turns out that any system can be manipulated in this way to obtain a
new system in reduced row echelon form. In this section, we will describe the
\emph{Gauss-Jordan algorithm}, which is a procedure for manipulating the rows of
a matrix until a new matrix in reduced row echelon form is produced.



\section{Elementary Row Operations}
\label{sec:elemopts}

In our phone case example, we used equation arithmetic to produce the reduced
row echelon form of the system. At each stage of the process, we manipulated the
rows in the augmented matrix to simplify the system. These manipulations are
examples of \emph{elementary row operations}. Formally, there are three types of
elementary row operations.

\begin{genbox}{The Three Types of Elementary Row Operations}
  The three types of \emph{elementary row operations} are:
  \begin{description}
  \item[Row Switching] A row can be switched with another row.
    \begin{description}
    \item[Notation] $R_i\leftrightarrow R_j$
    \end{description}
  \item[Row Scaling] A row can be scaled by a nonzero scalar.
    \begin{description}
    \item[Notation] $c\cdot R_i\to R_i$
    \end{description}
  \item[Row Addition] A row can be replaced by the sum
    of that row and any scalar multiple of another row.
    \begin{description}
    \item[Notation] $R_i+c\cdot R_j\to R_i$
    \end{description}
  \end{description}
\end{genbox}

Elementary row operations can be performed on \emph{any} matrix, as we see in
Examples \ref{ex:firstgen}, \ref{ex:secondgen}, and \ref{ex:thirdgen} below.

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 4)
  E = elementary_matrix(A.nrows(), row1=1, row2=3)
\end{sagesilent}
\begin{example}[Row Switching]\label{ex:firstgen}
  $\sage{A}\xrightarrow{R_2\leftrightarrow R_4}\sage{E*A}$
\end{example}

\begin{sagesilent}
  A = random_matrix(ZZ, 5, 3)
  E = elementary_matrix(A.nrows(), row1=2, scale=-11/3)
\end{sagesilent}
\begin{example}[Row Scaling]\label{ex:secondgen}
  $\sage{A}\xrightarrow{-(\nicefrac{11}{3})\cdot R_3\to R_3}\sage{E*A}$
\end{example}

\begin{sagesilent}
  A = random_matrix(ZZ, 3, 4)
  E = elementary_matrix(A.nrows(), row1=2, row2=0, scale=-4)
\end{sagesilent}
\begin{example}[Row Addition]\label{ex:thirdgen}
  $\sage{A}\xrightarrow{R_3-4\cdot R_1\to R_3}\sage{E*A}$
\end{example}

When we perform elementary row operations on an \emph{augmented} matrix, we
manipulate the operation is applied to both the coefficient matrix and the
augmented column, as we see in Examples \ref{ex:firstaug}, \ref{ex:secondaug},
and \ref{ex:thirdaug} below.

\begin{sagesilent}
  A = random_matrix(ZZ, 4, 4)
  b = random_vector(A.nrows())
  M = A.augment(b, subdivide=True)
  E = elementary_matrix(A.nrows(), row1=1, row2=3)
\end{sagesilent}
\begin{example}[Row Switching]\label{ex:firstaug}
  $\sage{M}\xrightarrow{R_2\leftrightarrow R_4}\sage{E*M}$
\end{example}

\begin{sagesilent}
  A = random_matrix(ZZ, 5, 3)
  b = random_vector(A.nrows())
  M = A.augment(b, subdivide=True)
  E = elementary_matrix(A.nrows(), row1=2, scale=-11/3)
\end{sagesilent}
\begin{example}[Row Scaling]\label{ex:secondaug}
  $\sage{M}\xrightarrow{-(\nicefrac{11}{3})\cdot R_3\to R_3}\sage{E*M}$
\end{example}

\begin{sagesilent}
  A = random_matrix(ZZ, 3, 3)
  b = random_vector(A.nrows())
  M = A.augment(b, subdivide=True)
  E = elementary_matrix(A.nrows(), row1=2, row2=0, scale=-4)
\end{sagesilent}
\begin{example}[Row Addition]\label{ex:thirdaug}
  $\sage{M}\xrightarrow{R_3-4\cdot R_1\to R_3}\sage{E*M}$
\end{example}

\section{The Gauss-Jordan Algorithm}
\label{sec:gj}

It turns out that given any matrix $A$, one may perform a sequence of elementary
row operations on $A$ to obtain a new matrix in reduced row echelon form. This
new matrix is called the \emph{reduced row echelon form of $A$} and denoted by
$\rref(A)$. One method for obtaining the reduced row echelon form of $A$ is the \emph{Gauss-Jordan algorithm}, which we outline below.

\begin{genbox}{The Gauss-Jordan Algorithm to Compute rref}
  Every matrix $A$ has a unique \emph{reduced row echelon form} $\rref(A)$. To
  determine $\rref(A)$, start with $(i, j)=(1, 1)$ and complete the following
  steps:
  \begin{description}
  \item[Step 1] If $a_{ij}=0$, switch the $i$th row with the first row below it
    whose $j$th entry is nonzero. If no such row exists, increase $j$ by one and
    repeat this step.
  \item[Step 2] Multiply the $i$th row by $\nicefrac{1}{a_{ij}}$ to make a pivot
    entry in the position $(i, j)$.
  \item[Step 3] Eliminate \emph{all} other entries in the $j$th column by
    subtracting suitable multiples of the $i$th row from the other rows.
  \item[Step 4] Increase $i$ and $j$ by one and return to Step 1.
  \end{description}
  The algorithm terminates after the last row or last column is processed. The
  output of this algorithm is the \emph{reduced row echelon form} $\rref(A)$ of
  $A$.
\end{genbox}

The reduced row echelon form of a matrix $A$ is used to define structural
properties of the matrix. Recall that the \emph{rank} and the \emph{nullity} of
a matrix in reduced row echelon form are the numbers of pivot and nonpivot
columns, respectively. Once the reduced row echelon form $\rref(A)$ of a matrix
$A$ is known, we say that the \emph{rank} and \emph{nullity} of $A$ are the
number of pivot and nonpivot columns in $\rref(A)$.

\begin{genbox}{Rank and Nullity}
  The \emph{rank} of a matrix $A$ is defined as
  \[
    \rank(A)=\textnormal{number of pivot columns in }\rref(A)
  \]
  The \emph{nullity} of $A$ is defined as
  \[
    \nullity(A)=\textnormal{number of nonpivot columns in }\rref(A)
  \]
  To compute $\rank(A)$ and $\nullity(A)$, we use the Gauss-Jordan algorithm to
  first obtain $\rref(A)$.
\end{genbox}

The reduced row echelon form of an \emph{augmented} matrix $[A\mid\vv{b}]$ can
be used to solve the system of linear equations corresponding to the augmented
matrix.

\begin{genbox}{Using rref to Solve Systems of Linear Equations}
  The solutions to a system of linear equations of the form $[A\mid\vv{b}]$ are
  the same as the solutions to the reduced row echelon form
  $\rref[A\mid\vv{b}]$.

  To solve $[A\mid\vv{b}]$, we use the Gauss-Jordan algorithm to first obtain
  $\rref[A\mid\vv{b}]$.
\end{genbox}

We devote the rest of this section to giving examples of applying the
Gauss-Jordan algorithm to compute reduced row echelon forms.

\section{Computing rref (General Matrices)}

\begin{sagesilent}
  A = matrix([(-1, -4, 5, 3), (10, 40, -37, -30), (-3, -12, 11, 9)])
  E1 = elementary_matrix(A.nrows(), row1=0, scale=-1)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-10)
  E3 = elementary_matrix(A.nrows(), row1=2, row2=0, scale=3)
  E4 = elementary_matrix(A.nrows(), row1=1, scale=1/13)
  E5 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=5)
  E6 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=4)
\end{sagesilent}

\subsection{A $3\times 4$ Example}

In this example, we will use the Gauss-Jordan algorithm to compute $\rref(A)$
where
\[
  A = \sage{A}
\]
We start by looking for a pivot in $(1, 1)$ entry of $A$
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{red}{-1} & -4 & 5 & 3 \\
      10 & 40 & -37 & -30 \\
      -3 & -12 & 11 & 9
    \end{array}
  \right]
\]
Here, $a_{11}=-1$. Since we need $a_{11}=1$ to have a pivot in the $(1, 1)$
position, we must scale the first row by $-1$. This is written as
\newcommand{\stepA}{\xrightarrow{-R_1\to R_1}}
\newcommand{\stepB}{\xrightarrow{\scriptsize
    \begin{array}{rcrcrr}
      R_2 &-& 10\cdot R_1 &\to& R_2 \\
      R_3 &+& 3\cdot R_1 &\to& R_3
    \end{array}
  }}
\newcommand{\stepC}{\xrightarrow{(\nicefrac{1}{13})\cdot R_2\to R_2}}
\newcommand{\stepD}{\xrightarrow{\scriptsize
    \begin{array}{rcrcrr}
      R_1 &+& 5\cdot R_2 &\to& R_1 \\
      R_3 &+& 4\cdot R_2 &\to& R_3
    \end{array}
  }}
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{red}{-1} & -4 & 5 & 3 \\
      10 & 40 & -37 & -30 \\
      -3 & -12 & 11 & 9
    \end{array}
  \right]
  \stepA
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      10 & 40 & -37 & -30 \\
      -3 & -12 & 11 & 9
    \end{array}
  \right]
\]
Now that we have a pivot in the $(1, 1)$ position, we need to ``zero out'' the
other entries in this pivot column. The nonpivot entries in this column are
$a_{2, 1}=10$ and $a_{31}=-3$.
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      \cfbox{red}{10} & 40 & -37 & -30 \\
      \cfbox{red}{-3} & -12 & 11 & 9
    \end{array}
  \right]
\]
We can ``zero out'' our pivot column by subtracting ten times the first row from
the second row and adding three times the first row to the third row. This is
written as
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      \cfbox{red}{10} & 40 & -37 & -30 \\
      \cfbox{red}{-3} & -12 & 11 & 9
    \end{array}
  \right]
  \stepB
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & 0 & 13 & 0 \\
      0 & 0 & -4 & 0
    \end{array}
  \right]
\]
We have successfully ``zeroed out'' the first pivot column.

Now that we have a pivot in position $(1, 1)$, we search for a pivot in the
$(2, 2)$ position.
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & \cfbox{red}{0} & 13 & 0 \\
      0 & 0 & -4 & 0
    \end{array}
  \right]
\]
Currently, $a_{22}=0$. Since we cannot scale $0$ by any number to obtain a $1$,
the Gauss-Jordan algorithm requires that we switch the second row with the
closest row beneath it that has a nonzero number in the second column. However,
in our case, both the $(2, 2)$ and $(3, 2)$ entries are equal to zero
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & \cfbox{red}{0} & 13 & 0 \\
      0 & \cfbox{red}{0} & -4 & 0
    \end{array}
  \right]
\]
This means that we will not have a pivot in the second column.

Since there will be no pivot in the second column, we move to the third column
of the second row and look for a pivot in the $(2, 3)$ position
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & 0 & \cfbox{red}{13} & 0 \\
      0 & 0 & -4 & 0
    \end{array}
  \right]
\]
Since $a_{32}=13$, we can obtain a pivot by scaling the second row by
$\frac{1}{13}$. This is written as
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & 0 & \cfbox{red}{13} & 0 \\
      0 & 0 & -4 & 0
    \end{array}
  \right]
  \stepC
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & -5 & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & -4 & 0
    \end{array}
  \right]
\]
We now have a pivot in the $(2, 3)$ position. The next step is to ``zero out''
this new pivot column. The other entries in this column are $a_{13}=-5$ and
$a_{33}=-4$
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & \cfbox{red}{-5} & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & \cfbox{red}{-4} & 0
    \end{array}
  \right]
\]
We can zero out these entries by adding five times row two to row one and
adding four times row two to row three. This is written as
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & \cfbox{red}{-5} & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & \cfbox{red}{-4} & 0
    \end{array}
  \right]
  \stepD
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & 0 & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & 0 & 0
    \end{array}
  \right]
\]
We have successfully cleared out our second pivot column.

With pivots in the $(1, 1)$ and $(2, 3)$ positions, we now move to the $(3, 4)$
position and search for a pivot
\[
  \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & 0 & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & 0 & \cfbox{red}{0}
    \end{array}
  \right]
\]
This is the last entry in the matrix and is equal to zero. The matrix is now in
reduced row echelon form and the Gauss-Jordan algorithm terminates. We have
shown that
\[
  \rref(A)=\rref\sage{A}=
    \left[
    \begin{array}{rrrr}
      \cfbox{blue}{1} & 4 & 0 & -3 \\
      0 & 0 & \cfbox{blue}{1} & 0 \\
      0 & 0 & 0 & 0
    \end{array}
  \right]
\]
Written in sequence, the steps of the algorithm were
\begin{align*}
  \sage{A}
  &\stepA \sage{E1*A} \\
  &\stepB \sage{E3*E2*E1*A} \\
  &\stepC \sage{E4*E3*E2*E1*A} \\
  &\stepD \sage{E6*E5*E4*E3*E2*E1*A}
\end{align*}
Note that there are two pivot columns and two nonpivot columns in
$\rref(A)$. This means that $\rank(A)=\sage{A.rank()}$ and
$\nullity(A)=\sage{A.right_nullity()}$.


\begin{sagesilent}
  B = matrix([(1, -6, -21, 6, 0), (-11, 14, 75, -58, 88), (-4, 5, 27, -21, 32), (-5, 9, 42, -28, 38)])
  E1 = elementary_matrix(B.nrows(), row1=1, row2=0, scale=11)
  E2 = elementary_matrix(B.nrows(), row1=2, row2=0, scale=4)
  E3 = elementary_matrix(B.nrows(), row1=3, row2=0, scale=5)
  E4 = elementary_matrix(B.nrows(), row1=1, scale=-1/52)
  E5 = elementary_matrix(B.nrows(), row1=0, row2=1, scale=6)
  E6 = elementary_matrix(B.nrows(), row1=2, row2=1, scale=19)
  E7 = elementary_matrix(B.nrows(), row1=3, row2=1, scale=21)
  E8 = elementary_matrix(B.nrows(), row1=2, scale=13)
  E9 = elementary_matrix(B.nrows(), row1=0, row2=2, scale=-66/13)
  E10 = elementary_matrix(B.nrows(), row1=1, row2=2, scale=2/13)
  E11 = elementary_matrix(B.nrows(), row1=3, row2=2, scale=16/13)
\end{sagesilent}
\newpage
\subsection{A $4\times 5$ Example}

In this example, we will use the Gauss-Jordan algorithm to compute $\rref(B)$
where
\[
  B=\sage{B}
\]
The elementary row operations described in the Gauss-Jordan algorithm are as
follows.
\renewcommand{\stepA}{\xrightarrow{\scriptsize
    \begin{array}{rcrcrr}
      R_2 &+& 11\cdot R_1 &\to& R_2 \\
      R_3 &+& 4\cdot R_1 &\to& R_3 \\
      R_4 &+& 5\cdot R_1 &\to& R_4
    \end{array}
  }}
\renewcommand{\stepB}{\xrightarrow{(-\nicefrac{1}{52})\cdot R_2\to R_2}}
\renewcommand{\stepC}{\xrightarrow{\scriptsize
    \begin{array}{rcrcrr}
      R_1 &+& 6\cdot R_2 &\to& R_1 \\
      R_3 &+& 19\cdot R_2 &\to& R_3 \\
      R_4 &+& 21\cdot R_2 &\to& R_4
    \end{array}
  }}
\renewcommand{\stepD}{\xrightarrow{13\cdot R_3\to R_3}}
\newcommand{\stepE}{\xrightarrow{\scriptsize
    \begin{array}{rcrcrr}
      R_1 &+& (-\nicefrac{66}{13})\cdot R_3 &\to& R_1 \\
      R_2 &+& (\nicefrac{2}{13})\cdot R_3 &\to& R_2 \\
      R_4 &+& (\nicefrac{16}{13})\cdot R_3 &\to& R_4
    \end{array}
  }}
\begin{align*}
  \sage{B}
  &\stepA \sage{E3*E2*E1*B} \\
  &\stepB \sage{E4*E3*E2*E1*B} \\
  &\stepC \sage{E7*E6*E5*E4*E3*E2*E1*B} \\
  &\stepD \sage{E8*E7*E6*E5*E4*E3*E2*E1*B} \\
  &\stepE \sage{E11*E10*E9*E8*E7*E6*E5*E4*E3*E2*E1*B} \\
\end{align*}
This shows that
\[
  \rref(B)=\rref\sage{B}=\sage{B.rref()}
\]
There are three pivot columns and two nonpivot columns in $\rref(B)$.  This
means that $\rank(B)=\sage{B.rank()}$ and
$\nullity(B)=\sage{B.right_nullity()}$.

\section{Computing rref (Systems)}

\subsection{Application Example: Real Estate Development}

Recall from Unit 2 Section 1 the real estate developer planning on building a
$240$-unit apartment complex. Each apartment in the complex will have either
one, two, or three bedrooms. Market research suggests that the number of
one-bedroom apartments should be the same as the total number of two- and
three-bedroom apartments. Research also suggests that the complex should have
four times as many one-bedroom apartments as three-bedroom apartments.

In Unit 2 Section 1, we found that the system of linear equations
\[
  \begin{array}{rcrcrcr}
    x_1 &+& x_2 &+&    x_3 &=& 240 \\
    x_1 &-& x_2 &-&    x_3 &=& 0 \\
    x_1 & &     &-& 4\,x_3 &=& 0
  \end{array}
\]
describes the numbers of one-, two-, and three-bedroom apartments that the
developer will build. We can now \emph{solve} this system by using the
Gauss-Jordan algorithm. The elementary row operations described in the
Gauss-Jordan algorithm are as follows.
\begin{sagesilent}
  A = matrix([[1, 1, 1], [1, -1, -1], [1, 0, -4]])
  b = matrix.column([240, 0, 0])
  M = A.augment(b, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-1)
  E2 = elementary_matrix(A.nrows(), row1=2, row2=0, scale=-1)
  E3 = elementary_matrix(A.nrows(), row1=1, scale=-1/2)
  E4 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=-1)
  E5 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=1)
  E6 = elementary_matrix(A.nrows(), row1=2, scale=-1/4)
  E7 = elementary_matrix(A.nrows(), row1=1, row2=2, scale=-1)
\end{sagesilent}
\newcommand{\myDeveloper}{
    \begin{blockarray}{*{4}{r}}
    \begin{block}{cccc}
      x_1 & x_2 & x_3 \\
    \end{block}
    \begin{block}{[rrr|r]}
      1 & 1 & 1 & 240 \\
      1 & -1 & -1 & 0 \\
      1 & 0 & -4 & 0 \\
    \end{block}%
  \end{blockarray}%
}
\newcommand{\myDeveloperRref}{
    \begin{blockarray}{*{4}{r}}
    \begin{block}{cccc}
      x_1 & x_2 & x_3 \\
    \end{block}
    \begin{block}{[rrr|r]}
      1 & 0 & 0 & 120 \\
      0 & 1 & 0 & 90 \\
      0 & 0 & 1 & 30 \\
    \end{block}%
  \end{blockarray}%
}
\newcommand{\myDStepA}{
  \xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_2 &-& R_1 &\to& R_2 \\
      R_3 &-& R_1 &\to& R_3
    \end{array}
  }
}
\newcommand{\myDStepB}{
  \xrightarrow{(-\frac{1}{2})\cdot R_2\to R_2}
}
\newcommand{\myDStepC}{
  \xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_1 &-& R_2 &\to& R_1 \\
      R_3 &+& R_2 &\to& R_3
    \end{array}
  }
}
\newcommand{\myDStepD}{
  \xrightarrow{(-\frac{1}{4})\cdot R_2\to R_2}
}
\newcommand{\myDStepE}{
  \xrightarrow{R_2-R_3\to R_2}
}
\begin{align*}
  \myDeveloper
  &\myDStepA \sage{E2*E1*M} \\
  &\myDStepB \sage{E3*E2*E1*M} \\
  &\myDStepC \sage{E5*E4*E3*E2*E1*M} \\
  &\myDStepD \sage{E6*E5*E4*E3*E2*E1*M} \\
  &\myDStepE \sage{E7*E6*E5*E4*E3*E2*E1*M} \\
\end{align*}
This shows that
\[
\rref\myDeveloper=\myDeveloperRref
\]
There is exactly one solution to the system and it is given by $x_1=120$,
$x_2=90$, and $x_3=30$. This means that the developer will build $120$
one-bedroom apartments, $90$ two-bedroom apartments, and $30$ three-bedroom
apartments.


\subsection{Application Example: Agriculture}

Recall from Unit 2 Section 1 the farmer who will spend \mymon{31100} growing
soybean and rice plants on \nounit{1000} acres of land. The cost of growing
soybeans is \mymon{36} per acre and the cost of growing rice is \mymon{29} per
acre.

In Unit 2 Section 1, we found that the system of linear equations
\[
  \begin{array}{rcrcr}
    s &+& r &=& 1000 \\
    36\,s &+& 29\,r &=& 31100
  \end{array}
\]
describes the number of acres of soybeans and rice that the farmer will grow. We
can now solve this system by using the Gauss-Jordan algorithm. The elementary
row operations described in the Gauss-Jordan algorithm are as follows
\newcommand{\myRice}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      s & r \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 1 & 1000 \\
      36 & 29 & 31100 \\
    \end{block}%
  \end{blockarray}%
}
\newcommand{\myRiceRref}{
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      s & r \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 0 & 300 \\
      0 & 1 & 700 \\
    \end{block}%
  \end{blockarray}%
}
\begin{sagesilent}
  A = matrix([[1, 1], [36, 29]])
  var('s r')
  vx = matrix.column([s, r])
  b = matrix.column([1000, 31100])
  M = A.augment(b, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-36)
  E2 = elementary_matrix(A.nrows(), row1=1, scale=-1/7)
  E3 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=-1)
\end{sagesilent}
\begin{align*}
  \myRice
  &\xrightarrow{R_2-36\cdot R_1\to R_2} \sage{E1*M} \\
  &\xrightarrow{(-\frac{1}{7})\cdot R_2\to R_2} \sage{E2*E1*M} \\
  &\xrightarrow{R_1-R_2\to R_1}\sage{E3*E2*E1*M}
\end{align*}
This shows that
\[
  \rref\myRice=\myRiceRref
\]
There is exactly one solution to the system and it is given by $s=300$ and
$r=700$. This means that the farmer will plant \nounit{300} acres of soybeans
and \nounit{700} acres of rice.



\subsection{Application Example: Traffic Control}

Recall the flow of traffic at four intersections in a city during a typical
morning rush hour described in Unit 2 Section 1.
\[
\begin{tikzpicture}[line join=round, line cap=round, scale=1]

  \pgfmathsetmacro{\myinner}{4}
  \pgfmathsetmacro{\myouter}{2}
  \pgfmathsetmacro{\myinnermidA}{(\myouter/2)/(2*\myouter+\myinner)}
  \pgfmathsetmacro{\myinnermidB}{(\myouter*3/2+\myinner)/(2*\myouter+\myinner)}

  \coordinate (e1) at (1, 0);
  \coordinate (e2) at (0, 1);

  \coordinate (s1b) at ($ \myouter*(e1) $);
  \coordinate (s1t) at ($ (s1b) + {2*\myouter+\myinner}*(e2) $);
  \coordinate (s2b) at ($ {\myouter+\myinner}*(e1) $);
  \coordinate (s2t) at ($ (s2b) + {2*\myouter+\myinner}*(e2) $);
  \coordinate (a4l) at ($ \myouter*(e2) $);
  \coordinate (a4r) at ($ (a4l) + {2*\myouter+\myinner}*(e1)$);
  \coordinate (a3l) at ($ {\myouter+\myinner}*(e2) $);
  \coordinate (a3r) at ($ (a3l) + {2*\myouter+\myinner}*(e1) $);

  \node at (s1t) [above] {1st St.};
  \node at (s2t) [above] {2nd St.};
  \node at (a3l) [left] {3rd Ave.};
  \node at (a4l) [left] {4th Ave.};

  \node at (a3r) [right] {$\frac{2000}{\operatorname{hr}}$ max};
  \node at (a4r) [right] {$\frac{2000}{\operatorname{hr}}$ max};

  \node at (s1b) [below] {$\frac{1000}{\operatorname{hr}}$ max};
  \node at (s2b) [below] {$\frac{1000}{\operatorname{hr}}$ max};

  \begin{scope}[very thick,decoration={
      markings,
      mark=at position \myinnermidA with {\arrow{>}},
      mark=at position 0.5 with {\arrow{>}},
      mark=at position \myinnermidB with {\arrow{>}},
    }
  ]
  \draw[ultra thick, blue, postaction={decorate}]
  (s1t) -- (s1b)
  node [midway, left, black] {$x_1$}
  node [pos=\myinnermidA, left, black] {300}
  node [pos=\myinnermidB, left, black] {700};

  \draw[ultra thick, blue, postaction={decorate}]
  (s2b) -- (s2t)
  node [midway, right, black] {$x_2$}
  node [pos=\myinnermidA, right, black] {400}
  node [pos=\myinnermidB, right, black] {500};

  \draw[ultra thick, red, postaction={decorate}]
  (a3l) -- (a3r)
  node [midway, above, black] {$x_3$}
  node [pos=\myinnermidA, above, black] {1200}
  node [pos=\myinnermidB, above, black] {800};

  \draw[ultra thick, red, postaction={decorate}]
  (a4r) -- (a4l)
  node [midway, below, black] {$x_4$}
  node [pos=\myinnermidA, below, black] {1400}
  node [pos=\myinnermidB, below, black] {1300};
\end{scope}

\end{tikzpicture}
\]
In Unit 2 Section 1, we found that the system of linear equations
\[
  \begin{array}{rcrcrcrcr}
    x_1 & &     &+& x_3 & &     &=& 1500 \\
    x_1 & &     & &     &+& x_4 &=& 2000 \\
        & & x_2 & &     &+& x_4 &=& 1800 \\
        & & x_2 &+& x_3 & &     &=& 1300
  \end{array}
\]
describes the rates of flow of traffic $x_1$, $x_2$, $x_3$, and $x_4$ along
$1$st St., $2$nd St., $3$rd Ave., and $4$th Ave.\ respectively. We can now solve
this system by using the Gauss-Jordan algorithm. The elementary row operations
described in the Gauss-Jordan algorithm are as follows.
\newcommand{\myTraffic}{
  \begin{blockarray}{*{5}{r}}
    \begin{block}{ccccc}
      x_1 & x_2 & x_3 & x_4 & \\
    \end{block}
    \begin{block}{[rrrr|r]}
      1 & 0 & 1 & 0 & 1500 \\
      1 & 0 & 0 & 1 & 2000 \\
      0 & 1 & 0 & 1 & 1800 \\
      0 & 1 & 1 & 0 & 1300 \\
    \end{block}%
  \end{blockarray}%
}
\newcommand{\myTrafficRref}{
  \begin{blockarray}{*{5}{r}}
    \begin{block}{ccccc}
      x_1 & x_2 & x_3 & x_4 & \\
    \end{block}
    \begin{block}{[rrrr|r]}
      1 & 0 & 0 & 1 & 2000 \\
      0 & 1 & 0 & 1 & 1800 \\
      0 & 0 & 1 & -1 & -500 \\
      0 & 0 & 0 & 0 & 0 \\
    \end{block}%
  \end{blockarray}%
}
\begin{sagesilent}
  A = matrix([[1, 0, 1, 0], [1, 0, 0, 1], [0, 1, 0, 1], [0, 1, 1, 0]])
  b = matrix.column([1500, 2000, 1800, 1300])
  M = A.augment(b, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-1)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=2)
  E3 = elementary_matrix(A.nrows(), row1=3, row2=1, scale=-1)
  E4 = elementary_matrix(A.nrows(), row1=2, scale=-1)
  E5 = elementary_matrix(A.nrows(), row1=0, row2=2, scale=-1)
  E6 = elementary_matrix(A.nrows(), row1=3, row2=2, scale=-1)
\end{sagesilent}
\newcommand{\myStack}{\xrightarrow{\scriptsize
  \begin{array}{rcrcr}
    R_1 &-& R_3 &\to& R_1 \\
    R_4 &-& R_3 &\to& R_4
  \end{array}
  }
}
\begin{align*}
  \myTraffic
  &\xrightarrow{R_2-R_1\to R_2} \sage{E1*M} \\
  &\xrightarrow{R_2\leftrightarrow R_3}\sage{E2*E1*M} \\
  &\xrightarrow{R_4-R_2\to R_4}\sage{E3*E2*E1*M} \\
  &\xrightarrow{-R_3\to R_3}\sage{E4*E3*E2*E1*M} \\
  &\myStack\sage{E6*E5*E4*E3*E2*E1*M}
\end{align*}
This shows that
\[
\rref\myTraffic=\myTrafficRref
\]
The system is consistent because there is no pivot in the augmented column. The
dependent variables in the system are $\Set*{x_1, x_2, x_3}$ and the free
variable in the system is $\Set{x_4}$. The solutions are given by
\begin{sagesilent}
  var('x1 x2 x3 x4')
  vx = matrix.column([x1, x2, x3, x4])
  xall = matrix.column([2000-x4, 1800-x4, -500+x4, x4])
  xp = matrix.column([2000, 1800, -500, 0])
  xc1 = matrix.column([-1, -1, 1, 1])
\end{sagesilent}
\[
  \sage{vx}
  =
  \sage{xall}
\]
Once the flow $x_4$ is set, the other flows are determined by this
equation.

Since the maximum flow along 1st.\ St.\ and 2nd.\ St.\ is \nounit{1000} vehicles
per hour, the flows $x_1$ and $x_2$ must satisfy
\begin{align*}
  0\leq x_1\leq 1000 && 0\leq x_2\leq 1000
\end{align*}
The first two equations in the reduced row echelon form of the system give
\begin{align*}
  x_1 &= -x_4+2000 & x_2 &= -x_4+1800
\end{align*}
The inequalities then become
\begin{align*}
  0\leq -x_4+2000\leq 1000 && 0\leq -x_4+1800\leq 1000
\end{align*}
Simplifying the inequalities then gives
\begin{align*}
  1000\leq x_4\leq 2000 && 800\leq x_4\leq 1800
\end{align*}
This means that the choice of flow $x_4$ must satisfy
\[
  1000\leq x_4\leq 1800
\]
So, by setting the flow $x_4$ to any value between $1000$ and $1800$ vehicles
per hour, the city planners will ensure that traffic will not jam and they can
find the other flow rates by using the equations $x_1=-x_4+2000$,
$x_2=-x_4+1800$, and $x_3=x_4-500$.


\begin{sagesilent}
  A = matrix([(-2, -10, 3, -2), (-1, -5, 1, -2)])
  A.subdivide([],[3])
  E1 = elementary_matrix(A.nrows(), row1=0, scale=-1/2)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=1)
  E3 = elementary_matrix(A.nrows(), row1=1, scale=-2)
  E4 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=3/2)
\end{sagesilent}
\subsection{A $2\times 3$ Example}

In this example, we will use the Gauss-Jordan algorithm to solve the system
\[
  \begin{blockarray}{*{4}{r}}
    \begin{block}{cccc}
      x_1 & x_2 & x_3 & \\
    \end{block}
    \begin{block}{[rrr|r]}
      -2 & -10 & 3 & -2 \\
      -1 & -5 & 1 & -2 \\
    \end{block}%
  \end{blockarray}%
\]
The elementary row operations described in the Gauss-Jordan algorithm are as
follows.
\renewcommand{\stepA}{\xrightarrow{(-\nicefrac{1}{2})\cdot R_1\to R_1}}
\renewcommand{\stepB}{\xrightarrow{R_2+R_1\to R_2}}
\renewcommand{\stepC}{\xrightarrow{-2\cdot R_2\to R_2}}
\renewcommand{\stepD}{\xrightarrow{R_1+(\nicefrac{3}{2})\cdot R_2\to R_1}}
\begin{align*}
  \sage{A}
  &\stepA \sage{E1*A} \\
  &\stepB \sage{E2*E1*A} \\
  &\stepC \sage{E3*E2*E1*A} \\
  &\stepD \sage{E4*E3*E2*E1*A} \\
\end{align*}
This shows that
\[
  \rref\sage{A}=\sage{A.rref()}
\]
The system is thus consistant with dependent variables $\Set{x_1, x_3}$ and free
variable $\Set{x_2}$. The infinitely many solutions to the system are given by
\begin{sagesilent}
  var('x1 x2 x3 c1')
  vx = matrix.column([x1, x2, x3])
  xf = matrix.column([4-5*c1, c1, 2])
  xp = matrix.column([4, 0, 2])
  xc1 = matrix.column([-5, 1, 0])
\end{sagesilent}
\[
  \sage{vx}
  = \sage{xf}
  = \sage{xp}+c_1\sage{xc1}
\]
Note that the rank of this system is two and the nullity of the system is one.

\begin{sagesilent}
  A = matrix([(5, 4, 3, 0), (-4, -3, -3, -1), (1, 3, -5, -10)])
  A.subdivide([], [3])
  E1 = elementary_matrix(A.nrows(), row1=0, scale=1/5)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=4)
  E3 = elementary_matrix(A.nrows(), row1=2, row2=0, scale=-1)
  E4 = elementary_matrix(A.nrows(), row1=1, scale=5)
  E5 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=-4/5)
  E6 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=-11/5)
  E7 = elementary_matrix(A.nrows(), row1=0, row2=2, scale=-3)
  E8 = elementary_matrix(A.nrows(), row1=1, row2=2, scale=3)
\end{sagesilent}
\newpage
\subsection{A $3\times 3$ Example}

In this example, we will use the Gauss-Jordan algorithm to solve the system
\[
  \begin{blockarray}{*{4}{r}}
    \begin{block}{cccc}
      x_1 & x_2 & x_3 & \\
    \end{block}
    \begin{block}{[rrr|r]}
      5 & 4 & 3 & 0 \\
      -4 & -3 & -3 & -1 \\
      1 & 3 & -5 & -10 \\
    \end{block}%
  \end{blockarray}%
\]
The elementary row operations described in the Gauss-Jordan algorithm are as
follows.
\renewcommand{\stepA}{\xrightarrow{(\nicefrac{1}{5})\cdot R_1\to R_1}}
\renewcommand{\stepB}{\xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_2 &+& 4\cdot R_1 &\to& R_2 \\
      R_3 &-&        R_1 &\to& R_3
    \end{array}
  }}
\renewcommand{\stepC}{\xrightarrow{5\cdot R_2\to R_2}}
\renewcommand{\stepD}{\xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_1 &-& (\nicefrac{4}{5})\cdot R_2 &\to& R_1 \\
      R_3 &-& (\nicefrac{11}{5})\cdot R_2 &\to& R_3
    \end{array}
  }}
\renewcommand{\stepE}{\xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_1 &-& 3\cdot R_3 &\to& R_1 \\
      R_2 &+& 3\cdot R_3 &\to& R_2
    \end{array}
  }}
\begin{align*}
  \sage{A}
  &\stepA \sage{E1*A} \\
  &\stepB \sage{E3*E2*E1*A} \\
  &\stepC \sage{E4*E3*E2*E1*A} \\
  &\stepD \sage{E6*E5*E4*E3*E2*E1*A} \\
  &\stepE \sage{E8*E7*E6*E5*E4*E3*E2*E1*A}
\end{align*}
The system is thus consistent with dependent variables $\Set{x_1, x_2, x_3}$ and
no free variables. The unique solution to the system is
\begin{sagesilent}
  var('x1 x2 x3')
  vx = matrix.column([x1, x2, x3])
  xp = matrix.column([1, -2, 1])
\end{sagesilent}
\[
  \sage{vx}=\sage{xp}
\]
Note that the system has rank three and nullity zero.


\begin{sagesilent}
  A = matrix([(-2, 3, 19, -2, -22), (0, 1, 3, -2, -5), (-3, 2, 21, 2, -21)])
  A.subdivide([], [4])
  E1 = elementary_matrix(A.nrows(), row1=0, scale=-1/2)
  E2 = elementary_matrix(A.nrows(), row1=2, row2=0, scale=3)
  E3 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=3/2)
  E4 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=5/2)
  E5 = elementary_matrix(A.nrows(), row1=2, scale=-2)
\end{sagesilent}
\newpage
\subsection{A $3\times 4$ Example}

In this example, we will use the Gauss-Jordan algorithm to solve the system
\[
  \begin{blockarray}{*{5}{r}}
    \begin{block}{ccccc}
      x_1 & x_2 & x_3 & x_4 & \\
    \end{block}
    \begin{block}{[rrrr|r]}
      -2 & 3 & 19 & -2 & -22 \\
      0 & 1 & 3 & -2 & -5 \\
      -3 & 2 & 21 & 2 & -21 \\
    \end{block}%
  \end{blockarray}%
\]
The elementary row operations described in the Gauss-Jordan algorithm are as
follows.
\renewcommand{\stepA}{\xrightarrow{(-\nicefrac{1}{2})\cdot R_1\to R_1}}
\renewcommand{\stepB}{\xrightarrow{R_3+3\cdot R_1\to R_3}}
\renewcommand{\stepC}{\xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_1 &+& (\nicefrac{3}{2})\cdot R_2 &\to& R_1 \\
      R_3 &+& (\nicefrac{5}{2})\cdot R_2 &\to& R_3
    \end{array}
  }}
\renewcommand{\stepD}{\xrightarrow{-2\cdot R_3\to R_3}}
\renewcommand{\stepE}{\xrightarrow{\scriptsize
    \begin{array}{rcrcr}
      R_1 &-& (\nicefrac{7}{2})\cdot R_3 &\to& R_1 \\
      R_2 &+& 5\cdot R_3 &\to& R_2
    \end{array}
  }}
\begin{align*}
  \sage{A}
  &\stepA \sage{E1*A} \\
  &\stepB \sage{E2*E1*A} \\
  &\stepC \sage{E4*E3*E2*E1*A} \\
  &\stepD \sage{E5*E4*E3*E2*E1*A} \\
  &\stepE \sage{A.rref()}
\end{align*}
This shows that
\[
  \rref\sage{A}=\sage{A.rref()}
\]
The system is thus inconsistent and therefore has no solutions.

\section{Video Supplements}

An additional example demonstrating the Gauss Jordan algorithm can be found in
the first video supplement to this section:
\url{https://warpwire.duke.edu/w/IxoCAA/}.

The second video supplement to this section demonstrates how to use the computer
algebra system \texttt{sage} to compute the reduced row echelon form of a
system: \url{https://warpwire.duke.edu/w/JBoCAA/}.




\end{document}
