\documentclass[12pt]{article}

\title{Unit 2.1: Augmented Matrices and Reduced Row-Echelon Form}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='\\lvert', right='\\rvert')
\end{sagesilent}


\maketitle

\tableofcontents


\newpage
\section{Motivation}
\label{sec:intro}

Recall the system of linear equations found in Unit 1 Section 3
\begin{equation}
  \begin{array}{rcrcr}
    x &+&     y &=& 800 \\
    5\,x &+& 20\,y &=& 8500
  \end{array}\label{eq:cases}
\end{equation}
In this system, the variable $x$ represents the total number of ``budget phone
cases'' sold by a company in 2017 and the variable $y$ represents the number of
``deluxe phone cases'' sold by the same company in 2017. The first equation in
this system communicates that the company sold a total of \nounit{800} phone
cases in 2017. Since the budget case is sold at a price of \mymon{5} per case
and the deluxe case is sold at a price of \mymon{20} per case, the second
equation in the system communicates that the company had a total revenue of
\mymon{8500} in 2017.

Throughout Unit 2, we will develop techniques for solving systems of linear
equations like the one in \eqref{eq:cases}. In this section, we will give an
overview of the basic linear algebraic tools for studying these systems in
matrix form.


\section{Augmented Matrices}
\label{sec:mats}

Recall the \emph{matrix form} of a system of linear equations, covered in
Unit 1 Section 3.

\begin{genbox}{Converting a System of Linear Equations into Matrix Form}
  Consider a linear system with $m$ equations and $n$ variables.
  \[
    \begin{array}{ccccccccc}
      {\color{blue}a_{11}}\,x_1 & + & {\color{red}a_{12}}\,x_2 & + & \dotsb & + & {\color{violet}a_{1n}}\,x_n & = & b_1    \\
      {\color{blue}a_{21}}\,x_1 & + & {\color{red}a_{22}}\,x_2 & + & \dotsb & + & {\color{violet}a_{2n}}\,x_n & = & b_2    \\
      \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
      {\color{blue}a_{m1}}\,x_1 & + & {\color{red}a_{m2}}\,x_2 & + & \dotsb & + & {\color{violet}a_{mn}}\,x_n & = & b_m
    \end{array}
  \]
  The system can be written in \emph{matrix form}
  $A\vv{x}=\vv{b}$ where
  \newcommand{\myA}{
    \left[
      \begin{array}{cccc}
        {\color{blue}a_{11}}  & {\color{red}a_{12}}  & \dotsb  & {\color{violet}a_{1n}} \\
        {\color{blue}a_{21}}  & {\color{red}a_{22}}  & \dotsb  & {\color{violet}a_{2n}} \\
        \vdots    & \vdots               & \ddots  & \vdots                 \\
        {\color{blue}a_{m1}}  & {\color{red}a_{m2}}  & \dotsb  & {\color{violet}a_{mn}}
      \end{array}
    \right]}
  \newcommand{\myVX}{
    \begin{bmatrix}
      x_1\\ x_2\\ \vdots\\ x_n
    \end{bmatrix}
  }
  \newcommand{\myVB}{
    \begin{bmatrix}
      b_1\\ b_2\\ \vdots\\ b_m
    \end{bmatrix}
  }
  \begin{align*}
    A &= \myA & \vv{x} &= \myVX & \vv{b} &= \myVB
  \end{align*}
  The $m\times n$ matrix $A$ is called the \emph{coefficient matrix} for the
  system.
\end{genbox}

For example, the system of equations \eqref{eq:cases} describing the number of
phone cases sold by a company may be written as $A\vv{x}=\vv{b}$ where
\begin{sagesilent}
  var('x y')
  vx = matrix.column([x, y])
  A = matrix([[1, 1], [5, 20]])
  b = matrix.column([800, 8500])
\end{sagesilent}
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
Another useful way to organize the information in a system of linear equations
is to insert the data defining the system into an \emph{augmented matrix}. The
augmented matrix of the phone case system \eqref{eq:cases} is
\[
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      x & y & \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 1 & 800 \\
      5 & 20 & 8500 \\
    \end{block}%
  \end{blockarray}%
\]
Each row of this matrix organizes the coefficients in one equation. Using
augmented matrices is an efficient way to manipulate systems of linear
equations.

\begin{genbox}{Converting a System of Linear Equations into an Augmented Matrix}
  Consider a linear system with $m$ equations and $n$ variables.
  \[
    \begin{array}{ccccccccc}
      {\color{blue}a_{11}}\,x_1 & + & {\color{red}a_{12}}\,x_2 & + & \dotsb & + & {\color{violet}a_{1n}}\,x_n & = & b_1    \\
      {\color{blue}a_{21}}\,x_1 & + & {\color{red}a_{22}}\,x_2 & + & \dotsb & + & {\color{violet}a_{2n}}\,x_n & = & b_2    \\
      \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
      {\color{blue}a_{m1}}\,x_1 & + & {\color{red}a_{m2}}\,x_2 & + & \dotsb & + & {\color{violet}a_{mn}}\,x_n & = & b_m
    \end{array}
  \]
  The system can be written as an \emph{augmented matrix}
  \newcommand{\myA}{ \left[
      \begin{array}{cccc|c}
        {\color{blue}a_{11}}  & {\color{red}a_{12}}  & \dotsb  & {\color{violet}a_{1n}} & b_1\\
        {\color{blue}a_{21}}  & {\color{red}a_{22}}  & \dotsb  & {\color{violet}a_{2n}} & b_2\\
        \vdots    & \vdots               & \ddots  & \vdots                 &\\
        {\color{blue}a_{m1}}  & {\color{red}a_{m2}}  & \dotsb  & {\color{violet}a_{mn}} & b_m
      \end{array}
    \right]}
  \[
    \myA
  \]
  This augmented matrix is of the form $[A\mid\vv{b}]$. The $m\times n$ matrix
  $A$ is the \emph{coefficient matrix} of the system and the vector $\vv{b}$ is
  the \emph{augmented column} of the system.
\end{genbox}


\begin{sagesilent}
  var('p q r x y z')
  A = matrix([(-10, 0, 1, -3, 0, 12), (-1, 4, 2, -1, 5, 1), (0, -1, 0, 5, -1, -2), (-1, -70, -1, -2, -1, -1)])
  vx = matrix.column([p, q, r, x, y, z])
  b = matrix.column([13, -45, 11, 271])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{example}
  The linear system of equations
  \[
    \begin{array}{rcrcrcrcrcrcr}
      -10\,p & &      &+& r    &-& 3\,x & &      &+& 12\,z &=& 13 \\
      -p &+& 4\,q &+& 2\,r &-&    x &+& 5\,y &+&     z &=& -45 \\
             & &   -q & &      & & 5\,x &-&    y &-&  2\,z &=& 11 \\
      -\,p &-& 70\,q &-& r &-& 2\,x &-& y &-& z &=& 271
    \end{array}
  \]
  is represented by the augmented matrix
  \[
    \sage{M}
  \]
\end{example}


\newpage
\begin{sagesilent}
  totunits = 240
\end{sagesilent}
\subsection{Application Example: Real Estate Development}

A real estate developer is planning to build a new $\sage{totunits}$-unit
apartment complex. Each apartment in the complex will have either one, two, or
three bedrooms. Market research suggests that the number of one-bedroom
apartments should be the same as the total number of two- and three-bedroom
apartments. Research also suggests that the complex should have four times as
many one-bedroom apartments as three-bedroom apartments. Set up a system of
linear equations to describe the numbers of one-, two-, and three-bedroom
apartments the developer will build in the complex.

To address this question, we begin by introducing the variables
\begin{align*}
  x_1 &= \textnormal{number of one-bedroom apartments} \\
  x_2 &= \textnormal{number of two-bedroom apartments} \\
  x_3 &= \textnormal{number of three-bedroom apartments}
\end{align*}
The fact that the total number of apartments in the complex will be
$\sage{totunits}$ can be written algebraically as
\[
  x_1+x_2+x_3=\sage{totunits}
\]
The fact that the number of one-bedroom apartments should be
the same as the total number of two- and three-bedroom apartments can be written
algebraically as
\[
  x_1=x_2+x_3
\]
The fact that the number of one-bedroom apartments should be four times the
number of three-bedroom apartments can be written algebraically as
\[
  x_1=4\,x_3
\]
Combining these three equations gives the system of linear equations
\begin{align*}
  x_1 + x_2 +x_3 &= \sage{totunits} \\
  x_1 &= x_2+x_3 \\
  x_1 &= 4\,x_3
\end{align*}
These equations can be rearranged to obtain
\[
  \begin{array}{rcrcrcr}
    x_1 &+& x_2 &+&    x_3 &=& \sage{totunits} \\
    x_1 &-& x_2 &-&    x_3 &=& 0 \\
    x_1 & &     &-& 4\,x_3 &=& 0
  \end{array}
\]
The matrix form of this system is $A\vv{x}=\vv{b}$ where
\begin{sagesilent}
  A = matrix([[1, 1, 1], [1, -1, -1], [1, 0, -4]])
  var('x1 x2 x3')
  vx = matrix.column([x1, x2, x3])
  b = matrix.column([totunits, 0, 0])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
In augmented form, this system is given by
\[
  \begin{blockarray}{*{4}{r}}
    \begin{block}{cccc}
      x_1 & x_2 & x_3 \\
    \end{block}
    \begin{block}{[rrr|r]}
      1 & 1 & 1 & \sage{totunits} \\
      1 & -1 & -1 & 0 \\
      1 & 0 & -4 & 0 \\
    \end{block}%
  \end{blockarray}%
\]
In Section 2 we will demonstrate how to manipulate this augmented matrix to
\emph{solve} this system.



\newpage
\subsection{Application Example: Agriculture}

A farmer has \mymon{31100} to spend growing soybean and rice plants on
\nounit{1000} acres of land. The cost of growing soybeans is \mymon{36} per
acre and the cost of growing rice is \mymon{29} per acre. Set up a system of
linear equations to describe the number of acres alloted to soybeans and rice.

To address this question, we introduce the variables
\begin{align*}
  s &= \textnormal{number of acres alloted to soybeans} \\
  r &= \textnormal{number of acres alloted to rice}
\end{align*}
Since the farmer has \nounit{1000} acres of land, the total number of acres
allocated to soybeans and rice is \nounit{1000}. Algebraically, this can be
expressed as
\[
  s+r=1000
\]
The farmer has \mymon{31100} to spend on soybean and rice cultivation.  Since
soybeans cost \mymon{36} per acre and rice costs \mymon{29} per acre, the
farmer's budgetary constraint can be expressed algebraically as
\[
  36\,s+29\,r=31100
\]
Combining these two equations gives the system of linear equations
\[
  \begin{array}{rcrcr}
    s &+& r &=& 1000 \\
    36\,s &+& 29\,r &=& 31100
  \end{array}
\]
The matrix form of this system is $A\vv{x}=\vv{b}$ where
\begin{sagesilent}
  A = matrix([[1, 1], [36, 29]])
  var('s r')
  vx = matrix.column([s, r])
  b = matrix.column([1000, 31100])
\end{sagesilent}
\begin{align*}
  A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
In augmented form, this system is given by
\[
  \begin{blockarray}{*{3}{r}}
    \begin{block}{ccc}
      s & r \\
    \end{block}
    \begin{block}{[rr|r]}
      1 & 1 & 1000 \\
      30 & 29 & 31100 \\
    \end{block}%
  \end{blockarray}%
\]
In Section 2 we will demonstrate how to manipulate this augmented matrix to
\emph{solve} this system.

\newpage
\subsection{Application Example: Traffic Control}

The figure below depicts the flow of traffic at four intersections in a city
during a typical morning rush hour.
\[
\begin{tikzpicture}[line join=round, line cap=round]

  \pgfmathsetmacro{\myinner}{4}
  \pgfmathsetmacro{\myouter}{2}
  \pgfmathsetmacro{\myinnermidA}{(\myouter/2)/(2*\myouter+\myinner)}
  \pgfmathsetmacro{\myinnermidB}{(\myouter*3/2+\myinner)/(2*\myouter+\myinner)}

  \coordinate (e1) at (1, 0);
  \coordinate (e2) at (0, 1);

  \coordinate (s1b) at ($ \myouter*(e1) $);
  \coordinate (s1t) at ($ (s1b) + {2*\myouter+\myinner}*(e2) $);
  \coordinate (s2b) at ($ {\myouter+\myinner}*(e1) $);
  \coordinate (s2t) at ($ (s2b) + {2*\myouter+\myinner}*(e2) $);
  \coordinate (a4l) at ($ \myouter*(e2) $);
  \coordinate (a4r) at ($ (a4l) + {2*\myouter+\myinner}*(e1)$);
  \coordinate (a3l) at ($ {\myouter+\myinner}*(e2) $);
  \coordinate (a3r) at ($ (a3l) + {2*\myouter+\myinner}*(e1) $);

  \node at (s1t) [above] {1st St.};
  \node at (s2t) [above] {2nd St.};
  \node at (a3l) [left] {3rd Ave.};
  \node at (a4l) [left] {4th Ave.};

  \begin{scope}[very thick,decoration={
      markings,
      mark=at position \myinnermidA with {\arrow{>}},
      mark=at position 0.5 with {\arrow{>}},
      mark=at position \myinnermidB with {\arrow{>}},
    }
  ]
  \draw[ultra thick, blue, postaction={decorate}]
  (s1t) -- (s1b)
  node [midway, left, black] {$x_1$}
  node [pos=\myinnermidA, left, black] {300}
  node [pos=\myinnermidB, left, black] {700};

  \draw[ultra thick, blue, postaction={decorate}]
  (s2b) -- (s2t)
  node [midway, right, black] {$x_2$}
  node [pos=\myinnermidA, right, black] {400}
  node [pos=\myinnermidB, right, black] {500};

  \draw[ultra thick, red, postaction={decorate}]
  (a3l) -- (a3r)
  node [midway, above, black] {$x_3$}
  node [pos=\myinnermidA, above, black] {1200}
  node [pos=\myinnermidB, above, black] {800};

  \draw[ultra thick, red, postaction={decorate}]
  (a4r) -- (a4l)
  node [midway, below, black] {$x_4$}
  node [pos=\myinnermidA, below, black] {1400}
  node [pos=\myinnermidB, below, black] {1300};
\end{scope}

\end{tikzpicture}
\]
The arrows indicate the direction of traffic on each one-way road. The numbers
at each intersection give the average number of vehicles entering and exiting
the intersection per hour. Each of 1st Street and 2nd Street can accomodate a
maximum of 1000 vehicles per hour without causing a traffic jam. Each of 3rd and
4th Avenues can accomodate a maximum of 2000 vehicles per hour without causing a
traffic jam.

City planners can control the rates of flow $x_1$, $x_2$, $x_3$, and $x_4$ (each
measured in vehicles per hour) by manipulating the traffic lights stationed at
each intersection. At what levels should the flows $x_1$, $x_2$, $x_3$, and
$x_4$ be set to avoid a traffic jam?

To avoid a traffic jam, the city planners must ensure that every vehicle
entering an intersection also exits the intersection. At the intersection of
$1$st and $3$rd, this condition is represented algebraically as
\[
  x_1+x_3 = 1200+300 = 1500
\]
At $1$st and $4$th the condition is
\[
  x_1+x_4 = 1300+700 = 2000
\]
At $2$nd and $4$th the condition is
\[
  x_2 + x_4 = 1400 + 400 = 1800
\]
At $2$nd and $3$rd the condition is
\[
  x_2 + x_3 = 800+500 = 1300
\]
Putting these four equations together gives the linear system of equations
\[
  \begin{array}{rcrcrcrcr}
    x_1 & &     &+& x_3 & &     &=& 1500 \\
    x_1 & &     & &     &+& x_4 &=& 2000 \\
        & & x_2 & &     &+& x_4 &=& 1800 \\
        & & x_2 &+& x_3 & &     &=& 1300
  \end{array}
\]
The matrix form of this system is $A\vv{x}=\vv{b}$ where
\begin{sagesilent}
  A = matrix([[1, 0, 1, 0], [1, 0, 0, 1], [0, 1, 0, 1], [0, 1, 1, 0]])
  var('x1 x2 x3 x4')
  vx = matrix.column([x1, x2, x3, x4])
  b = matrix.column([1500, 2000, 1800, 1300])
\end{sagesilent}
\begin{align*}
  A &= \sage{vx} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
\end{align*}
In augmented form, this system is given by
\[
  \begin{blockarray}{*{5}{r}}
    \begin{block}{ccccc}
      x_1 & x_2 & x_3 & x_4 & \\
    \end{block}
    \begin{block}{[rrrr|r]}
      1 & 0 & 1 & 0 & 1500 \\
      1 & 0 & 0 & 1 & 2000 \\
      0 & 1 & 0 & 1 & 1800 \\
      0 & 1 & 1 & 0 & 1300 \\
    \end{block}%
  \end{blockarray}%
\]
In Section 2 we will demonstrate how to manipulate this augmented matrix to
\emph{solve} this system.

\newpage
\section{Reduced Row-Echelon Form}
\label{sec:rref}

\subsection{Definition}
\label{sec:rrefdef}

In general, systems of linear equations are difficult to solve. However, when a
system is in a certain special form (\emph{reduced row echelon form}) there is
an explicit procedure for analyzing the system.

\begin{genbox}{Definition of Reduced Row Echelon Form}
  A matrix $A$ is in \emph{reduced row echelon form (rref)} if each of the
  following is satisfied:
  \begin{enumerate}
  \item Any zero-rows occur at the bottom.
  \item The first nonzero entry of a nonzero row is a $1$ (called a
    \emph{pivot}).
  \item Every pivot occurs to the right of the pivots above it.
  \item All nonpivot entries in a column containing a pivot are zero.
  \end{enumerate}
\end{genbox}

Checking if a matrix is in reduced row echelon form requires that one check
these four rules.

\begin{example}
  The matrix
  \[
    A
    =
    \left[
      \begin{array}{rrrrrrr}
        \cfbox{blue}{1} & 2 & 0 & -5 & 0 & 1 & -1 \\
        0 & 0 & \cfbox{blue}{1} & 3 & 0 & -1 & 1 \\
        0 & 0 & 0 & 0 & \cfbox{blue}{1} & -1 & -2 \\
        0 & 0 & 0 & 0 & 0 & 0 & 0 \\
        0 & 0 & 0 & 0 & 0 & 0 & 0
      \end{array}
    \right]
  \]
  is in reduced row echelon form.
  \begin{enumerate}
  \item $A$ has two zero-rows, which are the last two rows. \cmark
  \item The first nonzero entry in the nonzero rows are equal to $1$. These
    pivots occur in positions $(1, 1)$, $(2, 3)$, and $(3, 5)$. \cmark
  \item Each pivot occurs to the right of the pivots above it. \cmark
  \item All nonpivot entries in the pivot columns are equal to zero. \cmark
  \end{enumerate}
\end{example}

\begin{example}
  The matrix
  \[
    B
    =
    \left[
      \begin{array}{rrrrr}
        \cfbox{blue}{1} & \cfbox{red}{2} & 0 & 0 & 3 \\
        0 & \cfbox{blue}{1} & 3 & \cfbox{red}{1} & 1 \\
        0 & 0 & 0 & \cfbox{blue}{1} & 2
      \end{array}
    \right]
  \]
  is \emph{not} in reduced row echelon form.
  \begin{enumerate}
  \item $B$ has no rows of zeros. \cmark
  \item The first nonzero entry in the nonzero rows are equal to $1$. These
    pivots occur in positions $(1, 1)$, $(2, 2)$, and $(3, 4)$. \cmark
  \item Each pivot occurs to the right of the pivots above it. \cmark
  \item The $(1, 2)$ entry of $B$ is $b_{12}=2$. This is a nonpivot nonzero
    entry in a pivot column. The $(2, 4)$ entry of $B$ is another nonzero
    nonpivot entry in a pivot column. \xmark
  \end{enumerate}
\end{example}


\begin{example}
  The matrix
  \[
    C =
    \left[
      \begin{array}{rrrrr}
        0 & \cfbox{blue}{1} & 0 & 0 & -2 \\
        \cfbox{blue}{1} & 0 & 1 & 1 & 1 \\
        0 & 0 & 0 & 0 & 0
      \end{array}
    \right]
  \]
  is \emph{not} in reduced row echelon form.
  \begin{enumerate}
  \item $C$ has one row of zeros and this is the last row of $C$. \cmark
  \item The first nonzero entry in the nonzero rows are equal to $1$. These
    pivots occur in positions $(1, 2)$ and $(2, 1)$. \cmark
  \item The pivot in position $(2, 1)$ is to the left of the pivot in position
    $(1, 2)$. \xmark
  \item The nonpivot entries in the pivot columns are equal to zero. \cmark
  \end{enumerate}
\end{example}

\begin{example}
  The matrix
  \[
    D =
    \left[
      \begin{array}{rrrrrr}
        0 & \cfbox{blue}{1} & 6 & 0 & 0 & -4 \\
        0 & 0 & 0 & \cfbox{blue}{1} & 5 & 2 \\
        0 & 0 & 0 & 0 & \cfbox{red}{7} & 0 \\
        0 & 0 & 0 & 0 & 0 & 0
      \end{array}
    \right]
  \]
  is \emph{not} in reduced row echelon form.
  \begin{enumerate}
  \item $D$ has one row of zeros and this is the last row of $D$. \cmark
  \item There are pivots in positions $(1, 2)$ and $(2, 4)$. However, the first
    nonzero entry of row three of $D$ is $7\neq1$. \xmark
  \item Each pivot occurs to the right of the pivots above it. \cmark
  \item The nonpivot entries in the pivot columns are equal to zero. \cmark
  \end{enumerate}
\end{example}

\subsection{Rank, Nullity, Dependency, and Consistency}
\label{sec:ranknull}

Before discussing how to solve systems in reduced row echelon form, it is
necessary to introduce some terminology.

\begin{genbox}{Rank and Nullity}
  The \emph{rank} of a matrix in reduced row echelon form is the number of pivot
  columns. The \emph{nullity} of a matrix in reduced row echelon form is the
  number of nonpivot columns.
\end{genbox}

Determining the rank and the nullity of a matrix in reduced row echelon form
requires one to count the number of pivot and nonpivot columns, respectively.

\begin{example}
  Each of the matrices
  \newcommand{\myA}{
    \left[
      \begin{array}{rrr}
        \cfbox{blue}{1} & -5 & 0 \\
        0 & 0 & \cfbox{blue}{1}
      \end{array}
    \right]
  }
  \newcommand{\myB}{
    \left[
      \begin{array}{rrrrr}
        \cfbox{blue}{1} & 0 & 2 & 0 & -1 \\
        0 & \cfbox{blue}{1} & 3 & 0 & 1 \\
        0 & 0 & 0 & \cfbox{blue}{1} & 1
      \end{array}
    \right]}
  \newcommand{\myC}{
    \left[
      \begin{array}{cccc}
        \cfbox{blue}{1} & 0 & 0 & 0  \\
        0 & \cfbox{blue}{1} & 0 & 0  \\
        0 & 0 &  \cfbox{blue}{1} & 0 \\
        0 & 0 & 0 &  \cfbox{blue}{1}
      \end{array}
    \right]}
  \begin{align*}
    A &= \myA & B &= \myB & C &= \myC
  \end{align*}
  is in reduced row echelon form.

  The matrix $A$ has two pivot columns and one nonpivot column. Thus
  \begin{align*}
    \rank(A) &= 2 & \textnormal{and} && \nullity(A) &= 1
  \end{align*}
  The matrix $B$ has three pivot columns and two nonpivot columns. Thus
  \begin{align*}
    \rank(B) &= 3 & \textnormal{and} && \nullity(B) &= 2
  \end{align*}
  The matrix $C$ has four pivot columns and zero nonpivot columns. Thus
  \begin{align*}
    \rank(C) &= 4 & \textnormal{and} && \nullity(C) &= 0
  \end{align*}
\end{example}

By using the augmented matrix of a system of linear equations, one may apply the
notions of rank and nullity to systems of linear equations.

\begin{genbox}{Free and Dependent Variables}
  The \emph{dependent variables} of an rref system of linear equations are the
  variables in pivot columns. The \emph{free variables} of an rref system are the
  variables in the nonpivot columns.

  The \emph{rank} of a system is the number of dependent variables and the
  \emph{nullity} of a system is the number of free variables.
\end{genbox}

To determine which variables are dependent and which variables are free in an
rref system, one need only determine which variables are in pivot columns and
which are in nonpivot columns respectively.

\begin{example}\label{ex:myex1}
  Consider the system of linear equations given by the augmented matrix
  \[
    \begin{blockarray}{*{5}{r}}
      \begin{block}{ccccc}
        w & x & y & z  &\\
      \end{block}
      \begin{block}{[rrrr|r]}
        \cfbox{blue}{1} & 0 & 2 & 0 & -1 \\
        0 & \cfbox{blue}{1} & -2 & 0 & 0 \\
        0 & 0 & 0 & \cfbox{blue}{1} & 2 \\
      \end{block}%
    \end{blockarray}%
  \]
  This system is in reduced row echelon form. The dependent variables are
  $\Set{w, x, z}$, so the rank of this system is three. The free variable is
  $\Set{y}$, so the nullity of this system is one.
\end{example}

Another useful feature of rref systems is that they can quickly be classified as
\emph{solvable} or \emph{unsolvable}.

\begin{genbox}{Consistent and Inconsistent rref Systems}
  an rref system is
  \begin{description}
  \item[consistent (or solvable)] if the augmented column contains no pivot.
  \item[inconsistent (or unsolvable)] if the augmented column contains a pivot.
  \end{description}
\end{genbox}

So, to check whether or not it is possible to solve an rref system, one need only
determine whether or not all of the pivots are to the left of the augmented
column. In Section \ref{sec:solve-rref} below we will discuss how to obtain the
solutions to consistent systems.

\begin{example}\label{ex:myex2}
  The rref system
  \[
    \begin{blockarray}{*{5}{r}}
      \begin{block}{ccccc}
        x_1 & x_2 & x_3 & x_4  &\\
      \end{block}
      \begin{block}{[rrrr|r]}
        \cfbox{blue}{1} & 0 & 0 & 0 & 4 \\
        0 & 0 & \cfbox{blue}{1} & 2 & -1 \\
        0 & 0 & 0 & 0 & 0 \\
      \end{block}%
    \end{blockarray}%
  \]
  is \emph{consistent} since the augmented column does not contain a pivot.
\end{example}

Inconsistent rref systems can be seen to be unsolvable by inspecting the
equation corresponding to the row containing a pivot in the augmented column.

\begin{example}\label{ex:myex3}
  The rref system
  \[
    \begin{blockarray}{*{5}{r}}
      \begin{block}{ccccc}
        x_1 & x_2 & x_3 & x_4  &\\
      \end{block}
      \begin{block}{[rrrr|r]}
        \cfbox{blue}{1} & 0 & -5 & -3 & 0 \\
        0 & \cfbox{blue}{1} & -4 & -1 & 0 \\
        0 & 0 & 0 & 0 & \cfbox{red}{1} \\
      \end{block}%
    \end{blockarray}%
  \]
  is \emph{inconsistent} since the pivot in position $(3, 5)$ is in the
  augmented column. Note that this system may be written as
  \[
    \begin{array}{rcrcrcrcr}
      x_1  &+& 0\,x_2 &-& 5\,x_3 &-& 3\,x_4 &=& 0 \\
      0\,x_1  &+&    x_2 &-& 4\,x_3 &-&    x_4 &=& 0 \\
      0\,x_1 &+& 0\,x_2 &+& 0\,x_3 &+& 0\,x_4 &=& 1
    \end{array}
  \]
  The last equation is $0=1$, which is impossible! The system is thus
  unsolvable.
\end{example}


\subsection{Solving rref Systems}
\label{sec:solve-rref}

We have now established the basic terminology needed to \emph{solve} rref
systems. In fact, once an rref system is determined to be consistent, one need
only express the dependent variables in terms of the free variables to solve the
system.

\begin{genbox}{Solving rref Systems}
  To solve a consistent rref system, write each dependent variable in terms of
  the free variables.
\end{genbox}

When an rref system is consistent, the structure of the reduced row echelon form
allows one to use each nonzero row to solve for a dependent variable in terms of
the free variables.

\begin{example}\label{ex:myex4}
  The rref system
  \[
    \begin{blockarray}{*{5}{r}}
      \begin{block}{ccccc}
        x_1 & x_2 & x_3 & x_4  &\\
      \end{block}
      \begin{block}{[rrrr|r]}
        \cfbox{blue}{1} & 11 & 0 & -19 & 4 \\
        0 & 0 & \cfbox{blue}{1} & 2 & -1 \\
        0 & 0 & 0 & 0 & 0 \\
      \end{block}%
    \end{blockarray}%
  \]
  is consistent since the pivots are to the left of the augmented column. The
  dependent variables are $\Set{x_1, x_3}$ and the free variables are
  $\Set{x_2, x_4}$. The two pivot rows describe the equations
  \[
    \begin{array}{rcrcrcrcr}
      x_1 &+& 11\,x_2 & &     &-& 19\,x_4 &=& 4 \\
          & &         & & x_3 &+&  2\,x_4 &=& -1
    \end{array}
  \]
  The dependent variables $\Set{x_1, x_3}$ can then be solved for in terms of
  the free variables $\Set{x_2, x_4}$. This gives
  \begin{align*}
    x_1 &= -11\,x_2+19\,x_4+4 & x_3 &= -2\,x_4-1 \\
        &= -11\,c_1+19\,c_2+4 &     &= -2\,c_2-1
  \end{align*}
  where we have set the first free variable $x_2$ equal to the parameter $c_1$
  and the second free variable $x_4$ equal to the parameter $c_2$. Thus all
  solutions to the system are given by
  \begin{sagesilent}
    var('x1 x2 x3 x4 c1 c2')
    vx = matrix.column([x1, x2, x3, x4])
    xf = matrix.column([4-11*c1+19*c2, c1, -1-2*c2, c2])
    xp = matrix.column([4, 0, -1, 0])
    xc1 = matrix.column([-11, 1, 0, 0])
    xc2 = matrix.column([19, 0, -2, 1])
  \end{sagesilent}
  \[
    \sage{vx}
    = \sage{xf}
    = \sage{xp}+c_1\sage{xc1}+c_2\sage{xc2}
  \]
  where $c_1$ and $c_2$ can be chosen ``freely.'' Consequently, this system has
  \emph{infinitely many solutions}.
\end{example}

\begin{example}\label{ex:myex5}
  The rref system
  \[
    \begin{blockarray}{*{4}{r}}
      \begin{block}{cccc}
        x & y & z & \\
      \end{block}
      \begin{block}{[rrr|r]}
        \cfbox{blue}{1} & 0 & 0  & 4 \\
        0 & \cfbox{blue}{1} & 0  & -1 \\
        0 & 0 & \cfbox{blue}{1} & 47  \\
      \end{block}%
    \end{blockarray}%
  \]
  is consistent. The dependent variables are $\Set{x, y, z}$ and the system has
  no free variables. The only solution is
  \begin{sagesilent}
    var('x y z')
    vx = matrix.column([x, y, z])
    xp = matrix.column([4, -1, 47])
  \end{sagesilent}
  \[
    \sage{vx} = \sage{xp}
  \]
  Note that this system has \emph{exactly one solution}.
\end{example}

\begin{example}\label{ex:myex6}
  The consistent rref system
  \[
    \begin{blockarray}{*{6}{r}}
      \begin{block}{cccccc}
        y_1 & y_2 & y_3 & y_4 & y_5 &\\
      \end{block}
      \begin{block}{[rrrrr|r]}
        \cfbox{blue}{1} & -4 & 37 &               0 & -1  & -18 \\
        0 &  0 &  0 & \cfbox{blue}{1} & -13 &  23 \\
      \end{block}%
    \end{blockarray}%
  \]
  has dependent variables $\Set{y_1, y_4}$ and free variables are
  $\Set{y_2, y_3, y_5}$. Solving for the dependent variables in terms of the
  free variables describes all solutions as
  \begin{sagesilent}
    var('y1 y2 y3 y4 y5 c1 c2 c3')
    vy = matrix.column([y1, y2, y3, y4, y5])
    yf = matrix.column([4*c1-37*c2+c3-18, c1, c2, 13*c3+23, c3])
    yp = matrix.column([-18, 0, 0, 23, 0])
    yc1 = matrix.column([4, 1, 0, 0, 0])
    yc2 = matrix.column([-37, 0, 1, 0, 0])
    yc3 = matrix.column([1, 0, 0, 13, 1])
  \end{sagesilent}
  \[
    \sage{vy}
    = \sage{yf}
    = \sage{yp}+c_1\sage{yc1}+c_2\sage{yc2}+c_3\sage{yc3}
  \]
  where $c_1$, $c_2$ and $c_3$ can be chosen ``freely.'' Consequently, this
  system has \emph{infinitely many solutions}.
\end{example}

\begin{example}\label{ex:myex7}
  The rref system
  \[
    \begin{blockarray}{*{6}{r}}
      \begin{block}{cccccc}
        p & q & x & y & z &\\
      \end{block}
      \begin{block}{[rrrrr|r]}
        \cfbox{blue}{1} & 14 & -2 &               0 & -1  & 0 \\
        0 &  0 &  0 & \cfbox{blue}{1} & -13 & 0 \\
        0 &  0 &  0 & 0 & 0 & \cfbox{red}{1} \\
      \end{block}%
    \end{blockarray}%
  \]
  is \emph{inconsistent} since the pivot in position $(3, 6)$ is in the
  augmented column. The third equation of this system is $0=1$, so the system
  \emph{has no solutions}.
\end{example}

As seen in the above examples, the number of solutions to a consistent rref
system is controlled by the existence of free variables. Since the rank and the
nullity of an rref system count the number of dependent and free variables
respectively, we can neatly summarize an account of the number of solutions to
an rref system in terms of these numbers.

\begin{genbox}{The Number of Solutions to an rref System}
  A consistent rref system has
  \begin{description}
  \item[exactly one solution] if it has no free variables. This means that
    \begin{align*}
      \textnormal{rank} &= \textnormal{number of variables} & \textnormal{and} && \textnormal{nullity} &= 0
    \end{align*}
  \item[infinitely many solutions] if it has free variables. This means that
    \begin{align*}
      \textnormal{rank} &< \textnormal{number of variables} & \textnormal{and} && \textnormal{nullity} &> 0
    \end{align*}
  \end{description}
  An inconsistent system has \emph{no solutions}.
\end{genbox}

The table below summarizes the number of solutions to the systems in Examples
\ref{ex:myex1}-\ref{ex:myex7} above.

\begin{center}
  \begin{tabular}{*5l}    \toprule
    Example & Consistency & rank & nullity & \# of solutions  \\\midrule
    \rowcolor{blue!50}  \ref{ex:myex1} & consistent   & 3 & 1 & infinitely many solutions \\
    \rowcolor{green!50} \ref{ex:myex2} & consistent   & 2 & 2 & infinitely many solutions \\
    \rowcolor{blue!50}  \ref{ex:myex3} & inconsistent & 2 & 2 & no solutions \\
    \rowcolor{green!50} \ref{ex:myex4} & consistent   & 2 & 2 & infinitely many solutions \\
    \rowcolor{blue!50}  \ref{ex:myex5} & consistent   & 3 & 0 & exactly one solution \\
    \rowcolor{green!50} \ref{ex:myex6} & consistent   & 2 & 3 & infinitely many solutions \\
    \rowcolor{blue!50}  \ref{ex:myex7} & inconsistent   & 2 & 3 & no solutions \\
    \bottomrule
    \hline
  \end{tabular}
\end{center}

An additional example demonstrating how to analyze an rref system can be found
in the video supplement to this section:
\url{https://warpwire.duke.edu/w/IhoCAA/}.


\end{document}
