\documentclass[12pt]{article}

\title{Unit 2.3: Nonsingular Matrices}
\author{MIDS Linear Algebra Review}
\date{}

\input{../style.tex}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\maketitle


\tableofcontents


\newpage
\section{Motivation}
\label{sec:nonsing}

In scalar arithmetic, a number $a$ has a \emph{reciprocal} $a^{-1}=\frac{1}{a}$
provided that $a\neq0$. The reciprocal of a number is useful for solving
equations.


\begin{example}
  In the metric system, $\SI{1}{\km}$ (one \emph{kilometer}) is equal to
  $\SI{1000}{\meter}$ (one thousand \emph{meters}). To convert
  $\SI{1250}{\meter}$ into kilometers, one must solve the equation
  $x\,\si{\km}=\SI{1250}{\meter}$. Since $\SI{1}{\km}=\SI{1000}{\meter}$, this
  equation is equivalent to
  \begin{equation}
    1000\,x=1250\label{eq:ax=b}
  \end{equation}
  To solve this equation, we can multiply both sides by the reciprocal of $1000$
  to obtain
  \[
    \pair*{1000^{-1}}  1000\,x= \pair*{1000^{-1}}1250
  \]
  Multiplying $1000$ by the reciprocal $1000^{-1}=\frac{1}{1000}$ ``cancels''
  the $1000$ from the $1000\,x$ term. The equation then simplifies to
  \[
    x=(1000^{-1})\cdot1250=\oldfrac{1250}{1000}=\oldfrac{5}{4}=1.25
  \]
  This tells us that $\SI{1250}{\meter}=\SI{1.25}{\km}$.
\end{example}

The key feature of the reciprocal $a^{-1}$ of a number $a$ is that
$a^{-1}a=1$. So, multiplying both sides of the equation $ax=b$ by $a^{-1}$ gives
$a^{-1}ax=a^{-1}b$ which is equivalent to $1x=a^{-1}b$. Since multiplying a
number by $1$ does not change the number, we obtain $x=a^{-1}b$.

Throughout this unit we have studied ``matrix equations'' of the form
$A\vv{x}=\vv{b}$. This equation looks similar to the scalar equation
\eqref{eq:ax=b} above. It is therefore tempting to try to multiply the equation
$A\vv{x}=\vv{b}$ by the ``reciprocal'' $A^{-1}$ of $A$. Doing so would give the
equation
\[
  A^{-1}A\vv{x}=A^{-1}\vv{b}
\]
We would then hope that $A^{-1}A=I$ (the \emph{identity matrix}) so the equation
would become
\[
  I\vv{x}=A^{-1}\vv{b}
\]
Since multiplying by $I$ never changes a vector, this equation is
\[
  \vv{x}=A^{-1}\vv{b}
\]
So, multiplying $A\vv{x}=\vv{b}$ by the ``reciprocal'' of $A$ allows us to solve
for the unknown vector $\vv{x}$.

The problem with this idea is that we do not yet know what ``reciprocal'' means
in the context of matrices. We cannot multiply by $A^{-1}$ if we do not know
what $A^{-1}$ actually is! In this section, we will define $A^{-1}$ and discuss
when $A^{-1}$ exists.


\newpage
\section{Definition}
\label{sec:invdef}

Matrices $A$ that have a ``reciprocal'' $A^{-1}$ are called \emph{invertible} or
\emph{nonsingular} matrices.

\begin{genbox}{Definition of Invertible Matrices}
  A matrix $A$ is \emph{invertible} if $A$ is \emph{square} ($n\times n$) and if
  there is another $n\times n$ matrix $A^{-1}$ satisfying the equation
  $A^{-1}A=I$.

  Invertible matrices are also called \emph{nonsingular}. The matrix $A^{-1}$ is
  called the \emph{inverse} of $A$.
\end{genbox}

Just like the \emph{reciprocal} of a number $a$ is the number
$a^{-1}=\frac{1}{a}$ satisfying $a^{-1}a=1$, the \emph{inverse} of a square
matrix $A$ is the matrix $A^{-1}$ satisfying $A^{-1}A=I$.

To decide if a square matrix $A$ is invertible, one must find the inverse matrix
$A^{-1}$. When a candidate for $A^{-1}$ is proposed, this amounts to checking
the equation $A^{-1}A=I$.

\begin{sagesilent}
  set_random_seed(3921451)
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  B = A.inverse()
\end{sagesilent}
\begin{example}
  Consider the matrices $A$ and $B$ given by
  \begin{align*}
    A &= \sage{A} & B &= \sage{B}
  \end{align*}
  Determine if $B=A^{-1}$.
  \begin{solution}
    To decide if $B=A^{-1}$, we must check the equation $BA=I$. The product $BA$
    is given by
    \[
      BA
      = \sage{B}\sage{A}
      = \sage{B*A}
      = I
    \]
    This means that $B=A^{-1}$. We therefore say that $A$ is \emph{invertible}
    (or \emph{nonsingular}) and the \emph{inverse} of $A$ is
    \[
      A^{-1}=\sage{B}
    \]
  \end{solution}
\end{example}

Solving systems of the form $A\vv{x}=\vv{b}$ is dramatically simplified when $A$
is invertible and the inverse $A^{-1}$ is known.

\newcommand{\myDummyGenBox}{Solving $A\vv{x}=\vv{b}$ when $A^{-1}$ is known}
\begin{genbox}{\myDummyGenBox}
  When $A^{-1}$ exists, multiply $A\vv{x}=\vv{b}$ by $A^{-1}$ to obtain the
  solution $\vv{x}=A^{-1}\vv{b}$.
\end{genbox}

So, when $A^{-1}$ is known, the solution to $A\vv{x}=\vv{b}$ is given by
$\vv{x}=A^{-1}\vv{b}$. This means that we do not need to perform Gauss-Jordan
elimination on the augmented matrix $[A\mid\vv{b}]$.

\begin{sagesilent}
  var('a c')
  A = matrix([[3, 5], [6, 9]])
  vx = matrix.column([c, a])
  b = matrix.column([62, 114])
  numc, numa = A.inverse() * vector(b)
\end{sagesilent}
\begin{example}\label{ex:bus}
  A group of tourists spends \mymon{62} on bus tickets at a rate of \mymon{3}
  per child and \mymon{5} per adult. The same group also spends \mymon{114} on
  train tickets at a rate of \mymon{6} per child and \mymon{9} per adult. How
  many children and how many adults are in the group?
  \begin{solution}
    We wish to solve the system of linear equations
    \[
      \begin{array}{rcrcrcc}
        3\,c &+& 5\,a &=& 62  &\leftarrow& \textnormal{\mymon{62} spent on bus tickets (\mymon{3} / child, \mymon{5} / adult)}\\
        6\,c &+& 9\,a &=& 114 &\leftarrow& \textnormal{\mymon{114} spent on train tickets (\mymon{6} / child, \mymon{9} / adult)}
      \end{array}
    \]
    where $c$ is the number of children in the group and $a$ is the number of
    adults. This system is of the form $A\vv{x}=\vv{b}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{vx} & \vv{b} &= \sage{b}
    \end{align*}
    The matrix $A$ is invertible and the inverse is given by
    \[
      A^{-1}=\sage{A.inverse()}
    \]
    The solution to $A\vv{x}=\vv{b}$ is then given by
    \[
      \vv{x}
      = A^{-1}\vv{b}
      = \sage{A.inverse()}\sage{b}
      = \sage{A.inverse()*b}
    \]
    The number of children in the group is $c=\sage{numc}$ and the number of
    adults in the group is $a=\sage{numa}$.
  \end{solution}
\end{example}

Solving the system of equations $A\vv{x}=\vv{b}$ in Example \ref{ex:bus} amounts
to performing the matrix-vector multiplication $\vv{x}=A^{-1}\vv{b}$. This is
only possible, however, because $A^{-1}$ is given to us in the problem. In
Section \ref{sec:findinv} below, we will describe an \emph{algorithm} for
computing $A^{-1}$.

Before describing the algorithm for computing $A^{-1}$ we must issue an
important warning. Recall that every number $a$ has a \emph{reciprocal}
$a^{-1}=\frac{1}{a}$ provided that $a\neq0$. The situation for matrices,
however, is more complicated.

\begin{sagesilent}
  var('a b c d')
  A = matrix([[1, 0],[2, 0]])
  B = matrix([[a, b],[c, d]])
\end{sagesilent}
\begin{example}\label{ex:sing}
  Consider the matrix
  \[
    A = \sage{A}
  \]
  This matrix is \emph{square} ($2\times 2$), so we may ask whether or not $A$
  has an inverse. If such an inverse existed, it would be of the form
  \[
    A^{-1}
    = \sage{B}
  \]
  However, performing the matrix multiplication $A^{-1}A$ gives
  \newcommand{\myAiA}{
    \left[
      \begin{array}{rr}
        a+2\,b  & 0 \\
        c+2\,d & \cfbox{red}{0}
      \end{array}
    \right]
  }
  \newcommand{\myNotI}{
    \left[
      \begin{array}{rr}
        1 & 0 \\
        0 & \cfbox{red}{1}
      \end{array}
    \right]
  }
  \[
    A^{-1}A
    = \sage{B}\sage{A}
    = \myAiA
    \neq
    \myNotI
  \]
  The $(2, 2)$ entry of $A^{-1}A$ is $0$ but the $(2, 2)$ entry of $I$ is
  $1$. This means that $A^{-1}$ cannot exist! The matrix $A$ is \emph{not
    invertible} or \emph{singular}.
\end{example}

Example \ref{ex:sing} demonstrates that \emph{not all square matrices are
  invertible}.

\begin{genbox}{Not all Matrices are Invertible}
  Given a square matrix $A$, an inverse $A^{-1}$ is not guaranteed to exist.
\end{genbox}

Given a square matrix $A$, one is often interested in determining if $A^{-1}$
exists. The algorithm for computing inverses given in Section \ref{sec:findinv}
below will describe a criterion for determining when a square matrix is
invertible.


\newpage
\section{Using Gauss-Jordan Elimination to Compute Inverses}
\label{sec:findinv}

\subsection{Description of the Algorithm}
\label{sec:algo}

Given a $n\times n$ matrix $A$, one may determine if $A$ is invertible by using
Gauss-Jordan elimination. The idea is to ``augment'' a $n\times n$ matrix $A$
with the $n\times n$ identity matrix $I_n$, giving $[A\mid I_n]$. We then
row-reduce $[A\mid I_n]$ until the identity matrix appears on the left and the
inverse matrix appears on the right $[I_n\mid A^{-1}]$. This is only possible,
however, if the reduced row echelon form of $A$ is the identity matrix $I_n$.


\begin{genbox}{Gauss-Jordan Algorithm for Computing $A^{-1}$}
  Let $A$ be a $n\times n$ matrix. The following steps will compute $A^{-1}$ or
  determine that $A^{-1}$ exists.
  \begin{description}
  \item[Step 1.] Form the augmented matrix $[A\mid I_n]$.
  \item[Step 2.] Use Gauss-Jordan elimination to row reduce $[A\mid I_n]$ to
    $[\rref(A)\mid B]$.
  \item[Step 3.] If $\rref(A)\neq I_n$, then $A$ is not invertible.
  \item[Step 4.] If $\rref(A)=I_n$, then $A$ is invertible and $B=A^{-1}$.
  \end{description}
\end{genbox}

Before demonstrating how this algorithm works in examples, it is useful to
comment on the criterion for invertibility that the algorithm offers.  Steps two
and three of the algorithm tell us that a matrix $A$ can only have an inverse if
$\rref(A)=I_n$. Since the identity matrix $I_n$ is the only $n\times n$ matrix
in reduced row echelon form with $n$ pivots, this gives a useful way for
determining whether or not $A$ is invertible.

\begin{genbox}{Rank Criterion for Invertibility}
  A $n\times n$ matrix $A$ is invertible if and only if $\rank(A)=n$.
\end{genbox}

By counting the pivots in the reduced row echelon form of $A$, we can quickly
determine whether or not $A$ is invertible.

\begin{sagesilent}
  set_random_seed(3109834)
  A = random_matrix(ZZ, 2, algorithm='echelonizable', rank=2)
  B = random_matrix(ZZ, 3, algorithm='echelonizable', rank=2)
  C = random_matrix(ZZ, 4, algorithm='echelonizable', rank=4)
\end{sagesilent}
\begin{example}
  Suppose that $A$, $B$, and $C$ are matrices with
  \begin{align*}
    \rref(A) &= \sage{A.rref()} & \rref(B) &= \sage{B.rref()} & \rref(C) &= \sage{C.rref()}
  \end{align*}
  By counting the pivots in these reduced row echelon forms, we see that
  \begin{align*}
    \rank(A) &= \sage{A.rank()} & \rank(B) &= \sage{B.rank()} & \rank(C) &= \sage{C.rank()}
  \end{align*}
  The rank criterion for invertibility can be applied to each matrix:
  \begin{description}
  \item[$A$ is invertible] since $A$ is $2\times 2$ and $\rank(A)=2$
  \item[$B$ is not invertible] since $B$ is $3\times 3$ and $\rank(B)=2<3$
  \item[$C$ is invertible] since $C$ is $4\times 4$ and $\rank(C)=4$
  \end{description}
\end{example}

The rank criterion for invertibility is useful if one only wants to know whether
or not $A$ is invertible. To find $A^{-1}$, one must follow each of the steps in
the Gauss-Jordan algorithm for computing $A^{-1}$.

\newpage
\subsection{$2\times 2$ Examples}
\label{sec:22ex}

In this section, we use the Gauss-Jordan algorithm to compute the inverse of a
few $2\times 2$ matrices.

\begin{sagesilent}
  A = matrix([[3, 5], [6, 9]])
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=0, scale=1/3)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-6)
  E3 = elementary_matrix(A.nrows(), row1=1, scale=-1)
  E4 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=-5/3)
\end{sagesilent}
\begin{example}
  In Example \ref{ex:bus} we were given a matrix $A$ its inverse
  \begin{align*}
    A &= \sage{A} & A^{-1} &= \sage{A.inverse()}
  \end{align*}
  We may find $A^{-1}$ ourselves by using the Gauss-Jordan algorithm for
  computing inverses. The algorithm requires that we augment $A$ with the
  identity matrix $I_2$ to obtain $[A\mid I_2]$. We then row reduce
  $[A\mid I_2]$ until we reach the reduced row echelon form on the left. The
  steps for these row reductions are:
  \begin{align*}
    \sage{M}
    &\xrightarrow{(\frac{1}{3})\cdot R_1\to R_1} \sage{E1*M} \\
    &\xrightarrow{R_2-6\cdot R_1\to R_2} \sage{E2*E1*M} \\
    &\xrightarrow{(-1)\cdot R_2\to R_2}\sage{E3*E2*E1*M} \\
    &\xrightarrow{R_1-(\frac{5}{3})\cdot R_2\to R_1}\sage{E4*E3*E2*E1*M}
  \end{align*}
  After these row reductions, the reduced row echelon form of $A$ has appeared
  on the left. Here, we have
  \[
    \rref(A)=\sage{A.rref()}=I_2
  \]
  There are two pivots in $\rref(A)$. Since $A$ is $2\times 2$, this means that
  $A$ is invertible. The inverse of $A$ is the new matrix on the right
  \[
    A^{-1}=\sage{A.inverse()}
  \]
\end{example}


\begin{sagesilent}
  set_random_seed(524545098)
  A = random_matrix(ZZ, 2)
  I = identity_matrix(ZZ, A.nrows())
  M = A.augment(I, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=0, scale=-1)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=3)
  E3 = elementary_matrix(A.nrows(), row1=1, scale=-1/7)
  E4 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=2)
\end{sagesilent}
\begin{example}
  Determine whether or not $A=\sage{A}$ is invertible. If $A$ is invertible,
  find $A^{-1}$.
  \begin{solution}
    We use the Gauss-Jordan algorithm for computing inverses:
    \begin{align*}
      \sage{M}
      &\xrightarrow{-R_1\to R_1} \sage{E1*M} \\
      &\xrightarrow{R_2+3\cdot R_1\to R_2} \sage{E2*E1*M} \\
      &\xrightarrow{(-\frac{1}{7})\cdot R_2\to R_2} \sage{E3*E2*E1*M} \\
      &\xrightarrow{R_1+2\cdot R_2\to R_1} \sage{E4*E3*E2*E1*M}
    \end{align*}
    These row reductions show that $\rref(A)=I_2$, so $A$ is invertible. The
    inverse of $A$ is given by
    \[
      A^{-1}
      = \sage{A.inverse()}
      = \frac{1}{7}\sage{A.adjoint()}
    \]
    We can verify that this computation is correct by multiplying
    $A^{-1}A$. This gives
    \[
      A^{-1}A
      = \frac{1}{7}\sage{A.adjoint()}\sage{A}
      = \frac{1}{7}\sage{A.adjoint()*A}
      = \sage{I}
      = I_2
    \]
    Since $A^{-1}A=I_2$, our formula for $A^{-1}$ is indeed correct.
  \end{solution}
\end{example}


\begin{sagesilent}
  set_random_seed(2158)
  A = random_matrix(ZZ, 2, algorithm='echelonizable', rank=1)
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=-3)
\end{sagesilent}
\begin{example}
  Determine if $A=\sage{A}$ is nonsingular. If $A$ is nonsingular, find
  $A^{-1}$.
  \begin{solution}
    Recall that nonsingular is synonym for invertible. We can therefore use the
    Gauss-Jordan algorithm to determine if $A$ is nonsingular:
    \[
      \sage{M}
      \xrightarrow{R_2-3\cdot R_1\to R_2} \sage{E1*M}
    \]
    This single row reduction shows that
    \[
      \rref(A)=\sage{A.rref()}
    \]
    There is only one pivot, so $\rank(A)=\sage{A.rank()}$. Since $A$ is
    $2\times 2$, $A$ is \emph{not invertible} or \emph{singular}. This means
    that $A^{-1}$ does not exist.
  \end{solution}
\end{example}


\newpage
\subsection{$3\times 3$ Examples}
\label{sec:33ex}

In this section, we use the Gauss-Jordan algorithm to compute the inverse of a
few $3\times 3$ matrices.

\begin{sagesilent}
  set_random_seed(31510876)
  A = random_matrix(ZZ, 3, algorithm='echelonizable', rank=2)
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=0, scale=-1/5)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=2)
  E3 = elementary_matrix(A.nrows(), row1=1, scale=-5)
  E4 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=28/5)
  E5 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=-3)
\end{sagesilent}
\begin{example}
  Determine if $A=\sage{A}$ is invertible. If $A$ is invertible, find $A^{-1}$.
  \begin{solution}
    Use use the Gauss-Jordan algorithm for computing inverses:
    \newcommand{\myNextStep}{\scriptsize
      \begin{array}{rcrcr}
        R_1 &+& (\frac{28}{5})\cdot R_2 &\to& R_1 \\
        R_3 &-& 3\cdot R_2 &\to& R_3
      \end{array}
    }
    \begin{align*}
      \sage{M}
      &\xrightarrow{(-\frac{1}{5})\cdot R_1\to R_1} \sage{E1*M} \\
      &\xrightarrow{R_2+2\cdot R_1\to R_2}\sage{E2*E1*M} \\
      &\xrightarrow{-5\cdot R_2\to R_2}\sage{E3*E2*E1*M} \\
      &\xrightarrow{\myNextStep}\sage{E5*E4*E3*E2*E1*M}
    \end{align*}
    These row reductions show that
    \[
      \rref(A)=\sage{A.rref()}
    \]
    There are two pivots, so $\rank(A)=\sage{A.rank()}$. Since $A$ is
    $3\times 3$, $A$ is \emph{not invertible}. This means that $A^{-1}$ does not
    exist.
  \end{solution}
\end{example}


\begin{sagesilent}
  set_random_seed(91234598)
  A = random_matrix(ZZ, 3, algorithm='echelonizable', rank=3)
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  E1 = elementary_matrix(A.nrows(), row1=0, scale=1/7)
  E2 = elementary_matrix(A.nrows(), row1=1, row2=0, scale=3)
  E3 = elementary_matrix(A.nrows(), row1=2, row2=0, scale=2)
  E4 = elementary_matrix(A.nrows(), row1=1, scale=7/34)
  E5 = elementary_matrix(A.nrows(), row1=0, row2=1, scale=19/7)
  E6 = elementary_matrix(A.nrows(), row1=2, row2=1, scale=3/7)
  E7 = elementary_matrix(A.nrows(), row1=2, scale=34)
  E8 = elementary_matrix(A.nrows(), row1=0, row2=2, scale=-97/34)
  E9 = elementary_matrix(A.nrows(), row1=1, row2=2, scale=145/34)
\end{sagesilent}
\begin{example}
  Determine if $A=\sage{A}$ is invertible. If $A$ is invertible, find $A^{-1}$.
  \begin{solution}
    Use the Gauss-Jordan algorithm for computing inverses:
    \newcommand{\myStepA}{\scriptsize
      \begin{array}{rcrcr}
        R_2 &+& 3\cdot R_1 &\to& R_2 \\
        R_3 &+& 2\cdot R_1 &\to& R_3
      \end{array}
    }
    \newcommand{\myStepB}{\scriptsize
      \begin{array}{rcrcr}
        R_1 &+& (\frac{19}{7})\cdot R_2 &\to& R_1 \\
        R_3 &+& (\frac{3}{7})\cdot R_2 &\to& R_3
      \end{array}
    }
    \newcommand{\myStepC}{\scriptsize
      \begin{array}{rcrcr}
        R_1 &-& (\frac{97}{34})\cdot R_3 &\to& R_1 \\
        R_2 &+& (\frac{145}{34})\cdot R_3 &\to& R_2
      \end{array}
    }
    \begin{align*}
      &\sage{M} \\
      &\qquad\xrightarrow{(\frac{1}{7})\cdot R_1\to R_1} \sage{E1*M} \\
      &\qquad\xrightarrow{\myStepA}\sage{E3*E2*E1*M} \\
      &\qquad\xrightarrow{(\frac{7}{34})\cdot R_2\to R_2}\sage{E4*E3*E2*E1*M} \\
      &\qquad\xrightarrow{\myStepB}\sage{E6*E5*E4*E3*E2*E1*M} \\
      &\qquad\xrightarrow{34\cdot R_3\to R_3}\sage{E7*E6*E5*E4*E3*E2*E1*M} \\
      &\qquad\xrightarrow{\myStepC}\sage{E9*E8*E7*E6*E5*E4*E3*E2*E1*M}
    \end{align*}
    These row reductions show that $\rref(A)=I_3$, so $A$ is invertible. The
    inverse matrix $A^{-1}$ is then given by
    \[
      A^{-1}=\sage{A.inverse()}
    \]
  \end{solution}
\end{example}


\newpage
\section{Properties of Inverses}

We conclude this section by listing a few important properties of inverses.

\begin{genbox}{Matrix Inverse Rules}
  \begin{description}
  \item[Involution Rule] If $A$ is invertible, then $(A^{-1})^{-1}=A$.
  \item[Transposition Rule] If $A$ is invertible, then $(A^{-1})^\intercal=(A^\intercal)^{-1}$.
  \item[Reverse Multiplication Rule] If $A$ and $B$ are invertible, then
    $(AB)^{-1}=B^{-1}A^{-1}$.
  \item[Scaling Rule] If $A$ is invertible and $c\neq0$, then
    $(c\cdot A)^{-1}=(\frac{1}{c})\cdot A^{-1}$.
  \end{description}
\end{genbox}

\begin{sagesilent}
  set_random_seed(43235321)
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  Ai = A.inverse()
  N = Ai.augment(I, subdivide=True)
\end{sagesilent}
\begin{example}
  Consider the matrix $A=\sage{A}$. Performing Gauss Jordan elimination on
  $[A\mid I_{\sage{A.nrows()}}]$ gives
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  This demonstrates that $A^{-1}=\sage{Ai}$. On the other hand, performing Gauss
  Jordan elimination on $[A^{-1}\mid I_{\sage{A.nrows()}}]$ gives
  \[
    \rref\sage{N}=\sage{N.rref()}
  \]
  This demonstrates the involution rule $(A^{-1})^{-1}=A$.
\end{example}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  Ai = A.inverse()
  I = identity_matrix(A.nrows())
  M = A.augment(I, subdivide=True)
  N = A.T.augment(I, subdivide=True)
  ATi = A.T.inverse()
\end{sagesilent}
\begin{example}
  Consider the matrix $A=\sage{A}$. Performing Gauss Jordan elimination on
  $[A\mid I_{\sage{A.nrows()}}$ gives
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  This demonstrates that $A^{-1}=\sage{Ai}$. On the other hand, performing Gauss
  Jordan elimination on $[A^\intercal\mid I_{\sage{A.nrows()}}]$ gives
  \[
    \rref\sage{N}=\sage{N.rref()}
  \]
  This demonstrates the transposition rule
  $(A^{-1})^\intercal=(A^\intercal)^{-1}$.
\end{example}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  B = random_matrix(ZZ, 3, algorithm='unimodular')
  M = (A*B).augment(identity_matrix(A.nrows()), subdivide=True)
  Ai = A.inverse()
  Bi = B.inverse()
\end{sagesilent}
\begin{example}
  The inverses of the matrices
  \begin{align*}
    A &= \sage{A} & B &= \sage{B}
  \end{align*}
  are given by
  \begin{align*}
    A^{-1} &= \sage{Ai} & B^{-1} &= \sage{Bi}
  \end{align*}
  The product $AB$ is given by
  \[
    AB
    = \sage{A}\sage{B}
    = \sage{A*B}
  \]
  Performing Gauss Jordan elimination on $[AB\mid I_3]$ gives
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  This shows that $(AB)^{-1}=\sage{(A*B).inverse()}$. On the other hand, the
  product $B^{-1}A^{-1}$ is given by
  \[
    B^{-1}A^{-1}
    = \sage{Bi}\sage{Ai}
    = \sage{Bi*Ai}
  \]
  This demonstrates the reverse multiplication rule $(AB)^{-1}=B^{-1}A^{-1}$.
\end{example}



\begin{sagesilent}
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  c = 2
  M = (c*A).augment(identity_matrix(A.nrows()), subdivide=True)
  N = A.augment(identity_matrix(A.nrows()), subdivide=True)
\end{sagesilent}
\begin{example}
  Consider the matrix $A=\sage{A}$ and the nonzero scalar
  $c=\sage{c}$. Performing Gauss Jordan elimination on $[c\cdot A\mid I_3]$
  gives
  \[
    \rref\sage{M}=\sage{M.rref()}
  \]
  This demonstrates that $(c\cdot A)^{-1}=\sage{(c*A).inverse()}$. On the other
  hand, performing Gauss Jordan elimination on $[A\mid I_3]$ gives
  \[
    \rref\sage{N}=\sage{N.rref()}
  \]
  This shows that $A^{-1}=\sage{A.inverse()}$, so
  $(\sage{1/c})\cdot A^{-1}=\sage{(1/c)*A.inverse()}$. This demonstrates the
  scaling rule for inverses.
\end{example}



\end{document}
